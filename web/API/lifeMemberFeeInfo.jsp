<%-- 
    Document   : lifeMemberFeeInfo
    Created on : DEC 04, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.time.Period"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String mId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: lifeMemberFeeInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject feeYearObj = new JSONObject();
    JSONArray feeYearObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    Query mMemberSQL = null;
    String memberCountSQL = null;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    // Query mMemberTypeAgeLimitSQL = null;
    //  Object[] mMemberTypeAgeLimitObj = null;
    String mMemberTypeAgeLimitSQL = "";
    String mMemberTypeAgeLimit = "";

    int memberXId = 0;
    String memberXIEBId = "";
    String memberXName = "";
    java.util.Date memberDob = new Date();

    String membershipEntranceFee = "";
    String membershipAnnualSubscriptionFee = "";
    String membershipDiplomaFee = "";
    String membershipIDCardFee = "";

    //  String memberDob = "";
    //  String memberAge = "";
    int memberAge = 0;
    int memberAgeNextYear = 0;
    String memberAgeFee = "";
    String totalDueSumSQL = "";
    String memberTotalDue = "";
    
    String  totalAdvanceSumSQL = "";
    String memberTotalAdvance = "";
    //  String memberTotalAmount = "";

    String ageFeeAmountSQL = "";
    String ageFeeAmount = "";

    //  double value = Double.valueOf(str);
    double memberTotalAmount = 0.0d;

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    memberXIEBId = member.getMemberId();
                    memberXName = member.getMemberName();
                    memberDob = member.getDob();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + memId + "' AND SYSDATE() between mt.ed and mt.td");

                        for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

                            mMemberTypeObj = (Object[]) mMemberTypeItr.next();
                            mMemberTypeId = mMemberTypeObj[0].toString();
                            mMemberTypeName = mMemberTypeObj[1].toString();

                        }

                        mMemberTypeAgeLimitSQL = "select member_age FROM lifemember_age_limit  WHERE  member_type_id=" + mMemberTypeId + " AND SYSDATE() between ed and td";
                        logger.info("API :: mMemberTypeAgeLimitSQL:: " + mMemberTypeAgeLimitSQL);
                        mMemberTypeAgeLimit = dbsession.createSQLQuery(mMemberTypeAgeLimitSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(mMemberTypeAgeLimitSQL).uniqueResult().toString();

                        //Calculate Age 
                        //using Calendar Object
                        String strDate = dateFormat.format(memberDob);
                        Date d = dateFormat.parse(strDate);
                        Calendar c = Calendar.getInstance();
                        c.setTime(d);
                        int year = c.get(Calendar.YEAR);
                        int month = c.get(Calendar.MONTH) + 1;
                        int date = c.get(Calendar.DATE);
                        LocalDate l1 = LocalDate.of(year, month, date);
                        LocalDate now1 = LocalDate.now();
                        Period diff1 = Period.between(l1, now1);

                        memberAge = diff1.getYears();

                        memberAgeNextYear = memberAge + 1;

                        logger.info("API :: memberAge :: " + memberAge);
                        logger.info("API :: memberAgeNextYear :: " + memberAgeNextYear);

                        //check age Limit for Life lifemember_age_limit
                        //Show age wise fee info form life_age_amount
                        ageFeeAmountSQL = "SELECT amount FROM lifemember_age_fee  WHERE  age='" + memberAgeNextYear + "' AND member_type_id ='" + mMemberTypeId + "'  AND SYSDATE() between ed and td";
                        logger.info("API :: ageFeeAmountSQL :: " + ageFeeAmountSQL);
                        ageFeeAmount = dbsession.createSQLQuery(ageFeeAmountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(ageFeeAmountSQL).uniqueResult().toString();

                        logger.info("API :: ageFeeAmount :: " + ageFeeAmount);
                        //    memberAgeFee = "12050";
                        memberAgeFee = ageFeeAmount;
                        //total due
                        //  String totalPaymentDue = "0";
                        totalDueSumSQL = "SELECT sum(amount) FROM member_fee  WHERE  member_id=" + memId + " AND status ='0'";
                        memberTotalDue = dbsession.createSQLQuery(totalDueSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(totalDueSumSQL).uniqueResult().toString();
                        //   totalPaymentDue = Integer.parseInt(totalPaymentDue1);

                        logger.info("API :: Life Member Fee Info :: memberTotalDue :: " + memberTotalDue);
                        
                        
                        
                        SimpleDateFormat dateFormatToday = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat dateFormatM = new SimpleDateFormat("MM");
                        SimpleDateFormat dateFormatY = new SimpleDateFormat("yyyy");
                        Date dateToday = new Date();
                        String todayDate = dateFormatToday.format(dateToday);
                         String todayMonth = dateFormatM.format(dateToday);
                     //   String todayMonth = "1";
                        String todayYear = dateFormatY.format(dateToday);

                        String todayMemberFeesYearNameId = "";
                        String todayMemberFeesYearName = "";

                        String todayMonthCheck = "";
                        int currentYearValue = 0;

                        if (Integer.parseInt(todayMonth) <= 6) {
                            todayMonthCheck = "1-6-Month";
                            currentYearValue = Integer.parseInt(todayYear) - 1;
                            todayMemberFeesYearName = currentYearValue + "-" + todayYear;
                        }

                        if (Integer.parseInt(todayMonth) > 6 && Integer.parseInt(todayMonth) <= 12) {
                            todayMonthCheck = "7-12-Month";
                            currentYearValue = Integer.parseInt(todayYear) + 1;
                            todayMemberFeesYearName = todayYear + "-" + currentYearValue;
                        }

                        String todayFeeYearSQL = "SELECT member_fees_year_id FROM member_fees_year  WHERE  member_fees_year_name='" + todayMemberFeesYearName + "'";                        
                        todayMemberFeesYearNameId = dbsession.createSQLQuery(todayFeeYearSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(todayFeeYearSQL).uniqueResult().toString();
                        
                        
                        
                        
                         //  String totalPaymentDue = "0";
                        totalAdvanceSumSQL = "SELECT sum(amount) FROM member_fee  WHERE  member_id=" + memId + " AND fee_year > '" + todayMemberFeesYearNameId + "' AND status ='1'";
                        memberTotalAdvance = dbsession.createSQLQuery(totalAdvanceSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(totalAdvanceSumSQL).uniqueResult().toString();
                        //   totalPaymentDue = Integer.parseInt(totalPaymentDue1);

                        logger.info("API :: Life Member Fee Info :: memberTotalAdvance :: " + memberTotalAdvance);
                   

                        //total advance
                     //   memberTotalAdvance = "0";
                        //total Amount

                        //  double value = Double.valueOf(str);
                        //   feeAmount = Double.valueOf(renewalTypeId) * Double.valueOf(annualFeeAmount);
                        memberTotalAmount = (Double.valueOf(memberAgeFee) + Double.valueOf(memberTotalDue)) - Double.valueOf(memberTotalAdvance);

                        String memberFeeYearId = "20";
                        String memberFeeYearName = "Life";

                        feeYearObj = new JSONObject();

                        
                        
                        
                        
                        feeYearObj.put("Today", todayDate);
                        feeYearObj.put("TodayMonth", todayMonth);
                        feeYearObj.put("TodayYear", todayYear);
                        feeYearObj.put("TodayMonthCheck", todayMonthCheck);
                        feeYearObj.put("CurrentYearValue", currentYearValue);
                        feeYearObj.put("TodayMemberFeesYearNameId", todayMemberFeesYearNameId);
                        feeYearObj.put("TodayMemberFeesYearName", todayMemberFeesYearName);

                        feeYearObj.put("Id", memId);
                        feeYearObj.put("MemberTypeId", mMemberTypeId);
                        feeYearObj.put("MemberTypeName", mMemberTypeName);
                        feeYearObj.put("MemberTypeAgeLimit", mMemberTypeAgeLimit);
                        feeYearObj.put("MemberId", memberXIEBId);
                        feeYearObj.put("Name", memberXName);
                        feeYearObj.put("DateOfBirth", memberDob);
                        feeYearObj.put("Age", memberAge);
                        feeYearObj.put("AgeNextYear", memberAgeNextYear);
                        feeYearObj.put("AgeFee", memberAgeFee);
                        feeYearObj.put("TotalDue", memberTotalDue);
                        feeYearObj.put("TotalAdvance", memberTotalAdvance);
                        feeYearObj.put("TotalAmount", memberTotalAmount);
                        //   feeYearObj.put("AgeNextYear", memberAgeNextYear);

                        feeYearObj.put("FeeYearId", memberFeeYearId);
                        feeYearObj.put("FeeYearName", memberFeeYearName);

                        feeYearObjArr.add(feeYearObj);

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", feeYearObjArr);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
