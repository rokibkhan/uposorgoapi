/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.GlobalVariable;
import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.JSONObject;

/**
 *
 * @author Akter
 */
public class SendFirebasePushNotification {

    public static Logger logger = Logger.getLogger("SendSMS");
    private static String USER_AGENT = "Mozilla/5.0";

    @SuppressWarnings("empty-statement")
    public static String sendSMS(String msg, String receipentMobileNo) throws IOException, URISyntaxException {

//        String urlString = "http://www.bangladeshsms.com/smsapi?api_key=C20013475bbdd6cbdd2ac1.58430946&type=text&senderid=8804445629730&msg=test&contacts=+8801755569701";
        String sendUrlHost = GlobalVariable.sendUrlHost;
        String sendUrlApi = GlobalVariable.sendUrlApi;
        String apiKey = GlobalVariable.apiKey;
        String type = GlobalVariable.type;
        String senderid = GlobalVariable.senderid;
        String ret = "";

        try {

            URIBuilder builder = new URIBuilder()
                    .setScheme("http")
                    .setHost(sendUrlHost)
                    .setPath(sendUrlApi)
                    .addParameter("api_key", apiKey)
                    .addParameter("contacts", receipentMobileNo)
                    .addParameter("senderid", senderid)
                    .addParameter("type", type)
                    .addParameter("msg", msg);

            URL url = builder.build().toURL();

            try {
                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("HEAD");
                // optional default is GET
                con.setRequestMethod("GET");
                con.setConnectTimeout(5000); //set timeout to 5 seconds
                //add request header
                con.setRequestProperty("User-Agent", USER_AGENT);
                if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {

                    int responseCode = con.getResponseCode();
                    System.out.println("\nSending 'GET' request to URL : " + url);
                    System.out.println("Response Code : " + responseCode);
                    logger.info("Response Code : " + responseCode);

                    BufferedReader in;
                    in = new BufferedReader(new InputStreamReader(con.getInputStream()));

                    ret = in.readLine();
                    in.close();

                    logger.info("Response from SMS Server : " + ret);
                    System.out.println("Response from SMS Server  : " + ret);
                    return ret;
                } else {
                    return "sending failed!";
                }

            } catch (java.net.SocketTimeoutException e) {
                return "Exception or server unreachable";
            } catch (java.io.IOException e) {
                return "Exception or server unreachable";
            }

        } catch (IOException | URISyntaxException e) {
            return "Exception or server unreachable";
        }

    }

    /**
     * Topics Wise notification send.
     *
     * @param msgTitle represents {Notification title}
     * @param msgBody urepresents {Notification Body which contain all content
     * of notification}
     * @param receipentDeviceId represents {User device id}
     * @param msgPriority represents the value {high or normal}
     * @return an instance of { message Priority}
     * @throws IllegalStateException if an app with the same name has already
     * been initialized.
     */
    public static String sendSingleNotification(String msgTitle, String msgBody, String receipentDeviceId, String msgPriority) throws IOException, URISyntaxException {

        String ret = "";

        String fcmApiKey = GlobalVariable.GOOGLE_FCM_API_KEY;

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
        post.setHeader("Content-type", "application/json");

        post.setHeader("Authorization", "key=" + fcmApiKey + "");
        JSONObject message = new JSONObject();
        message.put("to", receipentDeviceId);
        message.put("priority", msgPriority);

        JSONObject notification = new JSONObject();
        notification.put("title", msgTitle);
        notification.put("body", msgBody);

        message.put("notification", notification);

        post.setEntity(new StringEntity(message.toString(), "UTF-8"));
        HttpResponse response = client.execute(post);
        System.out.println("FCM POST Message :: " + message);
        System.out.println("FCM Server response :: " + response);

        return msgPriority;

//        try {
//
//        } catch (IOException | URISyntaxException e) {
//            return "Exception or server unreachable";
//        }
    }
//eHaUvIcPrww:APA91bF9X3tT5IrpNlmv_wfCUeW5frygXAT487uf_sMTGfrosIKHIkC9YWs51lUb745RcKGiVfdWUHlamRAKLAd0AxTQC1HySEYuP8Fzn3XH0xgs7bPxe6nde6JoH14TpEU3sW-lbHA6
//    public static void main(String args[]) throws IOException, URISyntaxException {
//        String sms = "This is test Message";
//        String receipentMobileNo = "+8801955538279";
//        System.out.println("SMS : " + sms);
//        System.out.println("receipentMobileNo : " + receipentMobileNo);
//        System.out.println(SendSMS(sms, receipentMobileNo));
//    }
    //foxtcXDCdLE:APA91bEmDcR-bp2CZU3w83-WJjR8VRucqxgmFHO4Pgle829L4smBe-oKPAPOQM28tDazSoSTpEebie_LU89VfysOTtdLrzwzPwD2s-PNOD2WRtRyIqW1knyc0KgMVAJURpaXe0MxzwMP

    /**
     * Topics Wise notification send.
     *
     * @param msgTitle represents {Notification title}
     * @param msgBody urepresents {Notification Body which contain all content
     * of notification}
     * @param topicsName represents {Topics name where want to send
     * notification}
     * @param msgPriority represents the value {high or normal}
     * @return an instance of { message Priority}
     * @throws IllegalStateException if an app with the same name has already
     * been initialized.
     */
    public static String sendTopicsWiseNotification(String msgTitle, String msgBody, String topicsName, String msgPriority) throws IOException, URISyntaxException {

        String ret = "";
        String topics = "/topics/" + topicsName;

        String fcmApiKey = GlobalVariable.GOOGLE_FCM_API_KEY;

        HttpClient client = HttpClientBuilder.create().build();
        HttpPost post = new HttpPost("https://fcm.googleapis.com/fcm/send");
        post.setHeader("Content-type", "application/json");

        post.setHeader("Authorization", "key=" + fcmApiKey + "");
        JSONObject message = new JSONObject();
        //  message.put("to", receipentDeviceId);

        // json.put("to", "/topics/your-topic-name");
        message.put("to", topics);

        message.put("priority", msgPriority);

        JSONObject notification = new JSONObject();
        notification.put("title", msgTitle);
        notification.put("body", msgBody);

        message.put("notification", notification);

        post.setEntity(new StringEntity(message.toString(), "UTF-8"));
        HttpResponse response = client.execute(post);
        System.out.println("FCM POST Message :: " + message);
        System.out.println("FCM Server response :: " + response);

        return msgPriority;

//        try {
//
//        } catch (IOException | URISyntaxException e) {
//            return "Exception or server unreachable";
//        }
    }

}
