/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.appul.servlet;

import com.appul.util.GlobalVariable;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author Akter
 */
public class loginServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
           int errorCode =0;
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet loginServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet loginServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
            */

            UserBean user=new UserBean();
            user.setUserName(request.getParameter("username"));
            user.setPassword(request.getParameter("password"));
            user =UserCheck.login(user);
//            int storeId = 0;
            HttpSession session = request.getSession(true);
            if (user.isValid()) {
                
                session.setAttribute("username",request.getParameter("username"));
                session.setAttribute("logstat","Y");
                session.setAttribute("storeId",user.getStoreId());
                response.sendRedirect(GlobalVariable.baseUrl+"/home.jsp?sessionid="+session.getId());

            } else {
                errorCode = 1;
                session.setAttribute("rtnUser", errorCode);
                response.sendRedirect(GlobalVariable.baseUrl+"/userManagement/login.jsp");
               
            }        
        }  catch (Exception theException) {
            System.out.println(theException);
        } finally { 
            out.close();
        }
    
        
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
   
}