<%-- 
    Document   : postContentRequest
    Created on : APR 28, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    // DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatEvent = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat dateFormatEventM = new SimpleDateFormat("MMMM"); //October
    DateFormat dateFormatEventMM = new SimpleDateFormat("MMM"); //Oct
    DateFormat dateFormatEventD = new SimpleDateFormat("dd");
    DateFormat dateFormatEventY = new SimpleDateFormat("Y");
    Date dateEventM = null;
    Date dateEventMM = null;
    Date dateEventD = null;
    Date dateEventY = null;
    String newDateEventM = "";
    String newDateEventMM = "";
    String newDateEventD = "";
    String newDateEventY = "";

    Date todayDate = new Date();
    String today = dateFormatEvent.format(todayDate); //2016-11-16

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat parseFormat_YMDHMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat displayFormat_YMD_AM_PM = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    String adddate = "";
    adddate = dateFormatX.format(dateToday);

    String adddate_AMPM = displayFormat_YMD_AM_PM.format(dateToday);

    out.println("timeCheck API adddate :: dateToday ::" + adddate);
    out.println("timeCheck API adddate :: adddate_AMPM ::" + adddate_AMPM);

    Calendar calendar = new GregorianCalendar();
    TimeZone timeZone = calendar.getTimeZone();

    out.println("timeCheck API timeZone :: timeZone ::" + timeZone);

    String testDate = "2020-06-29 10:31:02";
    Date testDateX = parseFormat_YMDHMS.parse(testDate);
    String testDateXn = displayFormat_YMD_AM_PM.format(testDateX);
    out.println("timeCheck API testDate :" + testDate);
    out.println("timeCheck API testDateXn :" + testDateXn);

    String testDate2 = "2020-06-29 17:31:02";
    Date testDate2X = parseFormat_YMDHMS.parse(testDate2);
    String testDate2Xn = displayFormat_YMD_AM_PM.format(testDate2X);
    out.println("timeCheck API testDate2 :" + testDate2);
    out.println("timeCheck API testDate2Xn :" + testDate2Xn);

    String testDate3 = "2020-06-29 17:31:02";
    Date testDate3X = parseFormat_YMDHMS.parse(testDate3);
    String testDate3Xn = displayFormat_YMD_AM_PM.format(testDate3X);
    out.println("timeCheck API testDate3 :" + testDate3);
    out.println("timeCheck API testDate3Xn :" + testDate3Xn);

    String myTime = "14:10";
    SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    Date d = df.parse(myTime);
    Calendar cal = Calendar.getInstance();
    cal.setTime(d);
    cal.add(Calendar.MINUTE, 10);
    String newTime = df.format(cal.getTime());

    out.println("timeCheck API myTime :" + myTime);
    out.println("timeCheck API newTime1 :" + newTime);

    String myTime1 = "2020-06-29 17:31:02";
    //  SimpleDateFormat df = new SimpleDateFormat("HH:mm");
    Date myTime1X = parseFormat_YMDHMS.parse(myTime1);
    Calendar calXn = Calendar.getInstance();
    calXn.setTime(myTime1X);
    calXn.add(Calendar.MINUTE, 31);
    String newTime1Xn = displayFormat_YMD_AM_PM.format(calXn.getTime());

    out.println("timeCheck API myTime1 :" + myTime1);
    out.println("timeCheck API newTime1Xn :" + newTime1Xn);

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    out.println("timeCheck API ipAddress :" + ipAddress);
    out.println("timeCheck API hostname :" + hostname);
%>
