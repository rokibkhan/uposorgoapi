<%-- 
    Document   : patientQueryRequest
    Created on : MAY 05, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("patientQueryRequest_jsp.class");

    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    getRegistryID getId = new getRegistryID();

    String imageBase64String = "";
    // String filePath = GlobalVariable.imageUploadPath;
    //  public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";     //WINDOWS
    //  public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";   //SERVER
    //  String filePath = "D:/NetbeansDevelopment/FFMS/web/upload/";     //WINDOWS
    //  String filePath = "/app/FFMS/webapps/FFMS/upload/";     //SERVER
    //   String filePath = "/app/FFMS/webapps/FFMS/web/upload/";     //SERVER

    // public static final String baseUrlImg = "http://localhost:8084/IEBWEB";
    // public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";  //joybangla
    // public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";         //windows
    String filePath = GlobalVariable.imageUploadPath + "patientcontent/";

    logger.info("patientQueryRequest API filePath :" + filePath);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String haveAttachment = ""; //1== yes 0==No attachment    
    String patinetContentPicture = "";
    String patinetContentPictureLink = "";
    String pQueryDesc = "";
    String pQueryProblemCategoryId = "";
    String pQueryProblemCategoryName = "";
    String pQueryDate = "";
    String pQueryStatus = "0"; //0->UnAnswer;1->Answer
    String pQueryReplyStatus = "0"; //0- No reply required;1->After Answered now reply required
    String pQuerySufferingDate = "";

    int pQueryParent = 0;

    String fever = "";
    String cough = "";
    String tiredness = "";
    String nasalcongestion = "";
    String headache = "";

    String feverText = "";
    String coughText = "";
    String tirednessText = "";
    String nasalcongestionText = "";
    String headacheText = "";

    String patientName = "";
    String patientEmail = "";
    String patientPhone = "";
    String patientDOB = "";
    String patientGender = "";

    String pQueryCommentType = "";
    String pQueryCommenterId = "";

    String adddate = "";

    String doctorId = "";
    String doctorTimeslotId = "";
    String serialCountMax = "";
    String serialCountCurrent = "";
    String serialBaseTime = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("haveAttachment")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        doctorId = request.getParameter("doctorId").trim();
        doctorTimeslotId = request.getParameter("doctorTimeslotId").trim();

        haveAttachment = request.getParameter("haveAttachment").trim();
        pQueryProblemCategoryId = request.getParameter("pQueryProblemCategoryId") == null ? "" : request.getParameter("pQueryProblemCategoryId").trim();
        pQueryDesc = request.getParameter("pQueryDesc") == null ? "" : request.getParameter("pQueryDesc").trim();
        pQuerySufferingDate = request.getParameter("pQuerySufferingDate") == null ? "" : request.getParameter("pQuerySufferingDate").trim();

        patientName = request.getParameter("patientName") == null ? "" : request.getParameter("patientName").trim();
        patientEmail = request.getParameter("patientEmail") == null ? "" : request.getParameter("patientEmail").trim();
        patientPhone = request.getParameter("patientPhone") == null ? "" : request.getParameter("patientPhone").trim();
        patientDOB = request.getParameter("patientDOB") == null ? "" : request.getParameter("patientDOB").trim();
        patientGender = request.getParameter("patientGender") == null ? "" : request.getParameter("patientGender").trim();

        imageBase64String = request.getParameter("imageString") == null ? "" : request.getParameter("imageString");
        //imageBase64String = request.getParameter("imageString");
        logger.info("patientQueryRequest API  imageBase64String :" + imageBase64String);

        System.out.println("patientQueryRequest API imageBase64String :" + imageBase64String);

        fever = request.getParameter("fever") == null ? "" : request.getParameter("fever").trim();
        cough = request.getParameter("cough") == null ? "" : request.getParameter("cough").trim();
        tiredness = request.getParameter("tiredness") == null ? "" : request.getParameter("tiredness").trim();
        nasalcongestion = request.getParameter("nasalcongestion") == null ? "" : request.getParameter("nasalcongestion").trim();
        headache = request.getParameter("headache") == null ? "" : request.getParameter("headache").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("patientQueryRequest API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    Query patientInfoAddSQL = null;
    Query patientCovidInfoAddSQL = null;

    Query doctorAppointmentInfoAddSQL = null;

    if (rec == 1) {

        try {

            if (haveAttachment.equals("1")) {

                Random random = new Random();
                int rand1 = Math.abs(random.nextInt());
                int rand2 = Math.abs(random.nextInt());
                DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
                Date dateFileToday = new Date();
                String filePrefixToday = formatterFileToday.format(dateFileToday);

                String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

                logger.info("patientQueryRequest API filePath :" + filePath);
                logger.info("patientQueryRequest API fileName1 :" + fileName1);

                System.out.println("patientQueryRequest API filePath :" + filePath);
                System.out.println("patientQueryRequest API fileName1 :" + fileName1);

                // contentPostPictureLink = GlobalVariable.baseUrl + "/upload/" + fileName1;
                patinetContentPictureLink = GlobalVariable.baseUrlImg + "/upload/patientcontent/" + fileName1;

                /*
                * Converting a Base64 String into Image byte array 
                 */
                // byte[] imageByteArray = Base64.decodeBase64(imageBase64String);
                byte[] imageByteArray = Base64.getDecoder().decode(imageBase64String);

                /*
                  * Write a image byte array into file system  
                 */
                //  FileOutputStream imageOutFile = new FileOutputStream("/Users/jeeva/Pictures/wallpapers/water-drop-after-convert.jpg");
                FileOutputStream imageOutFile = new FileOutputStream(filePath + fileName1);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();

                patinetContentPicture = fileName1;

            }

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String idS = getId.getID(70);
                        int pQueryId = Integer.parseInt(idS);

                        String idAappS = getId.getID(47); //TEMP_PROPOSER_ID
                        int pAppointmentId = Integer.parseInt(idAappS);

                        //  String idPatient = getId.getID(71);
                        //  int pQueryPatientId = Integer.parseInt(idPatient);
                        //      int pQueryPatientId = 1;
                        int pQueryPatientId = pQueryId;

                        //    doctorTimeslotId
                        InetAddress ip;
                        String hostname = "";
                        String ipAddress = "";
                        try {
                            ip = InetAddress.getLocalHost();
                            hostname = ip.getHostName();
                            ipAddress = ip.getHostAddress();

                        } catch (UnknownHostException e) {

                            e.printStackTrace();
                        }

                        String adduser = Integer.toString(memId);
                        adddate = dateFormatX.format(dateToday);
                        String addterm = hostname;
                        String published = "1";

                        pQueryDate = dateFormatX.format(dateToday);

                        DateFormat dateFormatXnc = new SimpleDateFormat("yyyy-MM-dd");
                        Date pQuerySufferingDate1 = dateFormatXnc.parse(pQuerySufferingDate);

                        patientInfoAddSQL = dbsession.createSQLQuery("INSERT INTO patient_info("
                                + "patient_id,"
                                + "patient_name,"
                                + "patient_email,"
                                + "patient_phone,"
                                + "patient_dob,"
                                + "patient_gender,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip"
                                + ") VALUES("
                                + "'" + pQueryPatientId + "',"
                                + "'" + patientName + "',"
                                + "'" + patientEmail + "',"
                                + "'" + patientPhone + "',"
                                + "'" + patientDOB + "',"
                                + "'" + patientGender + "',"
                                + "'" + adduser + "',"
                                + "'" + adddate + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "'"
                                + "  ) ");

                        logger.info("patientInfoAddSQL ::" + patientInfoAddSQL);

                        patientInfoAddSQL.executeUpdate();

                        //prepare Statement
                        PreparedStatement ps = con.prepareStatement("INSERT INTO patient_query("
                                + "query_id,"
                                + "parent,"
                                + "patient_id,"
                                + "problem_category,"
                                + "query_desc,"
                                + "suffering_date,"
                                + "query_date,"
                                + "commenter_id,"
                                + "member_id,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip,"
                                + "mod_date,"
                                + "attachment, "
                                + "assign_doctor) "
                                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                        //  ps.setInt(1, Integer.parseInt(postContentRequestId));
                        ps.setInt(1, pQueryId);
                        ps.setInt(2, pQueryParent);
                        ps.setInt(3, pQueryPatientId);
                        ps.setInt(4, Integer.parseInt(pQueryProblemCategoryId));
                        ps.setString(5, pQueryDesc);
                        ps.setString(6, pQuerySufferingDate);
                        ps.setString(7, adddate);
                        ps.setInt(8, memId);
                        ps.setInt(9, memId);

                        ps.setString(10, adduser);
                        ps.setString(11, adddate);
                        ps.setString(12, addterm);
                        ps.setString(13, ipAddress);
                        ps.setString(14, adddate);
                        ps.setString(15, patinetContentPicture);
                        ps.setInt(16, Integer.parseInt(doctorId));

                        logger.info("SQL Query: " + ps.toString());

                        ps.executeUpdate();

                        patientCovidInfoAddSQL = dbsession.createSQLQuery("INSERT INTO patient_query_covid("
                                + "covid_qry_id,"
                                + "query_id,"
                                + "fever,"
                                + "cough,"
                                + "tiredness,"
                                + "nasalcongestion,"
                                + "headache,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip"
                                + ") VALUES("
                                + "'" + pQueryPatientId + "',"
                                + "'" + pQueryPatientId + "',"
                                + "'" + fever + "',"
                                + "'" + cough + "',"
                                + "'" + tiredness + "',"
                                + "'" + nasalcongestion + "',"
                                + "'" + headache + "',"
                                + "'" + adduser + "',"
                                + "'" + adddate + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "'"
                                + "  ) ");

                        logger.info("patientCovidInfoAddSQL ::" + patientCovidInfoAddSQL);

                        patientCovidInfoAddSQL.executeUpdate();

                        //set Appointment Time use by serialcount and max serial count
                        String appointmentTime = "3.10 PM";
                        String appointmentMsg = "";
                        String appointmentDoctorMsg = "";

                        doctorAppointmentInfoAddSQL = dbsession.createSQLQuery("INSERT INTO patient_appointment("
                                + "id,"
                                + "member_id,"
                                + "patient_id,"
                                + "doctor_timeslot_id,"
                                + "appointment_time,"
                                + "appointment_msg,"
                                + "doctor_msg,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip"
                                + ") VALUES("
                                + "'" + pAppointmentId + "',"
                                + "'" + doctorId + "',"
                                + "'" + pQueryPatientId + "',"
                                + "'" + doctorTimeslotId + "',"
                                + "'" + appointmentTime + "',"
                                + "'" + appointmentMsg + "',"
                                + "'" + appointmentDoctorMsg + "',"
                                + "'" + adduser + "',"
                                + "'" + adddate + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "'"
                                + "  ) ");

                        logger.info("doctorAppointmentInfoAddSQL ::" + doctorAppointmentInfoAddSQL);

                        doctorAppointmentInfoAddSQL.executeUpdate();

                        ps.close();
                        con.commit();
                        con.close();

//                        postContentObj = new JSONObject();
//                        postContentObj.put("ContentRequestId", postContentRequestId);
//                        postContentObj.put("ContentType", postContentType);
//                        postContentObj.put("ContentCategory", postContentCategory);
//                        postContentObj.put("ContentTitle", postContentTitle);
//                        postContentObj.put("ContentShortDesc", postContentShortDesc);
//                        postContentObj.put("ContentDesc", postContentDesc);
//                        postContentObj.put("ContentPostLink", postContentPostLink);
//                        postContentObj.put("ContentPicture", postContentPicture);
//                        postContentObj.put("ContentPictureLink", contentPostPictureLink);
                        postContentObj = new JSONObject();
                        postContentObj.put("pQueryId", pQueryId);

                        postContentObj.put("patientId", pQueryPatientId);
                        postContentObj.put("patientName", patientName);
                        postContentObj.put("patientEmail", patientEmail);
                        postContentObj.put("patientPhone", patientPhone);
                        postContentObj.put("patientDOB", patientDOB);
                        postContentObj.put("patientGender", patientGender);

                        postContentObj.put("pQueryParent", pQueryParent);
                        postContentObj.put("pQueryPatientId", pQueryPatientId);

                        postContentObj.put("pQueryProblemCategoryId", pQueryProblemCategoryId);
                        postContentObj.put("pQueryProblemCategoryName", pQueryProblemCategoryName);

                        postContentObj.put("pQueryDesc", pQueryDesc);
                        postContentObj.put("pQuerySufferingDate", pQuerySufferingDate);
                        postContentObj.put("pQueryDate", pQueryDate);
                        postContentObj.put("pQueryStatus", pQueryStatus);
                        postContentObj.put("pQueryReplyStatus", pQueryReplyStatus);
                        postContentObj.put("pQueryCommentType", pQueryCommentType);
                        postContentObj.put("pQueryCommenterId", pQueryCommenterId);

                        postContentObj.put("fever", fever);
                        postContentObj.put("feverText", feverText);

                        postContentObj.put("cough", cough);
                        postContentObj.put("coughText", coughText);

                        postContentObj.put("tiredness", tiredness);
                        postContentObj.put("tirednessText", tirednessText);

                        postContentObj.put("nasalcongestion", nasalcongestion);
                        postContentObj.put("nasalcongestionText", nasalcongestionText);

                        postContentObj.put("headache", headache);
                        postContentObj.put("headacheText", headacheText);

                        postContentObj.put("attachment", patinetContentPicture);
                        postContentObj.put("attachmentLink", patinetContentPictureLink);

                        // postContentObj.put("queryOwnerId", queryOwnerId);
                        //   postContentObj.put("queryOwnerName", queryOwnerName);
                        //  postContentObj.put("queryOwnerPic", queryOwnerPicLink);
                        //    postContentObjArr.add(postContentObj);
                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Patient query added successfully");
                        logingObj.put("ResponseData", postContentObj);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
