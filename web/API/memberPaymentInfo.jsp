<%-- 
    Document   : memberPaymentInfo
    Created on : NOV 08, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("memberPaymentInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberPaymentInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject approvalRequestObj = new JSONObject();
    JSONArray approvalRequestObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String transactionId = "";
    String transactionDate = "";
    String transactionStatus = "";
    String transactionStatusText = "";
    String billAmount = "";
    String referenceNo = "";
    String referenceDesc = "";
    String transactionType = "";
    String paidDate = "";
    String dueDate = "";
    String feeYear = "";
    String paymentOption = "";

    if (rec == 1) {

        try {
            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String dueSumSQL = "select sum(amount) from member_fee  WHERE  member_id=" + memId + " AND status ='0'";

                        String totalPaymentDue = dbsession.createSQLQuery(dueSumSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(dueSumSQL).uniqueResult().toString();
                        //   totalPaymentDue = Integer.parseInt(totalPaymentDue1);

                        logger.info("memberPaymentInfo API :: totalPaymentDue :: " + totalPaymentDue);

                        //   logger.info("memberPaymentInfo API OK");
                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();
                        if (!filter.equals("")) {
                            filterSQLStr = " AND mt.member_name LIKE '%" + filter + "%' ";
                        } else {
                            filterSQLStr = "";
                        }

//                        pContentCountSQL = "SELECT  count(*) FROM post_request pn "
//                                + "WHERE pn.published = 1  "
//                                + " " + filterSQLStr + " "
//                                + "ORDER BY pn.id_request_content DESC";
                        pContentCountSQL = "SELECT count(*) FROM member_fee WHERE member_id ='" + memId + "' "
                                + " ORDER BY txn_id DESC ";

                        //    logger.info("memberPaymentInfo API OK :: pContentCountSQL ::" + pContentCountSQL);
                        String numrow1 = dbsession.createSQLQuery(pContentCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(pContentCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        // number of rows to show per page
                        int rowsperpage = 10;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;

                        logger.info("memberPaymentInfo API::  currentpage : " + currentpage + " offset : " + offset + "");

                        pContentSQL = "SELECT * FROM member_fee WHERE member_id ='" + memId + "' "
                                + "ORDER BY txn_id DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        logger.info("memberPaymentInfo API pContentSQL ::" + pContentSQL);

                        pContentSQLQry = dbsession.createSQLQuery(pContentSQL);

                        for (Iterator pContentItr = pContentSQLQry.list().iterator(); pContentItr.hasNext();) {

                            pContentObj = (Object[]) pContentItr.next();
                            transactionId = pContentObj[0].toString();

                            referenceNo = pContentObj[2] == null ? "" : pContentObj[2].toString();
                            referenceDesc = pContentObj[3] == null ? "" : pContentObj[3].toString();

                            transactionDate = pContentObj[4] == null ? "" : pContentObj[4].toString();

                            paidDate = pContentObj[5] == null ? "" : pContentObj[5].toString();
                            feeYear = pContentObj[6] == null ? "" : pContentObj[6].toString();
                            billAmount = pContentObj[7] == null ? "0" : pContentObj[7].toString();

                            transactionStatus = pContentObj[8] == null ? "" : pContentObj[8].toString();

                            if (transactionStatus.equals("0")) {
                                transactionStatusText = "Due";
                            }
                            if (transactionStatus.equals("1")) {
                                transactionStatusText = "Paid";
                            }
                            if (transactionStatus.equals("2")) {
                                transactionStatusText = "Fail";
                            }

                            dueDate = pContentObj[9] == null ? "" : pContentObj[9].toString();
                            transactionType = pContentObj[10] == null ? "" : pContentObj[10].toString();
                            paymentOption = pContentObj[15] == null ? "" : pContentObj[15].toString();

                            approvalRequestObj = new JSONObject();
                            approvalRequestObj.put("TransactionId", transactionId);
                            approvalRequestObj.put("BillAmount", billAmount);
                            approvalRequestObj.put("TransactionStatus", transactionStatus);
                            approvalRequestObj.put("TransactionStatusText", transactionStatusText);
                            approvalRequestObj.put("TransactionDate", transactionDate);
                            approvalRequestObj.put("ReferenceNo", referenceNo);
                            approvalRequestObj.put("ReferenceDesc", referenceDesc);
                            approvalRequestObj.put("TransactionType", transactionType);
                            approvalRequestObj.put("PaidDate", paidDate);
                            approvalRequestObj.put("DueDate", dueDate);
                            approvalRequestObj.put("PaymentOption", paymentOption);
                            approvalRequestObj.put("FeeYear", feeYear);

                            approvalRequestObjArr.add(approvalRequestObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", approvalRequestObjArr);
                        logingObj.put("TotalPaymentDue", totalPaymentDue);
                        logingObj.put("TotalData", numrows);
                        logingObj.put("TotalPage", (int) totalpages);
                        logingObj.put("CurrentPage", currentpage);
                        logingObj.put("RowPerPage", rowsperpage);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        //finally {
        //    dbtrx.commit();
        //   }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
