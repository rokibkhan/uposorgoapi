<%-- 
    Document   : doctorAppointmentScheduleDelete
    Created on : Oct 20, 2020, 2:32:27 PM
    Author     : ROKIB
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>

<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorAppointmentScheduleDelete_jsp.class");
    System.out.println("doctorAppointmentScheduleDelete_jsp");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String doctorCategoryId = "";

    String doctorId = "";
    String scheduleSlotId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId")) {
     if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("doctorId") && request.getParameterMap().containsKey("scheduleSlotId") ) {
     //if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("doctorId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("mId")) {
        key = request.getParameter("key").trim();

        //  memberId = request.getParameter("memberId").trim();
        //   givenPassword = request.getParameter("password").trim();
        doctorId = request.getParameter("doctorId").trim();
        scheduleSlotId = request.getParameter("scheduleSlotId").trim();

        System.out.println("doctorId :: " + doctorId);
        System.out.println("scheduleSlotId :: " + scheduleSlotId);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    Query memberSQL = null;
    Object memberObj[] = null;
    
    Query doctorscheduleSQL = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    String memberXId = "";
    String appointmentScheduleId = "";
    String appointmentScheduleDate = "";
    
    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: doctorAppointmentScheduleDateWise API rec :: " + rec);

    if (rec == 1) {

        try {

            doctorscheduleSQL = dbsession.createSQLQuery("SELECT das.id,dpa.status  "                    
                    + "FROM  doctor_aponmnt_slot das join doctor_patient_aponmnt  dpa "
                    + " on dpa.aponmnt_slot_id = das.id "
                    + " WHERE  "                    
                    + " das.id ='" + scheduleSlotId + "' and"
                    + " dpa.status not in (2,4)");
                    
            
            if(doctorscheduleSQL.list().isEmpty())
            {
                
                
            memberSQL = dbsession.createSQLQuery("SELECT das.id "                    
                    + "FROM  doctor_aponmnt_slot das  "
                    
                    + "WHERE  "                    
                    + " das.id ='" + scheduleSlotId + "' ");
            //   memberTypeSQL = dbsession.createSQLQuery("SELECT * FROM doctor_category_name ORDER BY name ASC");
            if (!memberSQL.list().isEmpty()) {
                
                
                String adduser = doctorId;
                String adddate = dateFormatX.format(dateToday);
                
                
                 
                Query updateDoctorTimeSlotStatusSQL = dbsession.createSQLQuery("UPDATE  doctor_aponmnt_slot SET status = 4,mod_user='" + adduser + "', mod_date='" + adddate + "'  WHERE id = '" + scheduleSlotId + "'");
                updateDoctorTimeSlotStatusSQL.executeUpdate(); 
                                                

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Deleted Successfully");
                //responseDataObj.put("ResponseData", memberContentObjArr);
                
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                //responseDataObj.put("ResponseData", memberContentObjArr);

            }
            }
            else
            {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "Patient already appointed for slot");    
            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

       finally {
        dbtrx.commit();

        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
