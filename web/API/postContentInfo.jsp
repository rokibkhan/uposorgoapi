<%-- 
    Document   : postContentInfo
    Created on : APR 28, 2020, 10:23:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("postContentInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);
    
    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }

    // Consumer consumer = null;
    Member member = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    int memberEducationId = 0;

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: postContentInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String contentId = "";
    String contentCategory = "";
    String contentType = "";
    String contentTitle = "";
    String contentShortDesc = "";
    String contentDesc = "";
    String contentDate = "";
    String contentPostLink = "";
    String contentPostPicture = "";
    String contentPostShare = "";

    String pictureOpt = "";
    String videoOpt = "";

    String contentOwnerId = "";
    String memberIEBId = "";
    String memberIEBIdFirstChar = "";
    String memberPictureName = "";
    String memberPictureLink = "";
    String postDiseaseCategoryName = "";

    String ownContentFlag = "";

    // String contentTotalLikeCount = "";
    //  String contentTotalCommentCount = "";
    //  String contentTotalShareCount = "";
    Query mPostContentRatingPointSQL = null;
    Object[] mPostContentRatingPointObj = null;

    String totalLikeCount = "0";
    String totalLikeCountText = "";
    String totalCommentCount = "0";
    String totalShareCount = "0";

    String contentLiked = "";
    String contentLikedText = "";

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            //  qMember = dbsession.createQuery(" from Member where mobile = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();

                    memId = member.getId();

                    Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String filter = request.getParameter("filter") == null ? "" : request.getParameter("filter").trim();
                        if (!filter.equals("")) {
                            filterSQLStr = " AND pc.post_content_title LIKE '%" + filter + "%' ";
                        } else {
                            filterSQLStr = "";
                        }

                        pContentCountSQL = "SELECT  count(*) FROM post_content pc "
                                + "LEFT JOIN member AS m ON m.id = pc.content_owner "
                                + "WHERE pc.published = 1  "
                                + " " + filterSQLStr + " "
                                + "ORDER BY pc.id_post_content DESC";

                        String numrow1 = dbsession.createSQLQuery(pContentCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(pContentCountSQL).uniqueResult().toString();
                        int numrows = Integer.parseInt(numrow1);

                        // number of rows to show per page
                        int rowsperpage = 10;
                        // find out total pages
                        double totalpages = Math.ceil((double) numrows / rowsperpage);

                        // get the current page or set a default         
                        String currentpage1 = request.getParameter("currentpage") == null ? "" : request.getParameter("currentpage").trim();

                        int currentpage = 1;
                        if (!currentpage1.equals("")) {
                            currentpage = Integer.parseInt(currentpage1);
                        } else {
                            currentpage = 1; // default page number
                        }

                        // if current page is greater than total pages
                        if (currentpage > totalpages) {
                            // set current page to last page
                            currentpage = (int) totalpages;
                        }
                        // if current page is less than first page
                        if (currentpage < 1) {
                            // set current page to first page
                            currentpage = 1;
                        }

                        // the offset of the list, based on current page
                        int offset = (currentpage - 1) * rowsperpage;

                        pContentSQL = "SELECT  pc.id_post_content, pc.content_category, pc.post_content_type,pc.post_content_title,"
                                + "pc.content_short_desc,pc.content_desc,pc.content_date,pc.post_link,pc.picture,pc.share, pc.content_owner, "
                                + "m.member_id,m.member_name  contentOwnerName, m.picture_name contentOwnerPic, d.name  "
                                + "FROM post_content pc "
                                + "LEFT JOIN member AS m ON m.id = pc.content_owner "
                                + "LEFT JOIN doctor_category_name AS d ON d.id = pc.disease_category_id "
                                + "WHERE pc.published = 1  "
                                + " " + filterSQLStr + " "
                                + "ORDER BY pc.id_post_content DESC "
                                + "LIMIT " + offset + " , " + rowsperpage + " ";

                        pContentSQLQry = dbsession.createSQLQuery(pContentSQL);

                        for (Iterator pContentItr = pContentSQLQry.list().iterator(); pContentItr.hasNext();) {

                            pContentObj = (Object[]) pContentItr.next();
                            contentId = pContentObj[0].toString();
                            contentCategory = pContentObj[1] == null ? "" : pContentObj[1].toString();

                            contentType = pContentObj[2] == null ? "" : pContentObj[2].toString();

                            contentTitle = pContentObj[3] == null ? "" : pContentObj[3].toString();
                            contentShortDesc = pContentObj[4] == null ? "" : pContentObj[4].toString();
                            contentDesc = pContentObj[5] == null ? "" : pContentObj[5].toString();
                            contentDate = pContentObj[6] == null ? "" : pContentObj[6].toString();
                            contentPostLink = pContentObj[7] == null ? "" : pContentObj[7].toString();
                            contentPostPicture = pContentObj[8] == null ? "" : pContentObj[8].toString();
                            contentPostShare = pContentObj[9] == null ? "" : pContentObj[9].toString();

                            contentOwnerId = pContentObj[10] == null ? "" : pContentObj[10].toString();
                            memberIEBId = pContentObj[11] == null ? "" : pContentObj[11].toString();

                            memberName = pContentObj[12] == null ? "" : pContentObj[12].toString();

                            memberPictureName = pContentObj[13] == null ? "" : pContentObj[13].toString();
                            
                            postDiseaseCategoryName = pContentObj[14] == null ? "" : pContentObj[14].toString();

                            memberPictureLink = GlobalVariable.imageMemberDirLink + memberPictureName;

                            mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + contentOwnerId + "' AND SYSDATE() between mt.ed and mt.td");

                            for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

                                mMemberTypeObj = (Object[]) mMemberTypeItr.next();
                                mMemberTypeId = mMemberTypeObj[0].toString();
                                mMemberTypeName = mMemberTypeObj[1].toString();

                            }

                            mPostContentRatingPointSQL = dbsession.createSQLQuery("SELECT * FROM post_content_rating_point WHERE content_id = '" + contentId + "'");

                            for (Iterator mPostContentRatingPointItr = mPostContentRatingPointSQL.list().iterator(); mPostContentRatingPointItr.hasNext();) {

                                mPostContentRatingPointObj = (Object[]) mPostContentRatingPointItr.next();
                                totalLikeCount = mPostContentRatingPointObj[2].toString();
                                totalCommentCount = mPostContentRatingPointObj[3].toString();
                                totalShareCount = mPostContentRatingPointObj[4].toString();

                            }

                            JSONObject commentInfoResponseObj = new JSONObject();
                            JSONArray commentInfoObjArray = new JSONArray();

                            if (!totalCommentCount.equalsIgnoreCase("0")) {

                                String commentSQL = null;
                                Query commentSQLQry = null;
                                Object[] commentObj = null;

                                String commentId = "";
                                String commentParent = "";
                                String commmentDesc = "";
                                String commmentDate = "";
                                String commmentPinedStatus = "";
                                String commmentCustomOrder = "";

                                String commmenterId = "";
                                String commmenterName = "";
                                String commmenterPicture = "";
                                String commmenterPictureLink = "";
                                String commmenterType = "";
                                String commmenterTypeName = "";

                                String commentLikeCount = "";
                                String commentLiked = "";
                                String commentLikedText = "";
                                String commentIsAccepted = "";

                                //  SELECT * FROM  post_content_comment
                                commentSQL = "SELECT cm.comment_id,cm.parent,cm.commment_desc, "
                                        + "cm.commment_date,cm.pined, m.id, m.member_name,m.picture_name, "
                                        + " mt.member_type_id,mti.member_type_name, "
                                        + "m.mobile, m.phone1, m.phone2, m.email_id , "
                                        + "cm.attachment,cm.attachment_thumb,"
                                        + "cl.like_count,    "
                                        + "cm.isaccepted   "
                                        + "FROM post_content_comment cm "
                                        + "LEFT JOIN post_content_comment_like AS cl ON cl.comment_id = cm.comment_id  "
                                        + "LEFT JOIN member AS m ON m.id = cm.member_id  "
                                        + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                        + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                                        + "WHERE cm.status = 1 AND cm.parent = 0 AND cm.content_id = '" + contentId + "'  "
                                        + "ORDER BY cm.custom_order ASC "
                                        + "LIMIT 0 , 5 ";

                                System.out.println("commentSQL :: " + commentSQL);

                                commentSQLQry = dbsession.createSQLQuery(commentSQL);
                                if (!commentSQLQry.list().isEmpty()) {
                                    for (Iterator commentItr = commentSQLQry.list().iterator(); commentItr.hasNext();) {

                                        commentObj = (Object[]) commentItr.next();

                                        commentId = commentObj[0].toString();
                                        commentParent = commentObj[1].toString();
                                        commmentDesc = commentObj[2] == null ? "" : commentObj[2].toString();
                                        commmentDate = commentObj[3] == null ? "" : commentObj[3].toString();
                                        commmentPinedStatus = commentObj[4] == null ? "" : commentObj[4].toString();

                                        commmenterId = commentObj[5] == null ? "" : commentObj[5].toString();
                                        commmenterName = commentObj[6] == null ? "" : commentObj[6].toString();

                                        commmenterPicture = commentObj[7] == null ? "" : commentObj[7].toString();
                                        commmenterPictureLink = GlobalVariable.imageMemberDirLink + commmenterPicture;

                                        commmenterType = commentObj[8] == null ? "" : commentObj[8].toString();
                                        commmenterTypeName = commentObj[9] == null ? "" : commentObj[9].toString();

                                        commentLikeCount = commentObj[16] == null ? "0" : commentObj[16].toString();
                                        
                                        commentIsAccepted = commentObj[17].toString();

                                        JSONObject commentChildInfoResponseObj = new JSONObject();
                                        JSONArray commentChildInfoObjArray = new JSONArray();
                                        String commentChildSQL = null;
                                        Query commentChildSQLQry = null;
                                        Object[] commentChildObj = null;

                                        String commentChildId = "";
                                        String commentChildParent = "";
                                        String commmentChildDesc = "";
                                        String commmentChildDate = "";
                                        String commmentChildPinedStatus = "";
                                        String commmentChildCustomOrder = "";

                                        String commmenterChildId = "";
                                        String commmenterChildName = "";
                                        String commmenterChildPicture = "";
                                        String commmenterChildPictureLink = "";
                                        String commmenterChildType = "";
                                        String commmenterChildTypeName = "";

                                        String commentChildLikeCount = "";
                                        String commentChildLiked = "";
                                        String commentChildLikedText = "";
                                        String commentChildIsAccepted = "";

                                        //check child comment
                                        commentChildSQL = "SELECT cm.comment_id,cm.parent,cm.commment_desc, "
                                                + "cm.commment_date,cm.pined, m.id, m.member_name,m.picture_name, "
                                                + " mt.member_type_id,mti.member_type_name, "
                                                + "m.mobile, m.phone1, m.phone2, m.email_id , "
                                                + "cm.attachment,cm.attachment_thumb,"
                                                + "cl.like_count,    "
                                                + "cm.isaccepted   "
                                                + "FROM post_content_comment cm "
                                                + "LEFT JOIN post_content_comment_like AS cl ON cl.comment_id = cm.comment_id  "
                                                + "LEFT JOIN member AS m ON m.id = cm.member_id  "
                                                + "LEFT JOIN member_type AS mt ON mt.member_id = m.id  "
                                                + "LEFT JOIN member_type_info AS mti ON mti.member_type_id = mt.member_type_id "
                                                + "WHERE cm.status = 1 AND cm.parent = '" + commentId + "' AND cm.content_id = '" + contentId + "'  "
                                                + "ORDER BY cm.custom_order ASC "
                                                + "LIMIT 0 , 5 ";

                                        System.out.println("commentChildSQL :: " + commentChildSQL);

                                        commentChildSQLQry = dbsession.createSQLQuery(commentChildSQL);
                                        if (!commentChildSQLQry.list().isEmpty()) {
                                            for (Iterator commentChildItr = commentChildSQLQry.list().iterator(); commentChildItr.hasNext();) {

                                                commentChildObj = (Object[]) commentChildItr.next();

                                                commentChildId = commentChildObj[0].toString();
                                                commentChildParent = commentChildObj[1].toString();
                                                commmentChildDesc = commentChildObj[2] == null ? "" : commentChildObj[2].toString();
                                                commmentChildDate = commentChildObj[3] == null ? "" : commentChildObj[3].toString();
                                                commmentChildPinedStatus = commentChildObj[4] == null ? "" : commentChildObj[4].toString();

                                                commmenterChildId = commentChildObj[5] == null ? "" : commentChildObj[5].toString();
                                                commmenterChildName = commentChildObj[6] == null ? "" : commentChildObj[6].toString();

                                                commmenterChildPicture = commentChildObj[7] == null ? "" : commentChildObj[7].toString();
                                                commmenterChildPictureLink = GlobalVariable.imageMemberDirLink + commmenterChildPicture;

                                                commmenterChildType = commentChildObj[8] == null ? "" : commentChildObj[8].toString();
                                                commmenterChildTypeName = commentChildObj[9] == null ? "" : commentChildObj[9].toString();

                                                commentChildLikeCount = commentChildObj[16] == null ? "0" : commentChildObj[16].toString();
                                                commentChildIsAccepted = commentObj[17].toString();

                                                //check user rating history point already exits or not
                                                String commentChildLikedHostorySQL = "SELECT  count(*) FROM post_content_comment_like_history  "
                                                        + "WHERE member_id = '" + memId + "' AND comment_id = '" + commentChildId + "'";

                                                String commentChildLikedHostoryRows = dbsession.createSQLQuery(commentChildLikedHostorySQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(commentChildLikedHostorySQL).uniqueResult().toString();
                                                //   int numrows = Integer.parseInt(numrow1);

                                                logger.info("commentChildLikedHostoryRows ::" + commentChildLikedHostoryRows);
                                                System.out.println("commentChildLikedHostoryRows ::" + commentChildLikedHostoryRows);

                                                if (commentChildLikedHostoryRows.equals("0")) {
                                                    commentChildLiked = "0";
                                                    commentChildLikedText = "No";
                                                } else {
                                                    commentChildLiked = "1";
                                                    commentChildLikedText = "Yes";
                                                }

                                                commentChildInfoResponseObj = new JSONObject();
                                                commentChildInfoResponseObj.put("commentChildId", commentChildId);

                                                commentInfoResponseObj.put("commentChildLikeCount", commentChildLikeCount);
                                                commentInfoResponseObj.put("commentChildLiked", commentChildLiked);
                                                commentInfoResponseObj.put("commentChildLikedText", commentChildLikedText);
                                                commentInfoResponseObj.put("commentChildIsAccepted", commentChildIsAccepted);

                                                commentChildInfoResponseObj.put("commentChildParent", commentChildParent);
                                                commentChildInfoResponseObj.put("commmentChildDesc", commmentChildDesc);
                                                commentChildInfoResponseObj.put("commmentChildDate", commmentChildDate);
                                                commentChildInfoResponseObj.put("commmentChildPinedStatus", commmentChildPinedStatus);

                                                commentChildInfoResponseObj.put("commmenterChildId", commmenterChildId);
                                                commentChildInfoResponseObj.put("commmenterChildName", commmenterChildName);
                                                commentChildInfoResponseObj.put("commmenterChildPicture", commmenterChildPicture);
                                                commentChildInfoResponseObj.put("commmenterChildPictureLink", commmenterChildPictureLink);
                                                commentChildInfoResponseObj.put("commmenterChildType", commmenterChildType);
                                                commentChildInfoResponseObj.put("commmenterChildTypeName", commmenterChildTypeName);

                                                commentChildInfoObjArray.add(commentChildInfoResponseObj);

                                            }
                                        } else {

                                            commentChildInfoObjArray = new JSONArray();
                                        }

                                        //check post_content_comment_like_history point already exits or not
                                        String commentLikedHostorySQL = "SELECT  count(*) FROM post_content_comment_like_history  "
                                                + "WHERE member_id = '" + memId + "' AND comment_id = '" + commentId + "'";

                                        String commentLikedHostoryRows = dbsession.createSQLQuery(commentLikedHostorySQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(commentLikedHostorySQL).uniqueResult().toString();
                                        //   int numrows = Integer.parseInt(numrow1);

                                        logger.info("commentLikedHostoryRows ::" + commentLikedHostoryRows);
                                        System.out.println("commentLikedHostoryRows ::" + commentLikedHostoryRows);

                                        if (commentLikedHostoryRows.equals("0")) {
                                            commentLiked = "0";
                                            commentLikedText = "No";
                                        } else {
                                            commentLiked = "1";
                                            commentLikedText = "Yes";
                                        }

                                        commentInfoResponseObj = new JSONObject();
                                        commentInfoResponseObj.put("commentId", commentId);

                                        commentInfoResponseObj.put("commentLikeCount", commentLikeCount);
                                        commentInfoResponseObj.put("commentIsAccepted", commentIsAccepted);
                                        commentInfoResponseObj.put("commentLiked", commentLiked);
                                        commentInfoResponseObj.put("commentLikedText", commentLikedText);

                                        commentInfoResponseObj.put("CommentChildInfo", commentChildInfoObjArray);

                                        commentInfoResponseObj.put("commentParent", commentParent);
                                        commentInfoResponseObj.put("commmentDesc", commmentDesc);
                                        commentInfoResponseObj.put("commmentDate", commmentDate);
                                        commentInfoResponseObj.put("commmentPinedStatus", commmentPinedStatus);

                                        commentInfoResponseObj.put("commmenterId", commmenterId);
                                        commentInfoResponseObj.put("commmenterName", commmenterName);
                                        commentInfoResponseObj.put("commmenterPicture", commmenterPicture);
                                        commentInfoResponseObj.put("commmenterPictureLink", commmenterPictureLink);
                                        commentInfoResponseObj.put("commmenterType", commmenterType);
                                        commentInfoResponseObj.put("commmenterTypeName", commmenterTypeName);

                                        commentInfoObjArray.add(commentInfoResponseObj);

                                    }
                                }

                            } else {

                                commentInfoObjArray = new JSONArray();
                            }

                            if (contentType.equals("PC")) {

                                pictureOpt = GlobalVariable.imageDirLink + "postcontent/" + contentPostPicture;
                                //  pictureOpt = GlobalVariable.imageUploadPath + "postcontent/"+ contentPostPicture;
                            } else {
                                pictureOpt = "";
                            }

                            if (contentType.equals("VI")) {
                                videoOpt = contentPostLink;
                            } else {
                                videoOpt = "";
                            }

                            if (memberIEBId.equals(memberId)) {
                                ownContentFlag = "Y";
                            } else {
                                ownContentFlag = "";
                            }

                            //check user rating history point already exits or not
                            String contentLikedHostorySQL = "SELECT  count(*) FROM post_content_like_history  "
                                    + "WHERE member_id = '" + memId + "' AND content_id = '" + contentId + "'";

                            String contentLikedHostoryRows = dbsession.createSQLQuery(contentLikedHostorySQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(contentLikedHostorySQL).uniqueResult().toString();
                            //   int numrows = Integer.parseInt(numrow1);

                            logger.info("contentLikedHostoryRows ::" + contentLikedHostoryRows);
                            System.out.println("contentLikedHostoryRows ::" + contentLikedHostoryRows);

                            if (contentLikedHostoryRows.equals("0")) {
                                contentLiked = "0";
                                contentLikedText = "No";
                            } else {
                                contentLiked = "1";
                                contentLikedText = "Yes";
                            }

                            postContentObj = new JSONObject();
                            postContentObj.put("contentId", contentId);

                            postContentObj.put("contentLiked", contentLiked);
                            postContentObj.put("contentLikedText", contentLikedText);

                            postContentObj.put("CommentInfo", commentInfoObjArray);

                            postContentObj.put("totalLikeCount", totalLikeCount);
                            postContentObj.put("totalLikeCountText", totalLikeCountText);
                            postContentObj.put("totalCommentCount", totalCommentCount);
                            postContentObj.put("totalShareCount", totalShareCount);

                            postContentObj.put("contentCategory", contentCategory);
                            postContentObj.put("contentType", contentType);
                            postContentObj.put("contentTitle", contentTitle);
                            postContentObj.put("contentShortDesc", contentShortDesc);
                            postContentObj.put("contentDesc", contentDesc);
                            postContentObj.put("contentDate", contentDate);
                            postContentObj.put("contentPostLink", contentPostLink);
                            postContentObj.put("contentPostPicture", contentPostPicture);
                            postContentObj.put("contentPostShare", contentPostShare);
                            postContentObj.put("contentPostShare", contentPostShare);
                            postContentObj.put("contentPictureLinkOpt", pictureOpt);
                            postContentObj.put("contentVideoLinkOpt", videoOpt);

                            //   'contentPictureLinkOpt' => $pictureOpt,
                            //    'contentVideoLinkOpt' => $videoOpt,
                            //     'ownContentFlag' => $ownContentFlag,
                            //  'contentOwnerId' => $content_owner,
                            //   'contentOwnerName' => $contentOwnerName,
                            //   'contentOwnerDesg' => $contentOwnerDesg,
                            //    'contentOwnerPic' => $contentOwnerPic,
                            postContentObj.put("ownContentFlag", ownContentFlag);
                            postContentObj.put("contentOwnerType", mMemberTypeName);
                            postContentObj.put("contentOwnerId", contentOwnerId);
                            postContentObj.put("contentOwnerIEBId", memberIEBId);
                            postContentObj.put("contentOwnerName", memberName);
                            postContentObj.put("contentOwnerPic", memberPictureLink);
                            postContentObj.put("contentDiseaseCategoryName",postDiseaseCategoryName);

                            postContentObjArr.add(postContentObj);

                        }

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", postContentObjArr);
                        logingObj.put("TotalData", numrows);
                        logingObj.put("TotalPage", (int) totalpages);
                        logingObj.put("CurrentPage", currentpage);
                        logingObj.put("RowPerPage", rowsperpage);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
