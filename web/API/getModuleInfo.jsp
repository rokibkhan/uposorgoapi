<%-- 
    Document   : getModuleInfo
    Created on : APR 29, 2020, 11:19:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getModuleInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query moduleSQL = null;
    Object moduleObj[] = null;

    String moduleId = "";
    String moduleName = "";
    String moduleDesc = "";
    String moduleStatus = "";

    JSONArray moduleResponseObjArr = new JSONArray();
    JSONObject moduleResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    JSONArray moduleDataObjArr = new JSONArray();
    JSONObject moduleDataObj = new JSONObject();

    JSONArray noticeModuleResponseObjArr = new JSONArray();
    JSONObject noticeModuleResponseObj = new JSONObject();

    JSONArray bloodModuleResponseObjArr = new JSONArray();
    JSONObject bloodModuleResponseObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("BPSA :: API :: getModuleInfo rec :: " + rec);
    if (rec == 1) {

        try {

            moduleSQL = dbsession.createSQLQuery("SELECT * FROM sy_module  ORDER BY id ASC");
            //moduleSQL = dbsession.createSQLQuery("SELECT * FROM module  ORDER BY module_long_name ASC");

            if (!moduleSQL.list().isEmpty()) {
                for (Iterator it = moduleSQL.list().iterator(); it.hasNext();) {

                    moduleObj = (Object[]) it.next();
                    moduleId = moduleObj[0].toString();
                    moduleName = moduleObj[1].toString();
                    moduleDesc = moduleObj[2].toString() == null ? "" : moduleObj[2].toString();
                    moduleStatus = moduleObj[3].toString(); 
                 //   moduleStatus = "0";

                    moduleResponseObj = new JSONObject();

                    moduleResponseObj.put("Module", moduleName);
                    moduleResponseObj.put("ModuleDesc", moduleDesc);
                    moduleResponseObj.put("ModuleStatus", moduleStatus);

                    moduleResponseObjArr.add(moduleResponseObj);
                    moduleDataObj.put(moduleName, moduleResponseObjArr);

                }

                /*noticeModuleResponseObj = new JSONObject();
                noticeModuleResponseObj.put("Module", "NOTICE");
                noticeModuleResponseObj.put("ModuleDesc", "NOTICE TEST");
                noticeModuleResponseObj.put("ModuleStatus", "1");
                noticeModuleResponseObjArr.add(noticeModuleResponseObj);

                bloodModuleResponseObj = new JSONObject();
                bloodModuleResponseObj.put("Module", "BLOOD");
                bloodModuleResponseObj.put("ModuleDesc", "BLOOD TEST");
                bloodModuleResponseObj.put("ModuleStatus", "1");
                bloodModuleResponseObjArr.add(bloodModuleResponseObj);

                moduleDataObj.put("DOCTOR", moduleResponseObjArr);
                moduleDataObj.put("NOTICE", noticeModuleResponseObjArr);
                moduleDataObj.put("BLOOD", bloodModuleResponseObjArr);*/
                //   jsonThanaObjArr = new JSONArray();
                
                moduleDataObj.put("currentVersionCode", "1"); //app current version
                
                moduleDataObj.put("lastForceUpdateVersionCode", "1"); //app current version
          //     moduleDataObj.put("maxForceUpdateVersionCode", "3.0.3"); //app current version

                //    moduleDataObj.put("appUpdateRequired", "1"); //0 -> No 1 -> Yes
                moduleDataObj.put("appUpdateRequired", "1"); //0 -> No 1 -> Yes

                moduleDataObjArr.add(moduleDataObj);

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", moduleDataObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", moduleResponseObjArr);

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", moduleResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>