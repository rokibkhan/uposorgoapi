<%-- 
    Document   : patientQueryRequest
    Created on : MAY 05, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    // Logger logger = Logger.getLogger("patientQueryRequest_jsp.class");
    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    getRegistryID getId = new getRegistryID();
    SendFirebasePushNotification fcmsend = new SendFirebasePushNotification();

    Logger logger = Logger.getLogger("postContentShareRating_jsp.class");

    String imageBase64String = "";
    // String filePath = GlobalVariable.imageUploadPath;
    //  public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";     //WINDOWS
    //  public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";   //SERVER
    //  String filePath = "D:/NetbeansDevelopment/FFMS/web/upload/";     //WINDOWS
    //  String filePath = "/app/FFMS/webapps/FFMS/upload/";     //SERVER
    //   String filePath = "/app/FFMS/webapps/FFMS/web/upload/";     //SERVER

    // public static final String baseUrlImg = "http://localhost:8084/IEBWEB";
    // public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";  //joybangla
    // public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";         //windows
    String filePath = GlobalVariable.imageUploadPath + "patientcontent/";

    logger.info("patientQueryRequest API filePath :" + filePath);
    DateFormat parseFormat_YMDHMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat displayFormat_YMD_AM_PM = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String haveAttachment = ""; //1== yes 0==No attachment    
    String patinetContentPicture = "";
    String patinetContentPictureLink = "";
    String pQueryDesc = "";
    String pQueryProblemCategoryId = "";
    String pQueryProblemCategoryName = "";
    String pQueryDate = "";
    String pQueryStatus = "0"; //0->UnAnswer;1->Answer
    String pQueryReplyStatus = "0"; //0- No reply required;1->After Answered now reply required
    String pQuerySufferingDate = "";

    int pQueryParent = 0;

    String fever = "";
    String cough = "";
    String tiredness = "";
    String nasalcongestion = "";
    String headache = "";

    String feverText = "";
    String coughText = "";
    String tirednessText = "";
    String nasalcongestionText = "";
    String headacheText = "";

    String patientName = "";
    String patientEmail = "";
    String patientPhone = "";
    String patientDOB = "";
    String patientGender = "";

    String pQueryCommentType = "";
    String pQueryCommenterId = "";

    String adddate = "";

    String doctorId = "";
    String doctorTimeslotId = "";
    String serialCountMax = "";
    String serialCountCurrent = "";
    String serialBaseTime = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("haveAttachment") && request.getParameterMap().containsKey("doctorId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        doctorId = request.getParameter("doctorId").trim();
        doctorTimeslotId = request.getParameter("doctorTimeslotId").trim();

        haveAttachment = request.getParameter("haveAttachment").trim();
        pQueryProblemCategoryId = request.getParameter("pQueryProblemCategoryId") == null ? "" : request.getParameter("pQueryProblemCategoryId").trim();
        pQueryDesc = request.getParameter("pQueryDesc") == null ? "" : request.getParameter("pQueryDesc").trim();
        pQuerySufferingDate = request.getParameter("pQuerySufferingDate") == null ? "" : request.getParameter("pQuerySufferingDate").trim();

        patientName = request.getParameter("patientName") == null ? "" : request.getParameter("patientName").trim();
        patientEmail = request.getParameter("patientEmail") == null ? "" : request.getParameter("patientEmail").trim();
        patientPhone = request.getParameter("patientPhone") == null ? "" : request.getParameter("patientPhone").trim();
        patientDOB = request.getParameter("patientDOB") == null ? "" : request.getParameter("patientDOB").trim();
        patientGender = request.getParameter("patientGender") == null ? "" : request.getParameter("patientGender").trim();

        imageBase64String = request.getParameter("imageString") == null ? "" : request.getParameter("imageString");
        //imageBase64String = request.getParameter("imageString");
        logger.info("patientQueryRequest API  imageBase64String :" + imageBase64String);

        System.out.println("patientQueryRequest API imageBase64String :" + imageBase64String);

        fever = request.getParameter("fever") == null ? "" : request.getParameter("fever").trim();
        cough = request.getParameter("cough") == null ? "" : request.getParameter("cough").trim();
        tiredness = request.getParameter("tiredness") == null ? "" : request.getParameter("tiredness").trim();
        nasalcongestion = request.getParameter("nasalcongestion") == null ? "" : request.getParameter("nasalcongestion").trim();
        headache = request.getParameter("headache") == null ? "" : request.getParameter("headache").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: patientQueryRequest API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    Query patientInfoAddSQL = null;
    Query patientCovidInfoAddSQL = null;

    Query doctorAppointmentInfoAddSQL = null;

    String memMemberName = "";
    String memberDeviceId = "";

    if (rec == 1) {

        try {

            if (haveAttachment.equals("1")) {

                Random random = new Random();
                int rand1 = Math.abs(random.nextInt());
                int rand2 = Math.abs(random.nextInt());
                DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
                Date dateFileToday = new Date();
                String filePrefixToday = formatterFileToday.format(dateFileToday);

                String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

                logger.info("patientQueryRequest API filePath :" + filePath);
                logger.info("patientQueryRequest API fileName1 :" + fileName1);

                System.out.println("patientQueryRequest API filePath :" + filePath);
                System.out.println("patientQueryRequest API fileName1 :" + fileName1);

                // contentPostPictureLink = GlobalVariable.baseUrl + "/upload/" + fileName1;
                patinetContentPictureLink = GlobalVariable.baseUrlImg + "/upload/patientcontent/" + fileName1;

                /*
                * Converting a Base64 String into Image byte array 
                 */
                // byte[] imageByteArray = Base64.decodeBase64(imageBase64String);
                byte[] imageByteArray = Base64.getDecoder().decode(imageBase64String);

                /*
                  * Write a image byte array into file system  
                 */
                //  FileOutputStream imageOutFile = new FileOutputStream("/Users/jeeva/Pictures/wallpapers/water-drop-after-convert.jpg");
                FileOutputStream imageOutFile = new FileOutputStream(filePath + fileName1);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();

                patinetContentPicture = fileName1;

            }

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memMemberName = member.getMemberName().toString();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String idS = getId.getID(70);
                        int pQueryId = Integer.parseInt(idS);

                        String idAappS = getId.getID(47); //TEMP_PROPOSER_ID
                        int pAppointmentId = Integer.parseInt(idAappS);

                        //  String idPatient = getId.getID(71);
                        //  int pQueryPatientId = Integer.parseInt(idPatient);
                        //      int pQueryPatientId = 1;
                        int pQueryPatientId = pQueryId;

                        //    doctorTimeslotId
                        InetAddress ip;
                        String hostname = "";
                        String ipAddress = "";
                        try {
                            ip = InetAddress.getLocalHost();
                            hostname = ip.getHostName();
                            ipAddress = ip.getHostAddress();

                        } catch (UnknownHostException e) {

                            e.printStackTrace();
                        }

                        String adduser = Integer.toString(memId);
                        adddate = dateFormatX.format(dateToday);
                        String addterm = hostname;
                        String published = "1";

                        String assignDoctorStatus = "1";

                        pQueryDate = dateFormatX.format(dateToday);

                        DateFormat dateFormatXnc = new SimpleDateFormat("yyyy-MM-dd");
                        Date pQuerySufferingDate1 = dateFormatXnc.parse(pQuerySufferingDate);

                        Query doctorDetailsInfoSQL = null;
                        Object doctorDetailsInfoObj[] = null;

                        Query memberDeviceIdSQL = dbsession.createSQLQuery("SELECT device_id FROM member_deviceid where member_id='" + memId + "'");
                        memberDeviceId = memberDeviceIdSQL.uniqueResult() == null ? "" : memberDeviceIdSQL.uniqueResult().toString();

                        String doctorName = "";
                        String doctorDeviceId = "";
                        String doctorDegree0 = "";
                        String doctorDegree1 = "";
                        String doctorDegree2 = "";
                        String doctorDegree3 = "";
                        String doctorDegree4 = "";
                        String doctorRegNo = "";
                        String doctorMedicalCollege = "";
                        String doctorDetailsForNotification = "";

                        doctorDetailsInfoSQL = dbsession.createSQLQuery("select m.member_name,md.device_id,"
                                + "ai.addi_info_0 as mDegree0,ai.addi_info_1 as mDegree1,ai.addi_info_2 as mDegree2 ,ai.addi_info_3 as mDegree3, "
                                + "ai.addi_info_4 as mDegree4,ai.addi_info_6 as mRegNo,ai.addi_info_7 as medicalCollege   "
                                + "FROM member m,member_additional_info ai,member_deviceid md  "
                                + "WHERE m.id = ai.member_id AND m.id = md.member_id AND m.id='" + doctorId + "'");

                        //  String doctorDeviceId = doctorDeviceIdSQL.uniqueResult() == null ? "": doctorDeviceIdSQL.uniqueResult().toString();
                        logger.info("doctorDetailsInfoSQL ::" + doctorDetailsInfoSQL);
                        if (!doctorDetailsInfoSQL.list().isEmpty()) {
                            for (Iterator doctorTimeSlotIt = doctorDetailsInfoSQL.list().iterator(); doctorTimeSlotIt.hasNext();) {
                                doctorDetailsInfoObj = (Object[]) doctorTimeSlotIt.next();

                                doctorName = doctorDetailsInfoObj[1] == null ? "" : doctorDetailsInfoObj[0].toString();
                                doctorDeviceId = doctorDetailsInfoObj[1] == null ? "" : doctorDetailsInfoObj[1].toString();
                                doctorDegree0 = doctorDetailsInfoObj[2] == null ? "" : doctorDetailsInfoObj[2].toString();
                                doctorDegree1 = doctorDetailsInfoObj[3] == null ? "" : doctorDetailsInfoObj[3].toString();

                                doctorDegree2 = doctorDetailsInfoObj[4] == null ? "" : doctorDetailsInfoObj[4].toString();
                                doctorDegree3 = doctorDetailsInfoObj[5] == null ? "" : doctorDetailsInfoObj[5].toString();
                                doctorDegree4 = doctorDetailsInfoObj[6] == null ? "" : doctorDetailsInfoObj[6].toString();
                                doctorRegNo = doctorDetailsInfoObj[7] == null ? "" : doctorDetailsInfoObj[7].toString();
                                doctorMedicalCollege = doctorDetailsInfoObj[8] == null ? "" : doctorDetailsInfoObj[8].toString();

                            }
                        }

                        doctorDetailsForNotification = doctorName + "," + doctorDegree0 + "," + doctorDegree1 + "," + doctorMedicalCollege + ",Reg no-" + doctorRegNo;

                        Query doctorTimeSlotSQL = null;
                        Object doctorTimeSlotObj[] = null;

                        String doctorSerialBaseTime = "10";
                        String doctorTimeSlotMinSerialCount = "";
                        String doctorTimeSlotMaxSerialCount = "";
                        String doctorTimeSlotStartTime = "";
                        String doctorTimeSlotDate = "";

                        String doctorTimeSlotShortName = "";
                        String doctorTimeSlotName = "";
                        String doctorTimeSlotTimeText = "";
                        String doctorTimeSlotTimeHour = "";
                        String doctorTimeSlotTimeMinute = "";
                        String doctorTimeSlotTimeNumber = "";

                        String appointmentTime = "";

                        String doctorTimeSlotMinSerialCountNext = "";
                        //    String doctorTimeSlotTotalSerialCount = "";

                        doctorTimeSlotSQL = dbsession.createSQLQuery("SELECT "
                                + "dts.serial_count,dts.max_serial_count,"
                                + "dts.next_serial_start_time,dts.timeslot_date,  "
                                + "ts.short_name,ts.name,ts.time_text,ts.time_hour, "
                                + "ts.time_minute,ts.time_number  "
                                + "FROM doctor_timeslot dts,doctor_timeslot_name ts  "
                                + "WHERE dts.status = 1 "
                                + "AND dts.timeslot_id = ts.id "
                                + "AND dts.member_id = " + doctorId + " "
                                + "AND dts.id = '" + doctorTimeslotId + "'");

                        if (!doctorTimeSlotSQL.list().isEmpty()) {
                            for (Iterator doctorTimeSlotIt = doctorTimeSlotSQL.list().iterator(); doctorTimeSlotIt.hasNext();) {
                                doctorTimeSlotObj = (Object[]) doctorTimeSlotIt.next();

                                doctorTimeSlotMinSerialCount = doctorTimeSlotObj[0].toString();
                                doctorTimeSlotMaxSerialCount = doctorTimeSlotObj[1].toString();
                                doctorTimeSlotStartTime = doctorTimeSlotObj[2].toString();
                                doctorTimeSlotDate = doctorTimeSlotObj[3].toString();

                                doctorTimeSlotShortName = doctorTimeSlotObj[4].toString();
                                doctorTimeSlotName = doctorTimeSlotObj[5].toString();
                                doctorTimeSlotTimeText = doctorTimeSlotObj[6].toString();
                                doctorTimeSlotTimeHour = doctorTimeSlotObj[7].toString();
                                doctorTimeSlotTimeMinute = doctorTimeSlotObj[8] == null ? "" : doctorTimeSlotObj[8].toString();
                                doctorTimeSlotTimeNumber = doctorTimeSlotObj[9] == null ? "" : doctorTimeSlotObj[9].toString();

                            }
                        }

                        if (doctorTimeSlotMinSerialCount.equals("0")) {
                            appointmentTime = doctorTimeSlotStartTime;
                            doctorTimeSlotMinSerialCountNext = "1";
                        } else {

                            int baseTimeFact = Integer.parseInt(doctorTimeSlotTimeNumber) / Integer.parseInt(doctorTimeSlotMaxSerialCount);

                            //baseTimeFact 
                            int minuteAddForNextSerial = baseTimeFact * Integer.parseInt(doctorTimeSlotMinSerialCount);

                            //  String myTime1 = "2020-06-29 17:31:02";
                            //  SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                            Date myTime1X = parseFormat_YMDHMS.parse(doctorTimeSlotStartTime);
                            Calendar calXn = Calendar.getInstance();
                            calXn.setTime(myTime1X);
                            calXn.add(Calendar.MINUTE, minuteAddForNextSerial);
                            // String newTime1Xn = displayFormat_YMD_AM_PM.format(calXn.getTime());
                            appointmentTime = parseFormat_YMDHMS.format(calXn.getTime());

                            int nextSerial = Integer.parseInt(doctorTimeSlotMinSerialCount) + 1;
                            doctorTimeSlotMinSerialCountNext = Integer.toString(nextSerial);

                            System.out.println("timeCheck API doctorTimeSlotStartTime :" + doctorTimeSlotStartTime);
                            System.out.println("timeCheck API appointmentTime :" + appointmentTime);
                            System.out.println("timeCheck API appointmentTime :" + doctorTimeSlotMinSerialCountNext);

                        }

                        Query diseaseCategoryIdSQL = dbsession.createSQLQuery("SELECT doctor_category_id FROM doctor_category WHERE member_id ='" + doctorId + "'");

                        String disease_category_id = diseaseCategoryIdSQL.uniqueResult().toString();

                        patientInfoAddSQL = dbsession.createSQLQuery("INSERT INTO patient_info("
                                + "patient_id,"
                                + "patient_name,"
                                + "patient_email,"
                                + "patient_phone,"
                                + "patient_dob,"
                                + "patient_gender,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip"
                                + ") VALUES("
                                + "'" + pQueryPatientId + "',"
                                + "'" + patientName + "',"
                                + "'" + patientEmail + "',"
                                + "'" + patientPhone + "',"
                                + "'" + patientDOB + "',"
                                + "'" + patientGender + "',"
                                + "'" + adduser + "',"
                                + "'" + adddate + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "'"
                                + "  ) ");

                        logger.info("patientInfoAddSQL ::" + patientInfoAddSQL);

                        patientInfoAddSQL.executeUpdate();

                        //prepare Statement
                        PreparedStatement ps = con.prepareStatement("INSERT INTO patient_query("
                                + "query_id,"
                                + "parent,"
                                + "patient_id,"
                                + "problem_category,"
                                + "query_desc,"
                                + "suffering_date,"
                                + "query_date,"
                                + "commenter_id,"
                                + "member_id,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip,"
                                + "mod_date,"
                                + "attachment, "
                                + "assign_doctor, "
                                + "status, "
                                + "disease_category_id) "
                                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                        //  ps.setInt(1, Integer.parseInt(postContentRequestId));
                        ps.setInt(1, pQueryId);
                        ps.setInt(2, pQueryParent);
                        ps.setInt(3, pQueryPatientId);
                        ps.setInt(4, Integer.parseInt(pQueryProblemCategoryId));
                        ps.setString(5, pQueryDesc);
                        ps.setString(6, pQuerySufferingDate);
                        ps.setString(7, adddate);
                        ps.setInt(8, memId);
                        ps.setInt(9, memId);

                        ps.setString(10, adduser);
                        ps.setString(11, adddate);
                        ps.setString(12, addterm);
                        ps.setString(13, ipAddress);
                        ps.setString(14, adddate);
                        ps.setString(15, patinetContentPicture);
                        ps.setInt(16, Integer.parseInt(doctorId));
                        //   ps.setInt(17, Integer.parseInt(assignDoctorStatus)); //1->Assign Doctor
                        ps.setInt(17, '0'); //1->Assign Doctor
                        ps.setInt(18, Integer.parseInt(disease_category_id)); //1->Assign Doctor Category

                        logger.info("SQL Query: " + ps.toString());

                        ps.executeUpdate();

                        patientCovidInfoAddSQL = dbsession.createSQLQuery("INSERT INTO patient_query_covid("
                                + "covid_qry_id,"
                                + "query_id,"
                                + "fever,"
                                + "cough,"
                                + "tiredness,"
                                + "nasalcongestion,"
                                + "headache,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip"
                                + ") VALUES("
                                + "'" + pQueryPatientId + "',"
                                + "'" + pQueryPatientId + "',"
                                + "'" + fever + "',"
                                + "'" + cough + "',"
                                + "'" + tiredness + "',"
                                + "'" + nasalcongestion + "',"
                                + "'" + headache + "',"
                                + "'" + adduser + "',"
                                + "'" + adddate + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "'"
                                + "  ) ");

                        logger.info("patientCovidInfoAddSQL ::" + patientCovidInfoAddSQL);

                        patientCovidInfoAddSQL.executeUpdate();

                        //set Appointment Time use by serialcount and max serial count
                        //   String appointmentTime = "3.10 PM";
                        String appointmentMsg = "";
                        String appointmentDoctorMsg = "";

                        doctorAppointmentInfoAddSQL = dbsession.createSQLQuery("INSERT INTO patient_appointment("
                                + "id,"
                                + "member_id,"
                                + "pquery_id,"
                                + "patient_id,"
                                + "doctor_timeslot_id,"
                                + "appointment_time,"
                                + "appointment_msg,"
                                + "doctor_msg,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip"
                                + ") VALUES("
                                + "'" + pAppointmentId + "',"
                                + "'" + doctorId + "',"
                                + "'" + pQueryId + "',"
                                + "'" + pQueryPatientId + "',"
                                + "'" + doctorTimeslotId + "',"
                                + "'" + appointmentTime + "',"
                                + "'" + appointmentMsg + "',"
                                + "'" + appointmentDoctorMsg + "',"
                                + "'" + adduser + "',"
                                + "'" + adddate + "',"
                                + "'" + hostname + "',"
                                + "'" + ipAddress + "'"
                                + "  ) ");

                        logger.info("doctorAppointmentInfoAddSQL ::" + doctorAppointmentInfoAddSQL);

                        doctorAppointmentInfoAddSQL.executeUpdate();

                        Query updateDoctorTimeslotMinCountSQL = dbsession.createSQLQuery("UPDATE  doctor_timeslot SET serial_count = '" + doctorTimeSlotMinSerialCountNext + "', mod_date='" + adddate + "'  WHERE id = '" + doctorTimeslotId + "'");
                        updateDoctorTimeslotMinCountSQL.executeUpdate();

                        ps.close();
                        con.commit();
                        con.close();
                        String msgPriority = "high"; // normal
                        //Notification for doctor
                        if (!doctorDeviceId.equals("")) {
                            String msgTitle = memMemberName + " appointment details";
                            String msgBody = "Appointment Time :" + appointmentTime + " ," + doctorDetailsForNotification;
                            String receipentDeviceId = doctorDeviceId;
                            //     String receipentDeviceId = "cGfWg1IlE1o:APA91bHKfVtMGSlAlLeufYlCuv2ZGlH1RAjY3ssnT8WNVJIJ0Hpvkav1VeHxQ-1WlYcVId09B2ubUDPL7lhzB7GXgU-gwtzZ96MpCwCUuTUHKofNQar32Ypb-mejf4xIPV6p66NsRMAY";
                            System.out.println("receipentDeviceId :: doctorDeviceId ::" + receipentDeviceId);

                            String fcmsendDoctor = fcmsend.sendSingleNotification(msgTitle, msgBody, receipentDeviceId, msgPriority);
                            logger.info("fcmsendDoctor" + fcmsendDoctor);
                            System.out.println("fcmsendDoctor::" + fcmsendDoctor);

                        }
                        //Notification for patient
                        //    System.out.println("fcmsendpatient::" + fcmsendpatient);
                        if (!memberDeviceId.equals("")) {
                            //  String appointmentTime_YMD_AM_PM = displayFormat_YMD_AM_PM.format(appointmentTime);
                            //   System.out.println("appointmentTime_YMD_AM_PM" + appointmentTime_YMD_AM_PM);
                            String msgTitle = memMemberName + " appointment details";
                            String msgBody = "Appointment Time :" + appointmentTime + " ," + doctorDetailsForNotification;
                            String receipentDeviceId = memberDeviceId;
                            //     String receipentDeviceId = "cGfWg1IlE1o:APA91bHKfVtMGSlAlLeufYlCuv2ZGlH1RAjY3ssnT8WNVJIJ0Hpvkav1VeHxQ-1WlYcVId09B2ubUDPL7lhzB7GXgU-gwtzZ96MpCwCUuTUHKofNQar32Ypb-mejf4xIPV6p66NsRMAY";
                            System.out.println("receipentDeviceId::" + receipentDeviceId);

                            String fcmsendpatient = fcmsend.sendSingleNotification(msgTitle, msgBody, receipentDeviceId, msgPriority);
                            logger.info("fcmsendpatient" + fcmsendpatient);
                            System.out.println("fcmsendpatient::" + fcmsendpatient);
                        }

                        //                        postContentObj = new JSONObject();
                        //                        postContentObj.put("ContentRequestId", postContentRequestId);
                        //                        postContentObj.put("ContentType", postContentType);
                        //                        postContentObj.put("ContentCategory", postContentCategory);
                        //                        postContentObj.put("ContentTitle", postContentTitle);
                        //                        postContentObj.put("ContentShortDesc", postContentShortDesc);
                        //                        postContentObj.put("ContentDesc", postContentDesc);
                        //                        postContentObj.put("ContentPostLink", postContentPostLink);
                        //                        postContentObj.put("ContentPicture", postContentPicture);
                        //                        postContentObj.put("ContentPictureLink", contentPostPictureLink);
                        postContentObj = new JSONObject();
                        postContentObj.put("pQueryId", pQueryId);

                        postContentObj.put("patientId", pQueryPatientId);
                        postContentObj.put("patientName", patientName);
                        postContentObj.put("patientEmail", patientEmail);
                        postContentObj.put("patientPhone", patientPhone);
                        postContentObj.put("patientDOB", patientDOB);
                        postContentObj.put("patientGender", patientGender);

                        postContentObj.put("pQueryParent", pQueryParent);
                        postContentObj.put("pQueryPatientId", pQueryPatientId);

                        postContentObj.put("pQueryProblemCategoryId", pQueryProblemCategoryId);
                        postContentObj.put("pQueryProblemCategoryName", pQueryProblemCategoryName);

                        postContentObj.put("pQueryDesc", pQueryDesc);
                        postContentObj.put("pQuerySufferingDate", pQuerySufferingDate);
                        postContentObj.put("pQueryDate", pQueryDate);
                        postContentObj.put("pQueryStatus", pQueryStatus);
                        postContentObj.put("pQueryReplyStatus", pQueryReplyStatus);
                        postContentObj.put("pQueryCommentType", pQueryCommentType);
                        postContentObj.put("pQueryCommenterId", pQueryCommenterId);

                        postContentObj.put("fever", fever);
                        postContentObj.put("feverText", feverText);

                        postContentObj.put("cough", cough);
                        postContentObj.put("coughText", coughText);

                        postContentObj.put("tiredness", tiredness);
                        postContentObj.put("tirednessText", tirednessText);

                        postContentObj.put("nasalcongestion", nasalcongestion);
                        postContentObj.put("nasalcongestionText", nasalcongestionText);

                        postContentObj.put("headache", headache);
                        postContentObj.put("headacheText", headacheText);

                        postContentObj.put("attachment", patinetContentPicture);
                        postContentObj.put("attachmentLink", patinetContentPictureLink);

                        // postContentObj.put("queryOwnerId", queryOwnerId);
                        //   postContentObj.put("queryOwnerName", queryOwnerName);
                        //  postContentObj.put("queryOwnerPic", queryOwnerPicLink);
                        //    postContentObjArr.add(postContentObj);
                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Patient query added successfully");
                        logingObj.put("ResponseData", postContentObj);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
