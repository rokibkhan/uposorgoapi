<%-- 
    Document   : signup
    Created on : MAY 08, 2020, 8:26:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    RandomString randNumber = new RandomString();

    SendSMS sendsms = new SendSMS();
    SendEmail sendemail = new SendEmail();

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());

    long uniqueNumber = System.currentTimeMillis();

    String memberUniqueNumber = uniqueNumber + "-" + rand1 + "-" + rand2;

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    Logger logger = Logger.getLogger("signup_jsp.class");

    String key = "";
    String fullName = "";
    String mobileNumber = "";
    String districtId = "";
    String thanaId = "";

    String memberId = "";
    String givenPassword = "";
    String postContentId = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("fullName") && request.getParameterMap().containsKey("mobileNumber") && request.getParameterMap().containsKey("districtId") && request.getParameterMap().containsKey("thanaId")) {
        key = request.getParameter("key").trim();

        fullName = request.getParameter("fullName").trim();
        mobileNumber = request.getParameter("mobileNumber").trim();
        districtId = request.getParameter("districtId").trim();
        thanaId = request.getParameter("thanaId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    // String firstLetter = userInput.substring(0,1); //takes first letter
//String restOfString = userInput.substring(1); //takes rest of sentence
//firstLetter = firstLetter.toUpperCase(); //make sure to set the string, the methods return strings, they don't change the string itself
//restOfString = restOfString.toLowerCase();
//String merged = firstletter + restOfString;
    String firstLetter = mobileNumber.substring(0, 1);
    String adjustLettter1 = "+88";
    String adjustLettter2 = "88";
    String adjustLettter3 = "+";

    String mobileNumber1 = "";
    String mobileNumber2 = "";

    if (firstLetter.equals("0")) {
        mobileNumber1 = adjustLettter1 + mobileNumber;
        mobileNumber2 = adjustLettter2 + mobileNumber;

        System.out.println("firstLetter :: 0 :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: 0 :: mobileNumber2 ::" + mobileNumber2);
    }
    if (firstLetter.equals("8")) {
        mobileNumber1 = adjustLettter3 + mobileNumber;
        mobileNumber2 = adjustLettter3 + mobileNumber;
        System.out.println("firstLetter :: 8 :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: 8 :: mobileNumber2 ::" + mobileNumber2);
    }
    if (firstLetter.equals("+")) {
        mobileNumber1 = mobileNumber;
        mobileNumber2 = mobileNumber;

        System.out.println("firstLetter :: + :: mobileNumber1 ::" + mobileNumber1);
        System.out.println("firstLetter :: + :: mobileNumber2 ::" + mobileNumber2);

    }

    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("signup API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String ratingDate = "";
    Query postContentShareRatingAddSQL = null;

    Query postContentRatingHistoryAddSQL = null;

    Query memberAddSQL = null;

    //  String ratingPointHostorySQL = "";
    //   String ratingPointHostoryNumRows = "";
    //  java.util.Date ed = new java.util.Date();
    // java.util.Date td = new java.util.Date();
    //   td = new SimpleDateFormat("yyyy-mm-dd").parse("2099-12-31");
    String ed = dateFormatYmd.format(dateToday);
    String td = "2099-12-31";

    String memberCountryCode = "BD";
    String tempPassStatus = "2"; //2->for switch force password change ;1->already change password
    String memberTypeId = "1"; //1->member;2->doctor;
    String memberCustomOrder = "999999";
    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
          //  qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber + "'");
    qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber1 + "'");

            if (qMember.list().isEmpty()) {

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Mobile number already exist.");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("mobile number :" + mobileNumber + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        // finally {
        //     dbtrx.commit();
        //   }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
