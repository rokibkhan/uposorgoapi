<%-- 
    Document   : groupSMSTotalMemberInfo
    Created on : NOV 19, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String messageBody = "";
    String messageBodyCount = "";

    String mTypeId = "";
    String mDivisionId = "";
    String mCenterId = "";
    String mSubCenterId = "";
    String mUniversityId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("messageBody") && request.getParameterMap().containsKey("messageBodyCount")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();
        messageBody = request.getParameter("messageBody").trim();
        messageBodyCount = request.getParameter("messageBodyCount").trim();

        mTypeId = request.getParameter("mTypeId") == null ? "" : request.getParameter("mTypeId").trim();
        mDivisionId = request.getParameter("mDivisionId") == null ? "" : request.getParameter("mDivisionId").trim();
        mCenterId = request.getParameter("mCenterId") == null ? "" : request.getParameter("mCenterId").trim();
        mSubCenterId = request.getParameter("mSubCenterId") == null ? "" : request.getParameter("mSubCenterId").trim();
        mUniversityId = request.getParameter("mUniversityId") == null ? "" : request.getParameter("mUniversityId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: groupSMSTotalMemberInfo rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject totalMemberCountObj = new JSONObject();
    JSONArray totalMemberCountObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    String filterp = "";
    String searchCountSQL = "";
    String total_pages1 = "";
    int total_pages = 0;
    Object searchObj[] = null;

    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String perSMSCostSQL = "";

    String totalPhoneNumberCount = "";
    String perSMSCost = "";
    String perSMSCost1 = "";
    String totalCost = "";

    double totalCostAmount = 0.0d;

    String memberTypeId = "";
    String memberTypeName = "";
    String memberTypeNameSQL = "";

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    String divisionId = "";
    String divisionShortName = "";
    String divisionLongName = "";
    Query divisionSQL = null;
    Object[] divisionObj = null;

    JSONArray divisionResponseObjArr = new JSONArray();
    JSONObject divisionResponseObj = new JSONObject();

    String centerId = "";
    String centerName = "";
    String centerNameSQL = "";

    JSONArray centerResponseObjArr = new JSONArray();
    JSONObject centerResponseObj = new JSONObject();

    String subCenterId = "";
    String subCenterName = "";
    String subCenterNameSQL = "";

    JSONArray subCenterResponseObjArr = new JSONArray();
    JSONObject subCenterResponseObj = new JSONObject();

    String universityId = "";
    String universityShortName = "";
    String universityLongName = "";
    Query universitySQL = null;
    Object[] universityObj = null;

    JSONArray universityResponseObjArr = new JSONArray();
    JSONObject universityResponseObj = new JSONObject();

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {
                        //mTypeId
                        //mDivisionId
                        //mCenterId
                        //mSubCenterId
                        //mUniversityId

                        logger.info("API :: groupSMSTotalMemberInfo :: API :: ");
                        logger.info("API :: groupSMSTotalMemberInfo :: mTypeId :: " + mTypeId);
                        logger.info("API :: groupSMSTotalMemberInfo :: mDivisionId :: " + mDivisionId);
                        logger.info("API :: groupSMSTotalMemberInfo :: mCenterId :: " + mCenterId);
                        logger.info("API :: groupSMSTotalMemberInfo :: mSubCenterId :: " + mSubCenterId);
                        logger.info("API :: groupSMSTotalMemberInfo :: mUniversityId :: " + mUniversityId);

                        if (!mCenterId.isEmpty()) {

                            searchCountSQL = "SELECT count(*) "
                                    + "FROM  member_education_info mei, member m ,member_type mt,member_division md,center c,university u "
                                    + "WHERE m.status = 1 "
                                    + "AND mei.degree_type_id = '3' "
                                    + "AND mei.member_id = m.id "
                                    + "AND m.member_division_id = md.mem_division_id "
                                    + "AND m.center_id = c.center_id "
                                    + "AND m.id = mt.member_id "
                                    + "AND (mt.member_type_id like '%" + mTypeId + "%'  and m.member_division_id like '%" + mDivisionId + "%'  and mei.board_university_id like '%" + mUniversityId + "%' and (m.center_id in (select center_id from center where center_type_id=4 and center_parent_id='" + mCenterId + "' ) and  m.center_id like '%" + mSubCenterId + "%') )"
                                    + "AND mei.board_university_id = u.university_id";

                        } else {
                            searchCountSQL = "SELECT count(*)"
                                    + "FROM  member_education_info mei, member m ,member_type mt,member_division md,center c,university u "
                                    + "WHERE m.status = 1 "
                                    + "AND mei.degree_type_id = '3' "
                                    + "AND mei.member_id = m.id "
                                    + "AND m.member_division_id = md.mem_division_id "
                                    + "AND m.center_id = c.center_id "
                                    + "AND m.id = mt.member_id "
                                    + "AND (mt.member_type_id like '%" + mTypeId + "%'  and m.member_division_id like '%" + mDivisionId + "%'  and mei.board_university_id like '%" + mUniversityId + "%'  )"
                                    + "AND mei.board_university_id = u.university_id ";

                        }

                        /*

                        //Check Only Member Type
                        if (!mTypeId.equals("")) {

                            filterSQLStr = " AND mt.member_type_id=" + mTypeId + " ";
                            filterp = "&memberSearchTypeId=" + mTypeId + "&";
                            logger.info("API :: groupSMSTotalMemberInfo :: Only Member Type :: ");
                            logger.info("API :: groupSMSTotalMemberInfo :: Only Member Type :: filterSQLStr :: " + filterSQLStr);
                            logger.info("API :: groupSMSTotalMemberInfo :: Only Member Type :: filterp :: " + filterp);

                            searchCountSQL = "SELECT count(*) FROM  member m,member_type mt "
                                    + "WHERE m.status = 1 "
                                    + "AND m.id = mt.member_id "
                                    + " " + filterSQLStr + " "
                                    + "ORDER BY m.id DESC";

                        }
                        //check only Center
                        
                        
                        //Check Only Division
                        if (!mDivisionId.equals("")) {

                            filterSQLStr = " AND m.member_division_id=" + mDivisionId + " ";
                            filterp = "&memberSearchDivisionId=" + mDivisionId + "&";
                            logger.info("API :: groupSMSTotalMemberInfo :: Only Division :: ");
                            logger.info("API :: groupSMSTotalMemberInfo :: Only Division :: filterSQLStr :: " + filterSQLStr);
                            logger.info("API :: groupSMSTotalMemberInfo :: Only Division :: filterp :: " + filterp);

                            searchCountSQL = "SELECT count(*) FROM  member m,member_type mt "
                                    + "WHERE m.status = 1 "
                                    + "AND m.id = mt.member_id "
                                    + " " + filterSQLStr + " "
                                    + "ORDER BY m.id DESC";

                        }
                        
                        //Check Only University
                        if (!mUniversityId.equals("")) {

                            filterSQLStr = " AND mei.board_university_id=" + mUniversityId + " ";
                            filterp = "&memberSearchUniversityId=" + mUniversityId + "&";
                            logger.info("API :: groupSMSTotalMemberInfo :: Only University :: ");
                            logger.info("API :: groupSMSTotalMemberInfo :: Only University :: filterSQLStr :: " + filterSQLStr);
                            logger.info("API :: groupSMSTotalMemberInfo :: Only University :: filterp :: " + filterp);

                            searchCountSQL = "SELECT count(*) FROM member_education_info mei, member m,member_type mt "
                                    + "WHERE m.status = 1 "
                                    + "AND mei.degree_type_id = '3' "
                                    + "AND mei.member_id = m.id "
                                    + "AND m.id = mt.member_id "
                                    + " " + filterSQLStr + " "
                                    + "ORDER BY m.id DESC";

                        }

                         */
                        total_pages1 = dbsession.createSQLQuery(searchCountSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(searchCountSQL).uniqueResult().toString();
                        total_pages = Integer.parseInt(total_pages1);

                        logger.info("API :: groupSMSTotalMemberInfo :: searchCountSQL :: " + searchCountSQL);
                        logger.info("API :: groupSMSTotalMemberInfo :: total_pages1 :: " + total_pages1);
                        logger.info("API :: groupSMSTotalMemberInfo :: total_pages :: " + total_pages);

                        perSMSCostSQL = "SELECT WITH_MASK_COST FROM group_sms_rate WHERE NOW() BETWEEN ED AND TD";

                        perSMSCost1 = dbsession.createSQLQuery(perSMSCostSQL).uniqueResult() == null ? "0" : dbsession.createSQLQuery(perSMSCostSQL).uniqueResult().toString();

                        // if (!mTypeId.isEmpty()) {
                        if (!mTypeId.equals("")) {
                            memberTypeNameSQL = "SELECT member_type_name FROM member_type_info WHERE member_type_id='" + mTypeId + "'";
                            memberTypeName = dbsession.createSQLQuery(memberTypeNameSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(memberTypeNameSQL).uniqueResult().toString();
                        }

                        memberTypeResponseObj = new JSONObject();
                        memberTypeResponseObj.put("MemberTypeId", mTypeId);
                        memberTypeResponseObj.put("MemberTypeName", memberTypeName);
                        //   memberTypeResponseObjArr.add(memberTypeResponseObj);

                        // if (!mDivisionId.isEmpty()) {
                        if (!mDivisionId.equals("")) {
                            divisionSQL = dbsession.createSQLQuery("SELECT * FROM member_division  WHERE mem_division_id='" + mDivisionId + "'");
                            for (Iterator divisionItr = divisionSQL.list().iterator(); divisionItr.hasNext();) {
                                divisionObj = (Object[]) divisionItr.next();
                                divisionShortName = divisionObj[1].toString();
                                divisionLongName = divisionObj[3].toString() == null ? "" : divisionObj[3].toString();
                            }
                        }
                        divisionResponseObj = new JSONObject();
                        divisionResponseObj.put("DivisionId", mDivisionId);
                        divisionResponseObj.put("DivisionShortName", divisionShortName);
                        divisionResponseObj.put("DivisionLongName", divisionLongName);
                        //  divisionResponseObjArr.add(divisionResponseObj);

                        // if (!mCenterId.isEmpty()) {
                        if (!mCenterId.equals("")) {
                            centerNameSQL = "SELECT center_name FROM center WHERE center_id='" + mCenterId + "'";
                            centerName = dbsession.createSQLQuery(centerNameSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(centerNameSQL).uniqueResult().toString();
                        }

                        centerResponseObj = new JSONObject();
                        centerResponseObj.put("CenterId", mCenterId);
                        centerResponseObj.put("CenterName", centerName);
                        //   centerResponseObjArr.add(centerResponseObj);

                        // if (!mSubCenterId.isEmpty()) {
                        if (!mSubCenterId.equals("")) {
                            subCenterNameSQL = "SELECT center_name FROM center WHERE center_id='" + mSubCenterId + "'";
                            subCenterName = dbsession.createSQLQuery(subCenterNameSQL).uniqueResult() == null ? "" : dbsession.createSQLQuery(subCenterNameSQL).uniqueResult().toString();
                        }

                        subCenterResponseObj = new JSONObject();
                        subCenterResponseObj.put("subCenterId", mSubCenterId);
                        subCenterResponseObj.put("subCenterName", subCenterName);
                        //  subCenterResponseObjArr.add(subCenterResponseObj);

                        // if (!mUniversityId.isEmpty()) {
                        if (!mUniversityId.equals("")) {
                            universitySQL = dbsession.createSQLQuery("SELECT * FROM university  WHERE university_id='" + mUniversityId + "'");
                            for (Iterator universityItr = universitySQL.list().iterator(); universityItr.hasNext();) {
                                universityObj = (Object[]) universityItr.next();
                                universityShortName = universityObj[1].toString();
                                universityLongName = universityObj[2].toString() == null ? "" : universityObj[2].toString();
                            }
                        }
                        universityResponseObj = new JSONObject();
                        universityResponseObj.put("UniversityId", mUniversityId);
                        universityResponseObj.put("UniversityShortName", universityShortName);
                        universityResponseObj.put("UniversityLongName", universityLongName);
                        //  universityResponseObjArr.add(universityResponseObj);

                        //  totalPhoneNumberCount = "12890"; 
                        totalPhoneNumberCount = total_pages1;
                        //   perSMSCost = "0.25";

                        perSMSCost = perSMSCost1;
                        //   totalCost = "3222.5";

                        // feeAmount = 0.0d;
                        //  double value = Double.valueOf(str);
                        //   feeAmount = Double.valueOf(renewalTypeId) * Double.valueOf(annualFeeAmount);
                        totalCostAmount = Double.valueOf(perSMSCost) * Double.valueOf(messageBodyCount) * Double.valueOf(totalPhoneNumberCount);

                        totalMemberCountObj = new JSONObject();
                        totalMemberCountObj.put("TotalMember", totalPhoneNumberCount);
                        totalMemberCountObj.put("MessageBody", messageBody);
                        totalMemberCountObj.put("MessageBodyCount", messageBodyCount);
                        totalMemberCountObj.put("PerSMSCost", perSMSCost);
                        totalMemberCountObj.put("TotalCost", totalCostAmount);
                        totalMemberCountObj.put("TotalCostAmount", totalCostAmount);
                        totalMemberCountObj.put("MemberType", memberTypeResponseObj);
                        totalMemberCountObj.put("Division", divisionResponseObj);
                        totalMemberCountObj.put("Center", centerResponseObj);
                        totalMemberCountObj.put("SubCenter", subCenterResponseObj);
                        totalMemberCountObj.put("University", universityResponseObj);

                        totalMemberCountObjArr.add(totalMemberCountObj);

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", totalMemberCountObjArr);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
