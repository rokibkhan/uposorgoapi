# IEBAPI
The Institution of Engineers, Bangladesh (IEB) is the most prestigious National Professional Organization of the country. It is registered under the Societies Registration Act of the country. IEB includes all disciplines of engineering. Currently, it has in its roll more than 41,545 engineers with about 30% in the category of Fellows, 60% Members and the rest as Associate Members.
