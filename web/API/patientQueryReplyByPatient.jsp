<%-- 
    Document   : patientQueryReplyByPatient
    Created on : MAY 05, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("patientQueryReplyByPatient_jsp.class");

    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    getRegistryID getId = new getRegistryID();

    String imageBase64String = "";
    // String filePath = GlobalVariable.imageUploadPath;
    //  public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";     //WINDOWS
    //  public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";   //SERVER
    //  String filePath = "D:/NetbeansDevelopment/FFMS/web/upload/";     //WINDOWS
    //  String filePath = "/app/FFMS/webapps/FFMS/upload/";     //SERVER
    //   String filePath = "/app/FFMS/webapps/FFMS/web/upload/";     //SERVER

    // public static final String baseUrlImg = "http://localhost:8084/IEBWEB";
    // public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";  //joybangla
    // public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";         //windows
    String filePath = GlobalVariable.imageUploadPath + "patientcontent/";

    logger.info("patientQueryReplyByPatient API filePath :" + filePath);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String haveAttachment = ""; //1== yes 0==No attachment    
    String patinetContentPicture = "";
    String patinetContentPictureLink = "";

    String pQueryReplyId = "";
    String pQueryDesc = "";
    String pQueryProblemCategoryId = "";
    String pQueryDate = "";
    String pQueryStatus = "1"; //0->UnAnswer;1->Answer
    String pQueryReplyStatus = "0"; //0- No reply required;1->After Answered now reply required
    String pQuerySufferingDate = "";

    int pQueryParent = 0;

    String patientId = "";

    String pQueryCommentType = "";
    String pQueryCommenterId = "";
    int commenterType = 0;//0- Patient comments;1->Doctor comments
    int replyStatus = 1; //0- No reply required;1->After Answered now reply required

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("haveAttachment") && request.getParameterMap().containsKey("pQueryReplyId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        haveAttachment = request.getParameter("haveAttachment").trim();
        imageBase64String = request.getParameter("imageString") == null ? "" : request.getParameter("imageString");
        //imageBase64String = request.getParameter("imageString");
        logger.info("patientQueryReplyByPatient API  imageBase64String :" + imageBase64String);

        System.out.println("patientQueryReplyByPatient API imageBase64String :" + imageBase64String);

        pQueryReplyId = request.getParameter("pQueryReplyId") == null ? "" : request.getParameter("pQueryReplyId").trim();

        pQueryProblemCategoryId = request.getParameter("pQueryProblemCategoryId") == null ? "" : request.getParameter("pQueryProblemCategoryId").trim();
        pQueryDesc = request.getParameter("pQueryDesc") == null ? "" : request.getParameter("pQueryDesc").trim();
        pQuerySufferingDate = request.getParameter("pQuerySufferingDate") == null ? "" : request.getParameter("pQuerySufferingDate").trim();

        patientId = request.getParameter("patientId") == null ? "" : request.getParameter("patientId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: patientQueryReplyByPatient API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    Query patientInfoAddSQL = null;
    Query patientCovidInfoAddSQL = null;

    if (rec == 1) {

        try {

            if (haveAttachment.equals("1")) {

                Random random = new Random();
                int rand1 = Math.abs(random.nextInt());
                int rand2 = Math.abs(random.nextInt());
                DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
                Date dateFileToday = new Date();
                String filePrefixToday = formatterFileToday.format(dateFileToday);

                String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

                logger.info("patientQueryReplyByPatient API filePath :" + filePath);
                logger.info("patientQueryReplyByPatient API fileName1 :" + fileName1);

                System.out.println("patientQueryReplyByPatient API filePath :" + filePath);
                System.out.println("patientQueryReplyByPatient API fileName1 :" + fileName1);

                // contentPostPictureLink = GlobalVariable.baseUrl + "/upload/" + fileName1;
                patinetContentPictureLink = GlobalVariable.baseUrlImg + "/upload/patientcontent/" + fileName1;

                /*
                * Converting a Base64 String into Image byte array 
                 */
                // byte[] imageByteArray = Base64.decodeBase64(imageBase64String);
                byte[] imageByteArray = Base64.getDecoder().decode(imageBase64String);

                /*
                  * Write a image byte array into file system  
                 */
                //  FileOutputStream imageOutFile = new FileOutputStream("/Users/jeeva/Pictures/wallpapers/water-drop-after-convert.jpg");
                FileOutputStream imageOutFile = new FileOutputStream(filePath + fileName1);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();

                patinetContentPicture = fileName1;

            }

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String idS = getId.getID(70);
                        int pQueryId = Integer.parseInt(idS);

                        InetAddress ip;
                        String hostname = "";
                        String ipAddress = "";
                        try {
                            ip = InetAddress.getLocalHost();
                            hostname = ip.getHostName();
                            ipAddress = ip.getHostAddress();

                        } catch (UnknownHostException e) {

                            e.printStackTrace();
                        }

                        String adduser = Integer.toString(memId);
                        adddate = dateFormatX.format(dateToday);
                        String addterm = hostname;
                        String published = "1";

                        pQueryDate = dateFormatX.format(dateToday);

                        DateFormat dateFormatXnc = new SimpleDateFormat("yyyy-MM-dd");
                        Date pQuerySufferingDate1 = dateFormatXnc.parse(pQuerySufferingDate);

                        //prepare Statement
                        PreparedStatement ps = con.prepareStatement("INSERT INTO patient_query("
                                + "query_id,"
                                + "parent,"
                                + "patient_id,"
                                + "problem_category,"
                                + "query_desc,"
                                + "suffering_date,"
                                + "query_date,"
                                + "commenter_id,"
                                + "member_id,"
                                + "add_user,"
                                + "add_date,"
                                + "add_term,"
                                + "add_ip,"
                                + "mod_date,"
                                + "comment_type,"
                                + "attachment) "
                                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                        //  ps.setInt(1, Integer.parseInt(postContentRequestId));
                        ps.setInt(1, pQueryId);
                        ps.setInt(2, Integer.parseInt(pQueryReplyId));
                        ps.setInt(3, Integer.parseInt(patientId));
                        ps.setInt(4, Integer.parseInt(pQueryProblemCategoryId));
                        ps.setString(5, pQueryDesc);
                        ps.setString(6, pQuerySufferingDate);
                        ps.setString(7, adddate);
                        ps.setInt(8, memId);
                        ps.setInt(9, memId);

                        ps.setString(10, adduser);
                        ps.setString(11, adddate);
                        ps.setString(12, addterm);
                        ps.setString(13, ipAddress);
                        ps.setString(14, adddate);
                        ps.setInt(15, commenterType);
                        ps.setString(16, patinetContentPicture);

                        logger.info("SQL Query: " + ps.toString());

                        ps.executeUpdate();

                        //update pQueryReplyId status
                        //update patient_query reply_status field
                        Query updatePatientQueryStatusSQL = dbsession.createSQLQuery("UPDATE  patient_query SET reply_status='1',mod_date='" + adddate + "' WHERE query_id = '" + pQueryReplyId + "'");

                        updatePatientQueryStatusSQL.executeUpdate();
                        ps.close();
                        con.commit();
                        con.close();

                        postContentObj = new JSONObject();
                        postContentObj.put("pQueryId", pQueryId);

                        postContentObj.put("patientId", patientId);

                        postContentObj.put("pQueryProblemCategoryId", pQueryProblemCategoryId);
                        postContentObj.put("pQueryDesc", pQueryDesc);
                        postContentObj.put("pQuerySufferingDate", pQuerySufferingDate);
                        postContentObj.put("pQueryDate", pQueryDate);
                        postContentObj.put("pQueryStatus", pQueryStatus);
                        postContentObj.put("pQueryReplyStatus", pQueryReplyStatus);
                        postContentObj.put("pQueryCommentType", pQueryCommentType);
                        postContentObj.put("pQueryCommenterId", pQueryCommenterId);

                        postContentObj.put("attachment", patinetContentPicture);
                        postContentObj.put("attachmentLink", patinetContentPictureLink);

                        // postContentObj.put("queryOwnerId", queryOwnerId);
                        //   postContentObj.put("queryOwnerName", queryOwnerName);
                        //  postContentObj.put("queryOwnerPic", queryOwnerPicLink);
                        //    postContentObjArr.add(postContentObj);
                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Doctor reply added successfully");
                        logingObj.put("ResponseData", postContentObj);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
