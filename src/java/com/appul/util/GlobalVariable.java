/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

/**
 *
 * @author AKTER & TAHAJJAT
 */
public class GlobalVariable {
 /*
      public static final String baseUrl = "http://www.api.uposorgo.com"; //joybangla
       public static final String baseUrlImg = "http://www.admin.uposorgo.com";//joybangla
       public static final String imagePath = "/app/IEBWEB/adminuposorgo_webapps/ROOT/upload/";  //joybangla
*/
    public static final String baseUrl = "http://localhost:8084/UPOSORGOAPI";
    //public static final String baseUrlImg = "http://localhost:8084/UPOSORGOADMIN";
    public static final String baseUrlImg = "http://admin.uposorgo.com";
    //public static final String imagePath = "D:/NetbeansDevelopment/UPOSORGOADMIN/web/upload/";         //windows
    public static final String imagePath = "/app/IEBWEB/adminuposorgo_webapps/ROOT/upload/";         //server
//    public static final String imagePath = "/Users/Akter/NetBeansProjects/IEBWEB/web/upload/";  //MAC

    public static final String imageUploadPath = imagePath;
    public static final String imageDirLink = baseUrlImg + "/upload/";

    public static final String imagePhotoUploadPath = imagePath + "photos/";
    public static final String imagePhotoDirLink = baseUrlImg + "/upload/photos/";

    public static final String imageMediaUploadPath = imagePath + "media/";
    public static final String imageMediaDirLink = baseUrlImg + "/upload/media/";

    public static final String imageNewsUploadPath = imagePath + "news/";
    public static final String imageNewsDirLink = baseUrlImg + "/upload/news/";

    public static final String noticeUploadPath = imagePath + "notice/";
    public static final String noticeDirLink = baseUrlImg + "/upload/notice/";

    public static final String imageMemberUploadPath = imagePath + "member/";
    public static final String imageMemberDirLink = baseUrlImg + "/upload/member/";

    public static final String journalUploadPath = imagePath + "journal/";
    public static final String journalDirLink = baseUrlImg + "/upload/journal/";

    public static final String engineeringnewsUploadPath = imagePath + "engineeringnews/";
    public static final String engineeringnewsDirLink = baseUrlImg + "/upload/engineeringnews/";

    public static final String seminarPPTXUploadPath = imagePath + "seminarpptx/";
    public static final String seminarPPTXDirLink = baseUrlImg + "/upload/seminarpptx/";

    public static final String imageMemberDocumentUploadPath = imagePath + "document/";
    public static final String imageMemberDocumentDirLink = baseUrlImg + "/upload/document/";

    public static final String sendUrlHost = "www.bangladeshsms.com";
    public static final String sendUrlApi = "/smsapi";
    public static final String apiKey = "C20013475bbdd6cbdd2ac1.58430946";
    public static final String type = "text";
    public static final String senderid = "8801844502012";

    public static final String SMTPHost = "mail.apparelunlimited.net";
    public static final String SMTPUserName = "admin@apparelunlimited.net";
    public static final String SMTPUserPassword = "admin1234$#@!";

    /*
    public static final String SMTPHost = "smtp.gmail.com";
    // public static final String SMTPUserName = "iebreg2019@gmail.com";
    public static final String SMTPUserName = "joybanglareg2020@gmail.com";
    public static final String SMTPUserPassword = "Admin@321!";
     */
    public static final String fromEmailId = "tahajjat.tuhin@gmail.com";
    public static final String fromEmailIdCustom = "IEB Registration <tahajjat.tuhin@gmail.com>";
    public static final String bccEmailId = "tahajjat.tuhin@gmail.com";

//    public static final String SSLCommerzStoreId = "ieb5c9c8b9a864bf";
//    public static final String SSLCommerzStorePass = "ieb5c9c8b9a864bf@ssl";
//    // If Test Mode then insert true and false otherwise.
//    public static final String SSLCommerzStoreMode = "true";
//    //  public static final String SSLCommerzStoreMode = "false";
    //  public static final String SSLCommerzStoreId = "bwpeaorglive";
    //   public static final String SSLCommerzStorePass = "5D25662931C7147965";
    public static final String SSLCommerzStoreId = "iebbdorglive";
    public static final String SSLCommerzStorePass = "5DB17F6C7D1F254068";
    public static final String SSLCommerzStoreMode = "false";

    public static final String GOOGLE_MAP_API_KEY = "AIzaSyBWLjkIs1ztRCG8Zu5QtkxISs8RVXBGvsM";

    // public static final String GOOGLE_FCM_API_KEY = "AAAAUXXPia0:APA91bHX_cDXf4hsjgkIzxoVeHyme-iq2DFtugLAEXqtXxRCDwWVLw0UmquxqDIaQWai50YiA9uvwwsiet82aaczsnK7nz5J2gC71QzJIS2fcqF3jEx-iQqtr3_Qdrcc8TODO2HVMukP";
    //joybangla API Key
    public static final String GOOGLE_FCM_API_KEY = "AAAAvYo6H1w:APA91bE61fUKoWp_dkZx7SS5Jc95iMZc5cNhAZGJfu0GX-UvtJmfY9MCW1DvjUcIfUvMrO6b36fIb2bIH-hcYVOtC9XOJR0FiR1dm2F8uYKfQU-grJP69rH-7hB6AHLCh_u_P1LRgTxr";
}
