/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.util;

/**
 *
 * @author Akter
 */
import org.hibernate.*;
import org.apache.log4j.*;

public class HBMConnection {
    
    public Session getSession(){

        Logger logger=Logger.getLogger(this.getClass().getName());
        Session dbsession = null;                    
        org.hibernate.Transaction dbtrx = null;
    
        //logger.info("entered init() method");
                    
                    while (dbsession==null || dbsession.isOpen()==false || dbsession.isConnected()==false){
                        logger.info("trying database connection");                        
                        try{
                            dbsession = HibernateUtil.getSessionFactory().openSession();
                            //dbsession = HibernateUtil.getSession();
                            logger.info("DBSESS:"+dbsession);
                        } catch (Exception e){
                            //logger.info(e.getMessage());
                            logger.info("ERROR while connecting database");
                            dbsession=null;
                        }                        
                    }
        return dbsession;
    }

public Session closeSession(Session dbsession){

        Logger logger=Logger.getLogger(this.getClass().getName());               
        org.hibernate.Transaction dbtrx = null;

        while (dbsession.isConnected()==true){
            //logger.info("trying database disconnection");                        
            try{                
                dbsession.disconnect();
                //dbsession = HibernateUtil.getSession();
                if (dbsession.isConnected()==false){
                    logger.info("DBSESS:"+dbsession+"is Disconnected");
                }
                
            } catch (Exception e){
                //logger.info(e.getMessage());
                logger.info("ERROR while disconnecting database");
                dbsession=null;
            }                        
        }
    return dbsession;
    }
}
