/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "CustomerDocUpload", urlPatterns = {"/CustomerDocUpload"})
@MultipartConfig
public class CustomerDocUpload extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String userId = request.getSession(true).getAttribute("username").toString();

            Part docNameP = request.getPart("docName");
            Part consumerIdP = request.getPart("customerId");
            Part file1P = request.getPart("file1");
            
            String fileName = file1P.getSubmittedFileName()==null?"":file1P.getSubmittedFileName();

            Scanner docNameS = new Scanner(docNameP.getInputStream());
            Scanner consumerIdS = new Scanner(consumerIdP.getInputStream());

            String docName = docNameS.nextLine();
            String consumerId = consumerIdS.nextLine();

       

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            String entryDate = dateFormat.format(date);
            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            PreparedStatement ps = con.prepareStatement("insert into attachment(consumer_id,doc_name,attachment,file_name,add_user,add_dt) values(?,?,?,?,?,?)");

            ps.setString(1, consumerId);
            ps.setString(2, docName);
            ps.setBinaryStream(3, file1P.getInputStream(), (int) file1P.getSize());
            ps.setString(4, fileName);
            ps.setString(5, userId);
            ps.setString(6, entryDate);

            ps.executeUpdate();
            con.commit();
            con.close();
            dbsession.close();
            out.println("Successfully Upload Document.");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            out.close();
        }
    }
}
