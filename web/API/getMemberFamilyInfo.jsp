<%-- 
    Document   : getMemberFamilyInfo
    Created on : Nov 22, 2020, 12:40:47 PM
    Author     : HP
--%>



<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getMemberFamilyInfo.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String patientId = "";
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();
        patientId = request.getParameter("patientId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query familySQL = null;
    Object familyObj[] = null;

    String relationId = "";
    String fullName = "";
    String familyMemberId = "";

    JSONArray relationResponseObjArr = new JSONArray();
    JSONObject relationResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    if (rec == 1) {

        try {

            //  familySQL = dbsession.createSQLQuery("SELECT * FROM center WHERE center_type_id ='3' ORDER BY center_id ASC");
                familySQL = dbsession.createSQLQuery("SELECT relative_id,full_name,id FROM member_family_info WHERE member_id ='"+patientId+"'");

            if (!familySQL.list().isEmpty()) {
                for (Iterator it = familySQL.list().iterator(); it.hasNext();) {

                    familyObj = (Object[]) it.next();
                    
                    relationId = familyObj[0].toString();
                    fullName = familyObj[1].toString();
                    familyMemberId = familyObj[2].toString();

                    relationResponseObj = new JSONObject();
                    
                    Query memberRelationNameSQL = dbsession.createSQLQuery("SELECT relation_name FROM family_relation_info where id='" + relationId + "'");
                    String relationName = memberRelationNameSQL.uniqueResult() == null ? "" : memberRelationNameSQL.uniqueResult().toString();

                    relationResponseObj.put("Id", familyMemberId);
                    relationResponseObj.put("Name", fullName);
                    relationResponseObj.put("RelationId", relationId);
                    relationResponseObj.put("RelationName", relationName);

                    relationResponseObjArr.add(relationResponseObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", relationResponseObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", relationResponseObjArr);

            }
        } catch (Exception e) {

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", relationResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
