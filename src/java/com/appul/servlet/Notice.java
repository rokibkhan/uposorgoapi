/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.entity.NewsCategory;
import com.appul.entity.NewsInfo;
import com.appul.entity.PostNotice;
import com.appul.util.DBAccess;
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.hibernate.Query;

import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "Notice", urlPatterns = {"/Notice"})
@MultipartConfig
public class Notice extends HttpServlet {

    private boolean isMultipart;
    private String filePath;

    // private int maxFileSize = 500 * 1024;
    //   private int maxMemSize = 400 * 1024;
    private int maxFileSize = 5 * 1024 * 1024;  //5242880; // 5MB -> 5 * 1024 * 1024
    private int maxMemSize = 4 * 1024 * 1024;
    private File file;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        Session dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();

        getRegistryID getId = new getRegistryID();
        String idS = getId.getID(36);
        int noticesId = Integer.parseInt(idS);

        String sessionIdH = "";
        String userNameH = "";
        String strMsg = "";

        HttpSession session = request.getSession(false);
        userNameH = session.getAttribute("username").toString().toUpperCase();
        sessionIdH = session.getId();

        filePath = GlobalVariable.noticeUploadPath;

        // Check that we have a file upload request
        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        //  java.io.PrintWriter out = response.getWriter();

        if (!isMultipart) {

            strMsg = "ERROR!!! Form enctype type is not multipart/form-data";
            response.sendRedirect(GlobalVariable.baseUrl + "/noticesManagement/noticesAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
            return;
        }

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
        upload.setSizeMax(maxFileSize);

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);

            // Process the uploaded file items
            Iterator item = fileItems.iterator();

            Random random = new Random();
            int rand1 = Math.abs(random.nextInt());
            int rand2 = Math.abs(random.nextInt());

            DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
            Date dateFileToday = new Date();
            String filePrefixToday = formatterFileToday.format(dateFileToday);

            // long uniqueNumber = System.currentTimeMillis();
            String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".PDF";

            int noticeOfficeId = rand1;

            String name = "";
            String value = "";
            String noticeTitle = "";
            String noticeDesc = "";
            String noticeShortDesc = "";
            String noticeDate = "";
            String sticky = "";

            String fieldName = "";
            String fileName = "";
            String contentType = "";

            while (item.hasNext()) {
                FileItem fi = (FileItem) item.next();
                if (!fi.isFormField()) {

                    // Get the uploaded file parameters
                    fieldName = fi.getFieldName();
                    System.out.println("FileName:: " + fieldName);
                    fileName = fi.getName();
                    contentType = fi.getContentType();
                    System.out.println("FileName contentType:: " + contentType);
                    boolean isInMemory = fi.isInMemory();
                    long sizeInBytes = fi.getSize();

                    // Write the file
                    if (fileName.lastIndexOf("\\") >= 0) {
                        //  System.out.println("FileName0::" + fileName.substring(fileName.lastIndexOf("\\")));
                        //   file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
                        file = new File(filePath + fileName1);
                    } else {
                        //  System.out.println("FileName::" + fileName.substring(fileName.lastIndexOf("\\")));
                        //    file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        file = new File(filePath + fileName1);
                    }
                    fi.write(file);
                    System.out.println("Uploaded Filename: " + fileName + "<br>");
                    System.out.println("Uploaded Filename: " + fileName1 + "<br>");
                } else {

                    name = fi.getFieldName();
                    value = fi.getString();
                    System.out.println("FileName name:: " + name);
                    System.out.println("FileName value:: " + value);
                }

                if (name.equalsIgnoreCase("noticeDate")) {
                    noticeDate = value;
                }
                if (name.equalsIgnoreCase("noticeTitle")) {
                    noticeTitle = value;
                }

                if (name.equalsIgnoreCase("noticeDesc")) {
                    noticeDesc = value;
                }

                if (name.equalsIgnoreCase("noticeShortDesc")) {
                    noticeShortDesc = value;
                }

//                if (name.equalsIgnoreCase("sticky")) {
//                    sticky = value;
//                }
            }

//            out.println("photoCaption: " + photoCaption + "<br>");
//            out.println("fileName1: " + fileName1 + "<br>");
            System.out.println("Photo userNameHAA:: " + userNameH);
            System.out.println("Photo sessionIdHAA:: " + sessionIdH);

            //  if (session.getAttribute("username") != null && session.getAttribute("companyId") != null) {
            //if (session.getAttribute("username") != null && session.getAttribute("storeId") != null) {
            DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateX = new Date();
            String adddate = dateFormatX.format(dateX);

            Date noticeDate1 = dateFormatX.parse(noticeDate);

            String adduser = userNameH;
            String addterm = InetAddress.getLocalHost().getHostName().toString();
            String addip = InetAddress.getLocalHost().getHostAddress().toString();

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String entryDate = dateFormat.format(date);

            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();

            //insert gallary
            String newsCategoryId = "1";
            String pictureName = fileName1;
            String pictureThumb = fileName1;
            String showingOrder = "1";
            String news_date_time = "";

            String fileNameUrl = GlobalVariable.noticeDirLink + fileName1;

            /*
            
            
            Query q4 = dbsession.createSQLQuery("INSERT INTO post_notice("
                    + "id_notice_content,"
                    + "noticeOfficeId,"
                    + "notice_content_title,"
                    + "notice_short_desc,"
                    + "notice_desc,"
                    + "notice_date,"
                    + "notice_link,"
                    + "add_date,"
                    + "add_user,"
                    + "add_term,"
                    + "add_ip) values( "
                    + "'" + noticesId + "',"
                    + "'" + noticeOfficeId + "',"
                    + "'" + noticeTitle + "',"
                    + "'" + noticeShortDesc + "',"
                    + "'" + noticeDesc + "',"
                    + "'" + adddate + "',"
                    + "'" + fileNameUrl + "',"
                    + "'" + adddate + "',"
                    + "'" + adduser + "',"
                    + "'" + addterm + "',"
                    + "'" + addip + "')");

            System.out.println("SQL::" + q4);

            q4.executeUpdate();
             */
            PostNotice notice = new PostNotice();

            notice.setIdNoticeContent(noticesId);
            notice.setNoticeOfficeId(String.valueOf(noticeOfficeId));
            notice.setNoticeContentTitle(noticeTitle);
            notice.setNoticeShortDesc(noticeShortDesc);
            notice.setNoticeDesc(noticeDesc);
            notice.setNoticeDate(noticeDate1);
            notice.setNoticeLink(fileNameUrl);
            notice.setAddDate(adddate);
            notice.setAddUser(adduser);
            notice.setAddTerm(addterm);
            notice.setAddIp(addip);
            dbsession.save(notice);


           
            dbtrx.commit();
            dbsession.flush();
            dbsession.close();

            if (dbtrx.wasCommitted()) {

                strMsg = "Notice Added Successfully";
                response.sendRedirect(GlobalVariable.baseUrl + "/noticesManagement/noticesAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                return;
            } else {
                dbtrx.rollback();

                strMsg = "Error!!! When Notice add";
                response.sendRedirect(GlobalVariable.baseUrl + "/noticesManagement/noticesAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
                return;
            }

            //    out.println("photoCaption: " + photoCaption + "<br>");
            //    out.println("fileName1: " + fileName1 + "<br>");
        } catch (Exception ex) {
            System.out.println(ex);
        }

        strMsg = "Error!!! When adding Notice.Please try again";
        response.sendRedirect(GlobalVariable.baseUrl + "/noticesManagement/noticesAdd.jsp?sessionid=" + sessionIdH + "&strMsg=" + strMsg);
        return;
    }
}
