<%-- 
    Document   : getAllDistrictAllThanaInfo
    Created on : OCT 28, 2019, 1:32:05 PM
    Author     : AKTER & TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getAllDistrictAllThanaInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query districtSQL = null;
    Object districtObj[] = null;
    String districtId = "";
    String districtName = "";

    JSONArray jsonDistrictObjArr = new JSONArray();
    JSONObject jsonDistrictObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    Query thanaSQL = null;
    Object thanaObj[] = null;
    String thanaId = "";
    String thanaName = "";

    JSONArray jsonThanaObjArr = new JSONArray();
    JSONObject jsonThanaObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("JoyBangla :: API :: getAllDistrictAllThanaInfo rec :: " + rec);
    if (rec == 1) {

        try {

            districtSQL = dbsession.createSQLQuery("SELECT * FROM district  ORDER BY DISTRICT_NAME  ASC");

            if (!districtSQL.list().isEmpty()) {
                for (Iterator it = districtSQL.list().iterator(); it.hasNext();) {

                    districtObj = (Object[]) it.next();
                    districtId = districtObj[0].toString().trim();
                    districtName = districtObj[1].toString();

                    jsonDistrictObj = new JSONObject();

                    jsonDistrictObj.put("districtId", districtId);
                    jsonDistrictObj.put("districtName", districtName);

                    thanaSQL = dbsession.createSQLQuery("SELECT  * FROM thana  WHERE DISTRICT_ID ='" + districtId + "' ORDER BY THANA_NAME  ASC");

                    for (Iterator thanaItr = thanaSQL.list().iterator(); thanaItr.hasNext();) {

                        thanaObj = (Object[]) thanaItr.next();
                        thanaId = thanaObj[0].toString().trim();
                        thanaName = thanaObj[1].toString();

                        jsonThanaObj = new JSONObject();

                        jsonThanaObj.put("thanaId", thanaId);
                        jsonThanaObj.put("thanaName", thanaName);

                        jsonThanaObjArr.add(jsonThanaObj);

                    }

                    jsonDistrictObj.put("thana", jsonThanaObjArr);
                    jsonThanaObjArr = new JSONArray();

                    jsonDistrictObjArr.add(jsonDistrictObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", jsonDistrictObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", jsonDistrictObjArr);

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", jsonDistrictObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>