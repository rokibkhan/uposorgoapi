<%-- 
    Document   : doctorDiscountFeeInfoAdd
    Created on : Oct 31, 2020, 8:52:24 PM
    Author     : rokib
--%>


<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>



<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorDiscountFeeInfoAdd_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String doctorId = "";
    String discountType = "";
    String fixedAmount = "0.0000";
    String percentageAmount = "0.0000";
    String discountAmount  = "";
    String ed = "";
    String td = "";
    String givenPassword = "";
    String doctorCategoryId = "";
    getRegistryID getId = new getRegistryID();
    String adddate = "";
    //String memberId = "";
    String appointmentDate = "";
    
    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId")) {
    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

        //  memberId = request.getParameter("memberId").trim();
        //   givenPassword = request.getParameter("password").trim();
        doctorId = request.getParameter("doctorId").trim();
        discountType = request.getParameter("discountType").trim();
        discountAmount = request.getParameter("discountAmount").trim();
        ed = request.getParameter("ed").trim();
        td = request.getParameter("td").trim();
        System.out.println("doctorId :: " + doctorId);
        //  System.out.println("appointmentDate :: " + appointmentDate);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    Query doctorInfoSQL = null;
    Query doctorDiscountFeeInfoUpdSQl = null;
    Object memberObj[] = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    String feeAddDate = "";
    String memberXId = "";
    String appointmentId = "";
    String appointmentNo = "";
    Query mDoctorDiscountSQL = null;
    Object[] mDoctorDiscountObj = null;
    Query doctorScheduleSQL = null;
    Object doctorScheduleObj[] = null;
    
    String doctorDiscountId = "";
    String doctorDiscountType = "";
    String doctorDiscountTypeText = "";
    String doctorDiscountFixedAmount = "";
    String doctorDiscountPercentageAmount = "";

    String doctorDiscountTD = "";
    String doctorDiscountED = "";
    
    
    Query mDoctorFeeSQL = null;
    Object[] mDoctorFeeObj = null;
    
    String doctorFeeId = "";
    String doctorFeeAmount = "";
    Double doctorDiscontPercentageFee = 0.0d;
    
    //Date dateToday = new Date();
    String today = new SimpleDateFormat("yyyy-MM-dd").format(dateToday);
    
    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: patientAppointmentList API rec :: " + rec);

    if (rec == 1) {

        try {
            
            
                Query memberTypeIdSQL = dbsession.createSQLQuery("SELECT member_type_id FROM member_type where member_id='" + doctorId + "'");
                String memberTypeId = memberTypeIdSQL.uniqueResult() == null ? "" : memberTypeIdSQL.uniqueResult().toString();

                System.out.println("memberTypeId :: " + memberTypeId);
                
                
                if(memberTypeId.equals("2"))
                {
                    String adduser = doctorId;
                    adddate = dateFormatX.format(dateToday);
                    String customOrderby = "0";
                    String published = "0";
                    
                    String idS = getId.getID(82);
                    int doctorDiscountFeeId = Integer.parseInt(idS);

                    feeAddDate = dateFormatX.format(dateToday);
                    
                    if(discountType.equals("0"))
                    {
                        fixedAmount = discountAmount;
                    }
                    else if(discountType.equals("1"))
                    {
                        percentageAmount = discountAmount;
                        
                         mDoctorFeeSQL = dbsession.createSQLQuery("SELECT id_fee,fee_amount,ed,td FROM doctor_fee_info WHERE doctor_id='" + doctorId + "'"
                    + " and ed <='" 
                            +today +"' and td >= '" + today + "'");
                         
                    if (!mDoctorFeeSQL.list().isEmpty()) {
                        for (Iterator mDoctorFeeItr = mDoctorFeeSQL.list().iterator(); mDoctorFeeItr.hasNext();) {

                            mDoctorFeeObj = (Object[]) mDoctorFeeItr.next();
                            doctorFeeId = mDoctorFeeObj[0].toString();
                            doctorFeeAmount = mDoctorFeeObj[1].toString();
                            
                            doctorDiscontPercentageFee = ((Double.parseDouble(doctorFeeAmount)* Double.parseDouble(percentageAmount)) / 100);
                            //doctorFeeTD = mDoctorFeeObj[2].toString();
                            //doctorFeeED = mDoctorFeeObj[3].toString();

                        }
                    } else {

                        //doctorFeeAmount = "0.00";
                        //doctorFeeTD = "";
                        //doctorFeeED = "";
                        

                        responseDataObj = new JSONObject();
                        responseDataObj.put("ResponseCode", "0");
                        responseDataObj.put("ResponseText", "ERROR!!! Doctor Fee not found.");
                        //responseDataObj.put("ResponseData", logingObjArray);
                        logger.info("ERROR!!! Doctor Fee not found.");
                        PrintWriter writer = response.getWriter();
                        writer.write(responseDataObj.toJSONString());
                        response.getWriter().flush();
                        response.getWriter().close();
                        return;
                    }
                    
                   }
                
                     
                    
                    
                    InetAddress ip;
                    String hostname = "";
                    String ipAddress = "";
                    try {
                        ip = InetAddress.getLocalHost();
                        hostname = ip.getHostName();
                        ipAddress = ip.getHostAddress();

                    } catch (UnknownHostException e) {

                        e.printStackTrace();
                    }
                    
                    
                     mDoctorDiscountSQL = dbsession.createSQLQuery("SELECT ID,DISCOUNT_TYPE,FIXED_AMOUNT,PERCENTAGE_AMOUNT,TD,ED FROM doctor_discount WHERE doctor_id='" + doctorId + "'"
                     + " and ED <='" 
                            +ed +"' and TD >= '" + ed + "'");
                                if (!mDoctorDiscountSQL.list().isEmpty()) {
                                    for (Iterator mDoctorDiscountItr = mDoctorDiscountSQL.list().iterator(); mDoctorDiscountItr.hasNext();) {

                                        mDoctorDiscountObj = (Object[]) mDoctorDiscountItr.next();
                                        doctorDiscountId = mDoctorDiscountObj[0].toString();
                                        doctorDiscountType = mDoctorDiscountObj[1] == null ? "" : mDoctorDiscountObj[1].toString();

                                        doctorDiscountFixedAmount = mDoctorDiscountObj[2] == null ? "" : mDoctorDiscountObj[2].toString();
                                        doctorDiscountPercentageAmount = mDoctorDiscountObj[3] == null ? "" : mDoctorDiscountObj[3].toString();
                                    }
                                    
                                    doctorScheduleSQL = dbsession.createSQLQuery("SELECT das.id,das.slotstart_time,das.slotend_time, "
                                            + "das.slot_name,das.name,das.status,das.blocked_status "
                                            + "FROM doctor_aponmnt_slot das join doctor_patient_aponmnt dpa on dpa.aponmnt_slot_id =das.id  "
                                            + " WHERE das.slot_date >= '" + ed + "'  and dpa.status in( '0','1') "
                                            + " and dpa.doctor_id = '"+ doctorId +"' ");          
                                            
                                            if (!doctorScheduleSQL.list().isEmpty()) {
                                                
                                                responseDataObj = new JSONObject();
                                                responseDataObj.put("ResponseCode", "0");
                                                responseDataObj.put("ResponseText", "ERROR!!! Patient already booked this date ");
                                                //responseDataObj.put("ResponseData", logingObjArray);
                                                logger.info("ERROR!!! Patient already booked this date ");
                                                PrintWriter writer = response.getWriter();
                                                writer.write(responseDataObj.toJSONString());
                                                response.getWriter().flush();
                                                response.getWriter().close();
                                                return;
                                            
                                            }

                                    
                                   Date tillDate = new SimpleDateFormat("yyyy-MM-dd").parse(ed);  
                                   Calendar calendar = Calendar.getInstance();
                                   calendar.setTime(tillDate);
                                   calendar.add(Calendar.DATE, -1);
                                   Date updateDate = calendar.getTime();
                                   String updateTillDate = new SimpleDateFormat("yyyy-MM-dd").format(updateDate);
                                   

                                    doctorDiscountFeeInfoUpdSQl = dbsession.createSQLQuery("UPDATE  doctor_discount SET TD ='"+updateTillDate +"',MOD_USER ='"+adduser +"',MOD_DATE ='"+adddate +"'  WHERE ID='" + doctorDiscountId + "'");
                                    doctorDiscountFeeInfoUpdSQl.executeUpdate();
                                    
                                    doctorInfoSQL = dbsession.createSQLQuery("INSERT INTO doctor_discount("
                                        + "ID,"
                                        + "doctor_id,"
                                        + "DISCOUNT_TYPE,"
                                        + "FIXED_AMOUNT,"
                                        + "PERCENTAGE_AMOUNT,"
                                        + "ed,"
                                        + "td,"
                                        + "status,"
                                        + "add_user,"
                                        + "add_date,"
                                        
                                        + "add_ip"
                                        + ") VALUES("
                                        + "'" + doctorDiscountFeeId + "',"
                                        + "'" + doctorId + "',"
                                        + "'" + discountType + "',"
                                        + "'" + fixedAmount + "',"
                                        + "'" + Math.round(doctorDiscontPercentageFee) + "',"
                                        + "'" + ed + "',"
                                        + "'" + "2999-12-31" + "',"
                                        + "'" + 1 + "',"                                                
                                        + "'" + adduser + "',"
                                        + "'" + adddate + "',"
                                        
                                        + "'" + ipAddress + "'"
                                        + "  ) ");
                    
                                    doctorInfoSQL.executeUpdate();
                                    
                                }
                                else
                                {
                                    
                                    doctorInfoSQL = dbsession.createSQLQuery("INSERT INTO doctor_discount("
                                        + "ID,"
                                        + "doctor_id,"
                                        + "DISCOUNT_TYPE,"
                                        + "FIXED_AMOUNT,"
                                        + "PERCENTAGE_AMOUNT,"
                                        + "ed,"
                                        + "td,"
                                        + "status,"
                                        + "add_user,"
                                        + "add_date,"
                                        
                                        + "add_ip"
                                        + ") VALUES("
                                        + "'" + doctorDiscountFeeId + "',"
                                        + "'" + doctorId + "',"
                                        + "'" + discountType + "',"
                                        + "'" + fixedAmount + "',"
                                        + "'" + Math.round(doctorDiscontPercentageFee) + "',"
                                        + "'" + ed + "',"
                                        + "'" + "2999-12-31" + "',"
                                        + "'" + 1 + "',"                                                
                                        + "'" + adduser + "',"
                                        + "'" + adddate + "',"
                                        
                                        + "'" + ipAddress + "'"
                                        + "  ) ");
                    
                                    doctorInfoSQL.executeUpdate();
                                    
                                }
                    
                    


                                    
                                    
                                    dbtrx.commit();

                                if (dbtrx.wasCommitted()) {

                                    responseDataObj = new JSONObject();

                                    responseDataObj = new JSONObject();
                                    responseDataObj.put("ResponseCode", "1");
                                    responseDataObj.put("ResponseText", "Doctor Discount Fee Info added successfully");
                                    //responseDataObj.put("ResponseData", postContentObj);
                                } else {

                                    responseDataObj = new JSONObject();
                                    responseDataObj.put("ResponseCode", "0");
                                    responseDataObj.put("ResponseText", "ERROR!!! When Post like added ");
                                    //responseDataObj.put("ResponseData", logingObjArray);
                                    logger.info("ERROR!!! When Doctor Fee Info added");
                                }
                 

                }
                else
                {
                        bannerObj = new JSONObject();
                        bannerObj.put("ResponseCode", "0");
                        bannerObj.put("ResponseText", "Not a doctor ID.");
                        bannerObj.put("ResponseData", bannerObjArray);
                        PrintWriter writer = response.getWriter();
                        writer.write(bannerObj.toJSONString());
                        response.getWriter().flush();
                        response.getWriter().close();

                         return;
                }


                 

            
        } catch (Exception e) {
            responseDataObj = new JSONObject();
            responseDataObj.put("ResponseCode", "0");
            responseDataObj.put("ResponseText", "Something went wrong!");
            //responseDataObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>


