<%-- 
    Document   : paymentTransationStatusUpdate
    Created on : NOV 19, 2019, 08:44:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String transId = "";
    String appointmentSlotId = "";
    String transStatus = "";
    String transRefNo = "";
    String transRefDesc = "";
    //String 

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("transId") && request.getParameterMap().containsKey("transStatus") && request.getParameterMap().containsKey("transRefNo") && request.getParameterMap().containsKey("transRefDesc") && request.getParameterMap().containsKey("appointmentSlotId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();        
        transId = request.getParameter("transId").trim();
        transStatus = request.getParameter("transStatus").trim();
        transRefNo = request.getParameter("transRefNo").trim();
        transRefDesc = request.getParameter("transRefDesc").trim();
        appointmentSlotId = request.getParameter("appointmentSlotId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    
    // Consumer consumer = null;
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("API :: paymentTransationStatusUpdate rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject approvalRequestObj = new JSONObject();
    JSONArray approvalRequestObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;
    Query feeCheckSQL = null;
    Query feeUpdSQL = null;
    Query doctorPatientAponmentUpdSQl = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String transactionId = "";
    String transactionDate = "";
    String transactionStatus = "";
    String transactionStatusText = "";
    String billAmount = "";
    String referenceNo = "";
    String referenceDesc = "";
    String transactionType = "";
    String paidDate = "";
    String dueDate = "";
    String feeYear = "";
    String paymentOption = "";

    if (rec == 1) {

        try {

            qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        feeCheckSQL = dbsession.createSQLQuery("SELECT * FROM member_fee WHERE member_id = '" + memId + "' AND txn_id='" + transId + "' AND status='" + transStatus + "'");

                        logger.info("feeCheckSQL ::" + feeCheckSQL);

                        if (feeCheckSQL.list().isEmpty()) {

                            // String ref_description = "tranId::" + tranId + " ::valId::" + valId + " ::cardType::" + cardType + " ::cardNo::" + cardNo + " ::bankTransactionID::" + bankTransactionID + " ::cardIssuerBank::" + cardIssuerBank + " ::cardBrand::" + cardBrand + " ::cardBrandTranDate::" + cardBrandTranDate + " ::cardBrandTranCurrencyAmount::" + cardBrandTranCurrencyAmount + " ::cardBrandTranCurrencyType::" + cardBrandTranCurrencyType;
                            // String ref_no = bankTransactionID;
                            feeUpdSQL = dbsession.createSQLQuery("UPDATE  member_fee SET ref_no='" + transRefNo + "',ref_description ='" + transRefDesc + "',pay_option = '1',mod_user ='" + memId + "',mod_date=now(),status='" + transStatus + "' WHERE txn_id='" + transId + "'");

                            feeUpdSQL.executeUpdate();
                            
                            
                            //update doctor patient aponmnt status
                            doctorPatientAponmentUpdSQl = dbsession.createSQLQuery("UPDATE  doctor_patient_aponmnt SET payment_refno = '" + transRefNo + "',payment_refdesc ='" + transRefDesc +"' ,mod_user ='" + memId + "',mod_date=now(),status='" + transStatus + "'  WHERE aponmnt_slot_id='" + appointmentSlotId + "'");
                            doctorPatientAponmentUpdSQl.executeUpdate();
                            
                            
                            Query updateDoctorTimeSlotStatusSQL = dbsession.createSQLQuery("UPDATE  doctor_aponmnt_slot SET status = '2', mod_date= now()   WHERE id = '" + appointmentSlotId + "'");
                            updateDoctorTimeSlotStatusSQL.executeUpdate();


                            approvalRequestObj = new JSONObject();
                            approvalRequestObj.put("TransactionId", transId);
                            approvalRequestObj.put("TransactionStatus", transStatus);
                            approvalRequestObj.put("ReferenceNo", transRefNo);
                            approvalRequestObj.put("ReferenceDesc", transRefDesc);

                            approvalRequestObjArr.add(approvalRequestObj);

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "1");
                            logingObj.put("ResponseText", "Found");
                            logingObj.put("ResponseData", approvalRequestObjArr);

                        } else {
                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "0");
                            logingObj.put("ResponseText", "Error!!! Trnasaction status already updated");
                            logingObj.put("ResponseData", approvalRequestObjArr);
                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Not Found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } finally {
            dbtrx.commit();

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
