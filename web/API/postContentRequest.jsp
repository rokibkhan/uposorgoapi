<%-- 
    Document   : postContentRequest
    Created on : APR 28, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("postContentRequest_jsp.class");

    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    SendFirebasePushNotification fcmsend = new SendFirebasePushNotification();
    getRegistryID getId = new getRegistryID();

    String imageBase64String = "";
    
    
    // String filePath = GlobalVariable.imageUploadPath;
    //  public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";     //WINDOWS
    //  public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";   //SERVER
    //  String filePath = "D:/NetbeansDevelopment/FFMS/web/upload/";     //WINDOWS
    //  String filePath = "/app/FFMS/webapps/FFMS/upload/";     //SERVER
    //   String filePath = "/app/FFMS/webapps/FFMS/web/upload/";     //SERVER

    // public static final String baseUrlImg = "http://localhost:8084/IEBWEB";
    // public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";  //joybangla
    // public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";         //windows
    String filePath = GlobalVariable.imageUploadPath + "postcontent/";

    logger.info("postContentRequest API filePath :" + filePath);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String postContentType = "";
    String postContentCategory = "";
    String postContentTitle = "";
    String postContentShortDesc = "";
    String postContentDesc = "";
    String postContentPostLink = "";
    String postContentPicture = "";
    String postContentDate = "";
    String adddate = "";
    String PostdiseaseCategoryId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("contentType")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        postContentType = request.getParameter("contentType").trim();
        postContentCategory = request.getParameter("contentCategory") == null ? "" : request.getParameter("contentCategory").trim();
        postContentTitle = request.getParameter("contentTitle") == null ? "" : request.getParameter("contentTitle").trim();
        postContentShortDesc = request.getParameter("contentShortDesc") == null ? "" : request.getParameter("contentShortDesc").trim();
        postContentDesc = request.getParameter("contentDesc") == null ? "" : request.getParameter("contentDesc").trim();
        postContentPostLink = request.getParameter("contentPostLink") == null ? "" : request.getParameter("contentPostLink").trim();

        imageBase64String = request.getParameter("imageString") == null ? "" : request.getParameter("imageString");
        
        PostdiseaseCategoryId = request.getParameter("diseaseCategoryId").trim();
        
        logger.info("postContentRequest API  imageBase64String :" + imageBase64String);

        logger.info("postContentRequest API imageBase64String :" + imageBase64String);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: postContentRequest API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String pContentSQL = null;
    String pContentCountSQL = null;
    Query pContentSQLQry = null;
    Object[] pContentObj = null;

    String contentId = "";
    String contentCategory = "";
    String contentType = "";
    String contentTitle = "";
    String contentShortDesc = "";
    String contentDesc = "";
    String contentDate = "";
    String contentPostLink = "";
    String contentPostPicture = "";
    String contentPostPictureLink = "";

    String memberIEBId = "";
    String memberIEBIdFirstChar = "";
    String memberPictureName = "";
    String memberPictureLink = "";

    String ownContentFlag = "";

    Query postContentRequestAddSQL = null;

    Query contentRatingAddSQL = null;

    String memMemberName = "";

    if (rec == 1) {

        try {

            if (postContentType.equalsIgnoreCase("PC")) {

                Random random = new Random();
                int rand1 = Math.abs(random.nextInt());
                int rand2 = Math.abs(random.nextInt());
                DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
                Date dateFileToday = new Date();
                String filePrefixToday = formatterFileToday.format(dateFileToday);

                String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

                logger.info("postContentRequest API filePath :" + filePath);
                logger.info("postContentRequest API fileName1 :" + fileName1);

                // contentPostPictureLink = GlobalVariable.baseUrl + "/upload/" + fileName1;
                contentPostPictureLink = GlobalVariable.baseUrlImg + "/upload/postcontent/" + fileName1;

                /*
                * Converting a Base64 String into Image byte array 
                 */
                // byte[] imageByteArray = Base64.decodeBase64(imageBase64String);
                byte[] imageByteArray = Base64.getDecoder().decode(imageBase64String);

                /*
                  * Write a image byte array into file system  
                 */
                //  FileOutputStream imageOutFile = new FileOutputStream("/Users/jeeva/Pictures/wallpapers/water-drop-after-convert.jpg");
                FileOutputStream imageOutFile = new FileOutputStream(filePath + fileName1);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();

                postContentPicture = fileName1;

            }

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memMemberName = member.getMemberName().toString();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        String idS = getId.getID(60);
                        int postContentRequestId = Integer.parseInt(idS);

                        InetAddress ip;
                        String hostname = "";
                        String ipAddress = "";
                        try {
                            ip = InetAddress.getLocalHost();
                            hostname = ip.getHostName();
                            ipAddress = ip.getHostAddress();

                        } catch (UnknownHostException e) {

                            e.printStackTrace();
                        }

                        String adduser = Integer.toString(memId);
                        adddate = dateFormatX.format(dateToday);
                        String customOrderby = "0";
                        String published = "1";

                        postContentDate = dateFormatX.format(dateToday);

                        /*
                        //insert post_content_req
                        postContentRequestAddSQL = dbsession.createSQLQuery("INSERT INTO post_content_req("
                                + "id_post_content,"
                                + "content_category,"
                                + "post_content_type,"
                                + "post_content_title,"
                                + "content_short_desc,"
                                + "content_desc,"
                                + "post_link,"
                                + "content_date,"
                                + "picture,"
                                + "custom_orderby,"
                                + "published,"
                                + "add_date,"
                                + "add_ip,"
                                + "add_term,"
                                + "add_user"
                                + ") VALUES("
                                + "'" + postContentRequestId + "',"
                                + "'" + postContentCategory + "',"
                                + "'" + postContentType + "',"
                                + "'" + postContentTitle + "',"
                                + "'" + postContentShortDesc + "',"
                                + "'" + postContentDesc + "',"
                                + "'" + postContentPostLink + "',"
                                + "'" + postContentDate + "',"
                                + "'" + postContentPicture + "',"
                                + "'" + customOrderby + "',"
                                + "'" + published + "',"
                                + "'" + adddate + "',"
                                + "'" + ipAddress + "',"
                                + "'" + hostname + "',"
                                + "'" + adduser + "'"
                                + "  ) ");

                        logger.info("postContentRequestAddSQL ::" + postContentRequestAddSQL);

                        postContentRequestAddSQL.executeUpdate();
                         */
 /*
                        //insert post_content_req
                        postContentRequestAddSQL = dbsession.createSQLQuery("INSERT INTO post_content("
                                + "id_post_content,"
                                + "content_category,"
                                + "post_content_type,"
                                + "post_content_title,"
                                + "content_short_desc,"
                                + "content_desc,"
                                + "post_link,"
                                + "content_date,"
                                + "picture,"
                                + "content_owner,"
                                + "custom_orderby,"
                                + "published,"
                                + "add_date,"
                                + "add_ip,"
                                + "add_term,"
                                + "add_user"
                                + ") VALUES("
                                + "'" + postContentRequestId + "',"
                                + "'" + postContentCategory + "',"
                                + "'" + postContentType + "',"
                                + "'" + postContentTitle + "',"
                                + "'" + postContentShortDesc + "',"
                                + "'" + postContentDesc + "',"
                                + "'" + postContentPostLink + "',"
                                + "'" + postContentDate + "',"
                                + "'" + postContentPicture + "',"
                                + "'" + adduser + "',"
                                + "'" + customOrderby + "',"
                                + "'" + published + "',"
                                + "'" + adddate + "',"
                                + "'" + ipAddress + "',"
                                + "'" + hostname + "',"
                                + "'" + adduser + "'"
                                + "  ) ");

                        logger.info("postContentRequestAddSQL ::" + postContentRequestAddSQL);

                        postContentRequestAddSQL.executeUpdate();
                         */
                        //prepare Statement
                        PreparedStatement ps = con.prepareStatement("INSERT INTO post_content("
                                + "id_post_content,"
                                + "content_category,"
                                + "post_content_type,"
                                + "post_content_title,"
                                + "content_short_desc,"
                                + "content_desc,"
                                + "post_link,"
                                + "content_date,"
                                + "picture,"
                                + "content_owner,"
                                + "custom_orderby,"
                                + "published,"
                                + "add_date,"
                                + "add_ip,"
                                + "add_term,"
                                + "add_user,"
                                + "disease_category_id ) "
                                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

                        //  ps.setInt(1, Integer.parseInt(postContentRequestId));
                        ps.setInt(1, postContentRequestId);
                        ps.setString(2, postContentCategory);
                        ps.setString(3, postContentType);
                        //  ps.setDouble(3, Double.parseDouble(productCartonQtySft));
                        ps.setString(4, postContentTitle);
                        ps.setString(5, postContentShortDesc);
                        //  ps.setDouble(5, Double.parseDouble(productCartonQtyPcs));
                        ps.setString(6, postContentDesc);
                        ps.setString(7, postContentPostLink);
                        ps.setString(8, postContentDate);
                        ps.setString(9, postContentPicture);
                        ps.setString(10, adduser);
                        ps.setString(11, customOrderby);
                        ps.setString(12, published);

                        ps.setString(13, adddate);
                        ps.setString(14, ipAddress);
                        ps.setString(15, hostname);
                        ps.setString(16, adduser);
                        ps.setString(17, PostdiseaseCategoryId);

                        logger.info("SQL Query: " + ps.toString());

                        ps.executeUpdate();
                        ps.close();

                        //insert post_content_rating_point
                        contentRatingAddSQL = dbsession.createSQLQuery("INSERT INTO post_content_rating_point("
                                + "id,"
                                + "content_id,"
                                + "like_content_point,"
                                + "comment_content_point,"
                                + "share_content_point,"
                                + "update_time"
                                + ") VALUES("
                                + "'" + postContentRequestId + "',"
                                + "'" + postContentRequestId + "',"
                                + "'0',"
                                + "'0',"
                                + "'0',"
                                + "'" + adddate + "'"
                                + "  ) ");

                        logger.info("contentRatingAddSQL ::" + contentRatingAddSQL);

                        contentRatingAddSQL.executeUpdate();

                        con.commit();
                        con.close();

                        //send firebase notifications
                        // $notification = array();
                        //  $notification['contentId'] = $post_content_id;
                        //   $notification['contentType'] = $contentType;
                        //   $notification['contentPictureLinkOpt'] = $contentPictureLinkOpt;
                        //   $notification['contentVideoLinkOpt'] = $contentVideoLinkOpt;
                        //   $notification['contentTitle'] = $contentTitle;
                        //    $notification['contentShortDetails'] = $contentShortDetails;
                        //   $notification['contentDetails'] = $contentDetails;
                        //   $notification['postLink'] = $contentPostLink;
                        //   String msgTitle = "Title  Firebase General Topics message by Tahajjat";
                        //   String msgBody = "Body Firebase General Topics message by Tahajjat";
                        String msgTitle = "";
                        if (postContentType.equals("PC")) {
                            msgTitle = memMemberName + " has uploaded a picture.";
                        } else if (postContentType.equals("VI")) {
                            msgTitle = memMemberName + " has posted a video link.";
                        } else {
                            msgTitle = memMemberName + " has added a new post.";
                        }

                        //   String msgBody = postContentShortDesc;
                        String msgBody = "";
                        String topicsName = "General"; //
                        String msgPriority = "high"; // normal
                        String fcmsend1 = fcmsend.sendTopicsWiseNotification(msgTitle, msgBody, topicsName, msgPriority);
                        logger.info("fcmsend1" + fcmsend1);
                        System.out.println("fcmsend1::" + fcmsend1);

                        postContentObj = new JSONObject();
                        postContentObj.put("ContentRequestId", postContentRequestId);
                        postContentObj.put("ContentType", postContentType);
                        postContentObj.put("ContentCategory", postContentCategory);
                        postContentObj.put("ContentTitle", postContentTitle);
                        postContentObj.put("ContentShortDesc", postContentShortDesc);
                        postContentObj.put("ContentDesc", postContentDesc);
                        postContentObj.put("ContentPostLink", postContentPostLink);
                        postContentObj.put("ContentPicture", postContentPicture);
                        postContentObj.put("ContentPictureLink", contentPostPictureLink);

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Post content added successfully");
                        logingObj.put("ResponseData", postContentObj);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
