package com.appul.entity;
// Generated Nov 13, 2020 10:03:04 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Designation generated by hbm2java
 */
public class Designation  implements java.io.Serializable {


     private int id;
     private String designationName;
     private String shortName;
     private int orderby;
     private Set memberDesignations = new HashSet(0);

    public Designation() {
    }

	
    public Designation(int id, String shortName, int orderby) {
        this.id = id;
        this.shortName = shortName;
        this.orderby = orderby;
    }
    public Designation(int id, String designationName, String shortName, int orderby, Set memberDesignations) {
       this.id = id;
       this.designationName = designationName;
       this.shortName = shortName;
       this.orderby = orderby;
       this.memberDesignations = memberDesignations;
    }
   
    public int getId() {
        return this.id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    public String getDesignationName() {
        return this.designationName;
    }
    
    public void setDesignationName(String designationName) {
        this.designationName = designationName;
    }
    public String getShortName() {
        return this.shortName;
    }
    
    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
    public int getOrderby() {
        return this.orderby;
    }
    
    public void setOrderby(int orderby) {
        this.orderby = orderby;
    }
    public Set getMemberDesignations() {
        return this.memberDesignations;
    }
    
    public void setMemberDesignations(Set memberDesignations) {
        this.memberDesignations = memberDesignations;
    }




}


