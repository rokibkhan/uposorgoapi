<%-- 
    Document   : getTimeSlotList
    Created on : OCT 10, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getTimeslotInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query timeslotSQL = null;
    Object timeslotObj[] = null;

    String timeslotId = "";
    String timeslotShortName = "";
    String timeslotName = "";
    String timeslotTimeText = "";
    String timeslotTimeHour = "";
    String timeslotTimeMinute = "";
    String timeslotTimeNumber = "";
    String timeslotDayName = "";
    String timeslotDayNameShort = "";
    String timeslotDayOffName = "";
    String timeslotDayOffNameShort = "";

    JSONArray timeslotResponseObjArr = new JSONArray();
    JSONObject timeslotResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("Uposorgo :: API :: getTimeslotInfo rec :: " + rec);

    JSONObject logingObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();

    if (rec == 1) {

        try {

            timeslotSQL = dbsession.createSQLQuery("SELECT * FROM doctor_timeslot_name  ORDER BY id ASC");
            //timeslotSQL = dbsession.createSQLQuery("SELECT * FROM timeslot  ORDER BY timeslot_long_name ASC");

            if (!timeslotSQL.list().isEmpty()) {
                for (Iterator it = timeslotSQL.list().iterator(); it.hasNext();) {

                    timeslotObj = (Object[]) it.next();
                    timeslotId = timeslotObj[0].toString();
                    System.out.println("timeslotId ::" + timeslotId);
                    timeslotShortName = timeslotObj[1].toString() == null ? "" : timeslotObj[1].toString();
                    System.out.println("timeslotShortName ::" + timeslotShortName);
                    timeslotName = timeslotObj[2].toString() == null ? "" : timeslotObj[2].toString();
                    System.out.println("timeslotName ::" + timeslotName);
                    timeslotTimeText = timeslotObj[3].toString() == null ? "" : timeslotObj[3].toString();
                    System.out.println("timeslotTimeText ::" + timeslotTimeText);
                    timeslotTimeHour = timeslotObj[4].toString() == null ? "" : timeslotObj[4].toString();
                    System.out.println("timeslotTimeHour ::" + timeslotTimeHour);
                    timeslotTimeMinute = timeslotObj[5].toString() == null ? "" : timeslotObj[5].toString();
                    System.out.println("timeslotTimeMinute ::" + timeslotTimeMinute);
                    timeslotTimeNumber = timeslotObj[6].toString() == null ? "" : timeslotObj[6].toString();
                    System.out.println("timeslotTimeNumber ::" + timeslotTimeNumber);

                    //  timeslotDayName = timeslotObj[7].toString() == null ? "" : timeslotObj[7].toString();
                    //  System.out.println("timeslotDayName ::" + timeslotDayName);
                    //  timeslotDayNameShort = timeslotObj[8].toString() == null ? "" : timeslotObj[8].toString();
                    //  System.out.println("timeslotDayNameShort ::" + timeslotDayNameShort);
                    //   timeslotDayOffName = timeslotObj[9].toString() == null ? "" : timeslotObj[9].toString();
                    //  System.out.println("timeslotDayOffName ::" + timeslotDayOffName);
                    //   timeslotDayOffNameShort = timeslotObj[10].toString() == null ? "" : timeslotObj[10].toString();
                    //    System.out.println("timeslotDayOffNameShort ::" + timeslotDayOffNameShort);
                    timeslotResponseObj = new JSONObject();

                    timeslotResponseObj.put("TimeslotId", timeslotId);
                    timeslotResponseObj.put("TimeslotShortName", timeslotShortName);
                    timeslotResponseObj.put("TimeslotName", timeslotName);
                    timeslotResponseObj.put("TimeText", timeslotTimeText);
                    timeslotResponseObj.put("TimeHour", timeslotTimeHour);
                    timeslotResponseObj.put("TimeMinute", timeslotTimeMinute);
                    timeslotResponseObj.put("TimeNumber", timeslotTimeNumber);
                    //   timeslotResponseObj.put("DayName", timeslotDayName);
                    //   timeslotResponseObj.put("DayNameShort", timeslotDayNameShort);
                    //   timeslotResponseObj.put("DayOffName", timeslotDayOffName);
                    //   timeslotResponseObj.put("DayOffNameShort", timeslotDayOffNameShort);

                    timeslotResponseObjArr.add(timeslotResponseObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", timeslotResponseObjArr);

            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", timeslotResponseObjArr);

            }
        } catch (Exception e) {
            responseDataObj = new JSONObject();
            responseDataObj.put("ResponseCode", "0");
            responseDataObj.put("ResponseText", "Something went wrong!");
            responseDataObj.put("ResponseData", timeslotResponseObjArr);
        }

        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "Key Validation Failed");
        responseDataObj.put("ResponseData", timeslotResponseObjArr);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();


%>