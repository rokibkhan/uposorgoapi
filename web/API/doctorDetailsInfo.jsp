<%-- 
    Document    : doctorDetailsInfo
    Created on  : APR 30, 2020, 1:55:05 PM
    Author      : TAHAJJAT
    
--%>


<%@page import="java.sql.Time"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>

<%

     int Saturday = 0  , Sunday = 1, Monday = 2 , Tuesday =3, Wednesday = 4, Thursday = 5, Friday = 6;
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorDetailsInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String mId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("mId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();
        mId = request.getParameter("mId").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    
    
    // Consumer consumer = null;
    Member member = null;
    MemberOrganization mOrganization = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    String memId = "";

    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    String organizationName = "";
    String organizationLogo = "";
    String organizationLogoLink = "";
    String unitName = "";
    String nirbachoniAson = "";
    String ratingPoint = "";
    String likeCount = "";
    String shareCount = "";
    String uploadCount = "";
    String bloodDonationCount = "";
    
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    //Date dateToday = new Date();

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: doctorDetailsInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    JSONObject addressObj = new JSONObject();
    JSONObject addressResponseObj = new JSONObject();
    JSONArray addressObjArray = new JSONArray();

    JSONObject educationObj = new JSONObject();
    JSONObject educationResponseObj = new JSONObject();
    JSONArray educationObjArray = new JSONArray();

    JSONObject professionObj = new JSONObject();
    JSONObject professionResponseObj = new JSONObject();
    JSONArray professionObjArray = new JSONArray();

    JSONObject trainingObj = new JSONObject();
    JSONObject trainingResponseObj = new JSONObject();
    JSONArray trainingObjArray = new JSONArray();

    JSONObject publicationObj = new JSONObject();
    JSONObject publicationResponseObj = new JSONObject();
    JSONArray publicationObjArray = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMemberCheckSQL = null;
    Object[] qMemberCheckObj = null;

    Query memberKeySQL = null;
    Query memberDoctorTypeSQL = null;
    String memberDoctorType = "";

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    Query mMemberSQL = null;
    String memberCountSQL = null;
    Query memberSQLQry = null;
    Object[] memberObj = null;

    int memberXId = 0;
    String memberXIEBId = "";
    String memberXName = "";
    String memberXMobile = "";
    String memberXEmail = "";
    String memberXCenter = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String memberXIEBIdFirstChar = "";
    String memberXPictureName = "";
    String memberXPictureLink = "";

    String memberXPhone1View = "";
    String memberXPhone2View = "";
    String memberXMobileView = "";
    String memberXEmailView = "";

    Query mMemberOptionViewSQL = null;
    Object[] mMemberOptionViewObj = null;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    Query memberLifeSQL = null;
    Object[] memberLifeObj = null;
    String memberLifeStatus = "";
    String memberLifeStatusText = "";

    String doctorRegNo = "";
    String doctorDegree0 = "";
    String doctorDegree1 = "";
    String doctorDegree2 = "";
    String doctorDegree3 = "";
    String doctorDegree4 = "";
    String doctorMedicalCollege = "";

    Query mDoctorAdditionalInfoSQL = null;
    Object[] mDoctorAdditionalInfoObj = null;
    
    Query mDoctorFeeSQL = null;
    Object[] mDoctorFeeObj = null;

    Query mDoctorDiscountSQL = null;
    Object[] mDoctorDiscountObj = null;
    
    Query mDoctorInfoSQL = null;
    Object[] mDoctorInfoObj = null;
    
    Query mDoctorAppointmntInfoSQL = null;
    Object[] mDoctorAppointmntInfoObj = null;    

    String doctorDiscountId = "";
    String doctorDiscountType = "";
    String doctorDiscountTypeText = "";
    String doctorDiscountFixedAmount = "";
    String doctorDiscountPercentageAmount = "";

    String doctorDiscountTD = "";
    String doctorDiscountED = "";

    String baseFee = "";
    String discountType = ""; //fixed //percentage
    String discountAmount = "";
    String totalFee = "";
    String totalWithDiscount = "";
    String totalWithoutDiscount = "";
    String doctorBaseFee = "";

    String doctorFeeId = "";
    String doctorFeeAmount = "";
    String doctorFeeTD = "";
    String doctorFeeED = "";
    String aponmntDate = "";
    String aponmntStartTime = "";
    String aponmntEndTime = "";
    String aponmntTime = "";
    
    String doctorWeekStartDay = "";
    String doctorWeekEndDay = "";
    String doctorWeekOffDay = "";
    String doctorDailyStartTime = "";
    String doctorDailyEndTime = "";
    String doctorAvailableStatus = "";
    String doctorUpcomingAvailableTime = "";
    String doctorUpcomingAvailableDate = "";
    String doctorUpcomingAvailableDateFormatShow = "";
    String doctorAvailableFlag = "";
    String doctorWeekDay = "";
    String doctorDailyTime = "";
    String doctorCategoryID = "";
    String doctorCategoryName = "";
    String doctorDiscountFeePercentage = "";
    Double doctorActualFee = 0.0d;
    Double doctorDiscontPercentageFee = 0.0d;
    DateFormat displayFormat_H_AM_PM = new SimpleDateFormat("hh aa");
    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");  
    Date dateToday = new Date();


    if (rec == 1) {

        try {

            // qMemberCheckSQL = dbsession.createSQLQuery(" SELECT * FROM member WHERE member_id = '" + memberId + "'");
            qMemberCheckSQL = dbsession.createSQLQuery(" SELECT * FROM member WHERE mobile = '" + memberId1 + "'");

            if (!qMemberCheckSQL.list().isEmpty()) {
                for (Iterator qMemberCheckItr = qMemberCheckSQL.list().iterator(); qMemberCheckItr.hasNext();) {

                    qMemberCheckObj = (Object[]) qMemberCheckItr.next();
                    memId = qMemberCheckObj[0].toString();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential WHERE member_id='" + memId + "'");
                    dbPass = memberKeySQL.uniqueResult().toString();

                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        memberDoctorTypeSQL = dbsession.createSQLQuery("SELECT member_type_id FROM member_type WHERE member_id='" + mId + "'");
                        memberDoctorType = memberDoctorTypeSQL.uniqueResult().toString();

                        if (memberDoctorType.equalsIgnoreCase("2")) {

                            mMemberSQL = dbsession.createQuery(" from Member WHERE id = '" + mId + "'");

                            for (Iterator memberItr = mMemberSQL.list().iterator(); memberItr.hasNext();) {
                                member = (Member) memberItr.next();
                                memberXId = member.getId();

                                memberXIEBId = member.getMemberId();
                                memberXName = member.getMemberName();

                                //instituteName = memberEducation.getInstituteName() == null ? "" : memberEducation.getInstituteName();
                                fatherName = member.getFatherName() == null ? "" : member.getFatherName();
                                motherName = member.getMotherName() == null ? "" : member.getMotherName();

                                dob = member.getDob();
                                gender = member.getGender() == null ? "" : member.getGender().trim();

                                mobileNo = member.getMobile() == null ? "" : member.getMobile();
                                phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                                phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                                memberEmail = member.getEmailId() == null ? "" : member.getEmailId();
                                bloodGroup = member.getBloodGroup() == null ? "" : member.getBloodGroup();

                                memberXName = member.getMemberName();

                                unitName = member.getUnitName() == null ? "" : member.getUnitName();
                                nirbachoniAson = member.getNirbachoniAson() == null ? "" : member.getNirbachoniAson();

                                Query mOrganizationSQL = dbsession.createQuery(" from MemberOrganization WHERE member_id='" + memId + "'");

                                logger.info("API :: Login API memberOrganizationSQL ::" + mOrganizationSQL);

                                if (!mOrganizationSQL.list().isEmpty()) {
                                    for (Iterator mOrganizationItr = mOrganizationSQL.list().iterator(); mOrganizationItr.hasNext();) {
                                        //   addressBook = (AddressBook) mAddressItr.next();

                                        mOrganization = (MemberOrganization) mOrganizationItr.next();

                                        organizationName = mOrganization.getOrganizationInfo().getOrganizationName();
                                        organizationLogo = mOrganization.getOrganizationInfo().getOrgLogo();
                                        organizationLogoLink = GlobalVariable.imageDirLink + "company/" + organizationLogo;
                                    }
                                }

                                memberXPictureName = member.getPictureName();

                                memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;

                                mDoctorAdditionalInfoSQL = dbsession.createSQLQuery("SELECT "
                                        + "ai.addi_info_0 as mDegree0,ai.addi_info_1 as mDegree1,ai.addi_info_2 as mDegree2 ,ai.addi_info_3 as mDegree3, "
                                        + "ai.addi_info_4 as mDegree4,ai.addi_info_6 as mRegNo,ai.addi_info_7 as medicalCollege  "
                                        + "FROM member_additional_info ai WHERE ai.member_id='" + mId + "'");

                                for (Iterator mDoctorAdditionalInfoItr = mDoctorAdditionalInfoSQL.list().iterator(); mDoctorAdditionalInfoItr.hasNext();) {

                                    mDoctorAdditionalInfoObj = (Object[]) mDoctorAdditionalInfoItr.next();

                                    doctorDegree0 = mDoctorAdditionalInfoObj[0] == null ? "" : mDoctorAdditionalInfoObj[0].toString();
                                    doctorDegree1 = mDoctorAdditionalInfoObj[1] == null ? "" : mDoctorAdditionalInfoObj[1].toString();
                                    doctorDegree2 = mDoctorAdditionalInfoObj[2] == null ? "" : mDoctorAdditionalInfoObj[2].toString();
                                    doctorDegree3 = mDoctorAdditionalInfoObj[3] == null ? "" : mDoctorAdditionalInfoObj[3].toString();
                                    doctorDegree4 = mDoctorAdditionalInfoObj[4] == null ? "" : mDoctorAdditionalInfoObj[4].toString();

                                    doctorRegNo = mDoctorAdditionalInfoObj[5] == null ? "" : mDoctorAdditionalInfoObj[5].toString();
                                    doctorMedicalCollege = mDoctorAdditionalInfoObj[6] == null ? "" : mDoctorAdditionalInfoObj[6].toString();

                                }

//                            memberXPhone1View = "1";
//                            memberXPhone2View = "1";
//                            memberXMobileView = "1";
//                            memberXMobileView = "1";
                                mMemberOptionViewSQL = dbsession.createSQLQuery("SELECT phone1_view,phone2_view,mobile_view,email_view FROM member_view_setting WHERE member_id='" + mId + "'");

                                for (Iterator mMemberOptionViewItr = mMemberOptionViewSQL.list().iterator(); mMemberOptionViewItr.hasNext();) {

                                    mMemberOptionViewObj = (Object[]) mMemberOptionViewItr.next();
                                    memberXPhone1View = mMemberOptionViewObj[0].toString();
                                    memberXPhone2View = mMemberOptionViewObj[1].toString();
                                    memberXMobileView = mMemberOptionViewObj[2].toString();
                                    memberXMobileView = mMemberOptionViewObj[3].toString();

                                }

                                mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + mId + "' AND SYSDATE() between mt.ed and mt.td");

                                for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

                                    mMemberTypeObj = (Object[]) mMemberTypeItr.next();
                                    mMemberTypeId = mMemberTypeObj[0].toString();
                                    mMemberTypeName = mMemberTypeObj[1].toString();

                                }

                                //                        Query mAddressSQL = dbsession.createSQLQuery("select *from address_book WHERE id=(select address_id from member_address where member_id= " + memId + " and address_Type='M' ) ");
//                        for (Iterator itr2 = mAddressSQL.list().iterator(); itr2.hasNext();) {
//                            mAddressObj = (Object[]) itr2.next();
//                            mAddressLine1 = mAddressObj[1].toString();
//                            mAddressLine2 = mAddressObj[2].toString();
//                            mThanaId = Integer.parseInt(mAddressObj[3].toString());
//
//                        }
                                //   Query mAddressSQL = dbsession.createSQLQuery(" from AddressBook WHERE id=(select address_id from member_address where member_id= " + memId + " and address_Type='M' ) ");
                                Query memberAddressSQL = dbsession.createQuery(" from MemberAddress WHERE member_id='" + mId + "'");

                                logger.info("API ::  memberDetailsInfo API memberAddressSQL ::" + memberAddressSQL);

                                if (!memberAddressSQL.list().isEmpty()) {
                                    for (Iterator memberAddressItr = memberAddressSQL.list().iterator(); memberAddressItr.hasNext();) {
                                        //   addressBook = (AddressBook) mAddressItr.next();

                                        memberAddress = (MemberAddress) memberAddressItr.next();

                                        memberAddressId = memberAddress.getId();
                                        memberAddressType = memberAddress.getAddressType();
                                        logger.info("API ::  memberDetailsInfo API memberAddressType ::" + memberAddressType);

                                        if (memberAddressType.equals("M")) {
                                            mAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                                            mAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                                            mZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                                            //   mThanaId = memberAddress.getAddressBook().getThana().getId();
                                            //   mThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                                            //   mDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                                            //   mDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
//                                    mThanaId1 =   memberAddress.getAddressBook().getThana();
                                            //  mThanaId = memberAddress.getAddressBook().getThana().getId();
                                            //    mThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                                            //   mDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                                            //    mDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                                        }

                                        if (memberAddressType.equals("P")) {
                                            pAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                                            pAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                                            pZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                                            //   pThanaId = memberAddress.getAddressBook().getThana().getId();
                                            //   pThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                                            //   pDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                                            //   pDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                                        }

                                    }
                                }

//                        Query pAddressSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address where member_id= " + memId + " and address_Type='P' ) ");
//                        for (Iterator itr3 = pAddressSQL.list().iterator(); itr3.hasNext();) {
//
//                            pAddressObj = (Object[]) itr3.next();
//                            pAddressLine1 = pAddressObj[1].toString();
//                            pAddressLine2 = pAddressObj[2].toString();
//                            pThanaId = Integer.parseInt(pAddressObj[3].toString());
//
//                        }






                                addressResponseObj = new JSONObject();

                                addressResponseObj.put("mAddressLine1", mAddressLine1);
                                addressResponseObj.put("mAddressLine2", mAddressLine2);
                                addressResponseObj.put("mThanaId", mThanaId);
                                addressResponseObj.put("mThanaName", mThanaName);
                                addressResponseObj.put("mDistrictId", mDistrictId);
                                addressResponseObj.put("mDistrictName", mDistrictName);
                                addressResponseObj.put("mCountry", mCountry);
                                addressResponseObj.put("mZipCode", mZipCode);

                                addressResponseObj.put("pAddressLine1", pAddressLine1);
                                addressResponseObj.put("pAddressLine2", pAddressLine2);
                                addressResponseObj.put("pThanaId", pThanaId);
                                addressResponseObj.put("pThanaName", pThanaName);
                                addressResponseObj.put("pDistrictId", pDistrictId);
                                addressResponseObj.put("pDistrictName", pDistrictName);
                                addressResponseObj.put("pCountry", pCountry);
                                addressResponseObj.put("pZipCode", pZipCode);

                                addressObjArray.add(addressResponseObj);

                                /*
                            //   MemberEducationInfo memberEducation
                            MemberEducationInfo memberEducation = null;
                            int memberEducationId = 0;
                            int degreeId = 0;
                            String degreeName = "";
                            String instituteName = "";
                            String boardUniversityName = "";
                            String yearOfPassing = "";
                            String resultTypeName = "";
                            String result = "";

                            Object[] subjectObject = null;
                            String subjectUniversityName = "";
                            String subjectName = "";

                            Query memberEducationSQL = dbsession.createQuery(" from MemberEducationInfo WHERE member_id='" + mId + "'");

                            logger.info("API ::  memberDetailsInfo API memberEducationSQL ::" + memberEducationSQL);

                            if (!memberEducationSQL.list().isEmpty()) {
                                for (Iterator memberEducationItr = memberEducationSQL.list().iterator(); memberEducationItr.hasNext();) {

                                    memberEducation = (MemberEducationInfo) memberEducationItr.next();
                                    memberEducationId = memberEducation.getId();

                                    degreeId = memberEducation.getDegree().getDegreeId();
                                    degreeName = memberEducation.getDegree().getDegreeName();

                                    instituteName = memberEducation.getInstituteName() == null ? "" : memberEducation.getInstituteName();

                                    logger.info("API ::  memberDetailsInfo API instituteName ::" + instituteName);

                                    boardUniversityName = memberEducation.getUniversity().getUniversityLongName();
                                    yearOfPassing = memberEducation.getYearOfPassing();
                                    resultTypeName = memberEducation.getResultType().getResultTypeName();
                                    result = memberEducation.getResult();

                                    Query subjectSQL = dbsession.createSQLQuery("SELECT usb.subject_long_name,u.university_long_name FROM "
                                            + "member_subject_info msb,university_subject usb,university u "
                                            + "WHERE msb.subject_id = usb.subject_id "
                                            + "AND usb.university_id = u.university_id "
                                            + "AND msb.member_id='" + mId + "'");

                                    if (!subjectSQL.list().isEmpty()) {

                                        for (Iterator subjectItr = subjectSQL.list().iterator(); subjectItr.hasNext();) {

                                            subjectObject = (Object[]) subjectItr.next();
                                            subjectName = subjectObject[0].toString();
                                            subjectUniversityName = subjectObject[1].toString();
                                        }

                                    }

                                    educationResponseObj = new JSONObject();
                                    educationResponseObj.put("EducationId", memberEducationId);
                                    educationResponseObj.put("DegreeId", degreeId);
                                    educationResponseObj.put("DegreeName", degreeName);
                                    educationResponseObj.put("InstituteName", instituteName);
                                    educationResponseObj.put("BoardUniversityName", boardUniversityName);
                                    educationResponseObj.put("YearOfPassing", yearOfPassing);
                                    educationResponseObj.put("ResultTypeName", resultTypeName);
                                    educationResponseObj.put("Result", result);
                                    educationResponseObj.put("SubjectName", subjectName);

                                    educationObjArray.add(educationResponseObj);

                                }
                            }

                            //Profession Object
                            MemberProfessionalInfo memberProfession = null;
                            int memberProfessionId = 0;
                            String organizationName = "";
                            String organizationType = "";
                            String organizationAddress = "";
                            String organizationDesignationName = "";
                            String organizationStartDate = "";
                            String organizationTillDate = "";
                            Query memberProfessionSQL = dbsession.createQuery("from MemberProfessionalInfo WHERE  member_id='" + mId + "' ");

                            logger.info("API ::  memberDetailsInfo API memberProfessionSQL ::" + memberProfessionSQL);

                            for (Iterator memberProfessionItr = memberProfessionSQL.list().iterator(); memberProfessionItr.hasNext();) {
                                memberProfession = (MemberProfessionalInfo) memberProfessionItr.next();
                                memberProfessionId = memberProfession.getId();
                                organizationName = memberProfession.getCompanyName() == null ? "" : memberProfession.getCompanyName();

                                organizationType = memberProfession.getCompanyType() == null ? "" : memberProfession.getCompanyType();
                                organizationDesignationName = memberProfession.getDesignation() == null ? "" : memberProfession.getDesignation();
                                organizationStartDate = memberProfession.getFromDate().toString() == null ? "" : memberProfession.getFromDate().toString();
                                organizationTillDate = memberProfession.getTillDate().toString() == null ? "" : memberProfession.getTillDate().toString();
                                organizationAddress = memberProfession.getCompanyAddress() == null ? "" : memberProfession.getCompanyAddress();

                                professionResponseObj = new JSONObject();
                                professionResponseObj.put("ProfessionId", memberProfessionId);
                                professionResponseObj.put("OrganizationName", organizationName);
                                professionResponseObj.put("OrganizationType", organizationType);
                                professionResponseObj.put("DesignationName", organizationDesignationName);
                                professionResponseObj.put("StartDate", organizationStartDate);
                                professionResponseObj.put("EndDate", organizationTillDate);
                                professionResponseObj.put("OrganizationAddress", organizationAddress);

                                professionObjArray.add(professionResponseObj);

                            }

                            //Training
                            MemberTrainingInfo memberTraining = null;
                            int trainingId = 0;
                            String trainingInstituteName = "";
                            String courseTitle = "";
                            String trainingTitle = "";
                            String trainingYearOfPassing = "";
                            String weeks = "";

                            Query memberTrainingSQL = dbsession.createQuery("from MemberTrainingInfo where  member_id=" + mId + " ");
                            logger.info("API ::  memberDetailsInfo API memberTrainingSQL ::" + memberTrainingSQL);
                            for (Iterator memberTrainingItr = memberTrainingSQL.list().iterator(); memberTrainingItr.hasNext();) {
                                memberTraining = (MemberTrainingInfo) memberTrainingItr.next();

                                trainingId = memberTraining.getId();
                                trainingInstituteName = memberTraining.getMemberTrainingInstitute() == null ? "" : memberTraining.getMemberTrainingInstitute();
                                trainingTitle = memberTraining.getMemberTrainingTitle() == null ? "" : memberTraining.getMemberTrainingTitle();
                                courseTitle = memberTraining.getMemberTrainingCourse() == null ? "" : memberTraining.getMemberTrainingCourse();
                                trainingYearOfPassing = memberTraining.getMemberTrainingYear() == null ? "" : memberTraining.getMemberTrainingYear();
                                weeks = memberTraining.getMemberTrainingDuration() == null ? "" : memberTraining.getMemberTrainingDuration();

                                trainingResponseObj = new JSONObject();
                                trainingResponseObj.put("TrainingId", trainingId);
                                trainingResponseObj.put("TrainingInstituteName", trainingInstituteName);
                                trainingResponseObj.put("TrainingTitle", trainingTitle);
                                trainingResponseObj.put("CourseTitle", courseTitle);
                                trainingResponseObj.put("TrainingYearOfPassing", trainingYearOfPassing);
                                trainingResponseObj.put("weeks", weeks);

                                trainingObjArray.add(trainingResponseObj);
                            }

                            MemberPublicationInfo memberPublication = null;
                            int publicationId = 0;
                            String publicationTitle = "";
                            String publicationAuthor = "";
                            String publicationYear = "";
                            String publicationJournal = "";

                            Query memberPublicationSQL = dbsession.createQuery("from MemberPublicationInfo where  member_id='" + mId + "' ");
                            logger.info("API ::  memberDetailsInfo API memberPublicationSQL ::" + memberPublicationSQL);
                            for (Iterator memberPublicationItr = memberPublicationSQL.list().iterator(); memberPublicationItr.hasNext();) {
                                memberPublication = (MemberPublicationInfo) memberPublicationItr.next();

                                publicationId = memberPublication.getId();
                                publicationTitle = memberPublication.getPublicationTitle() == null ? "" : memberPublication.getPublicationTitle();
                                publicationYear = memberPublication.getPublicationYear() == null ? "" : memberPublication.getPublicationYear();
                                publicationJournal = memberPublication.getPublicationJournalConference() == null ? "" : memberPublication.getPublicationJournalConference();
                                publicationAuthor = memberPublication.getPublicationAuthor() == null ? "" : memberPublication.getPublicationAuthor().toString();

                                publicationResponseObj = new JSONObject();
                                publicationResponseObj.put("PublicationId", publicationId);
                                publicationResponseObj.put("PublicationTitle", publicationTitle);
                                publicationResponseObj.put("PublicationYear", publicationYear);
                                publicationResponseObj.put("PublicationJournal", publicationJournal);
                                publicationResponseObj.put("PublicationAuthor", publicationAuthor);

                                publicationObjArray.add(publicationResponseObj);
                            }
                            
                                 */
                                responseObj = new JSONObject();
                                
                                // doctor info start
                                
                                //doctor info end
                                
                                
                                mDoctorInfoSQL = dbsession.createSQLQuery("SELECT di.id, di.doctor_id, di.week_start_day, di.week_end_day, "
                                        + "di.daily_start_time, di.daily_end_time, dcn.name, dc.doctor_category_id,di.week_off_day  FROM doctor_info di "
                                        + " left join doctor_category dc on dc.member_id = di.doctor_id  "
                                        + "  join doctor_category_name dcn on dc.doctor_category_id = dcn.id  "
                                        + " WHERE  di.status = '1' and di.doctor_id='" + memberXId + "'");
                                if (!mDoctorInfoSQL.list().isEmpty()) {
                                    for (Iterator mDoctorInfoItr = mDoctorInfoSQL.list().iterator(); mDoctorInfoItr.hasNext();) {

                                        mDoctorInfoObj = (Object[]) mDoctorInfoItr.next();
                                        doctorWeekStartDay = mDoctorInfoObj[2].toString();
                                        doctorWeekEndDay = mDoctorInfoObj[3].toString();
                                        doctorDailyStartTime = mDoctorInfoObj[4].toString();
                                        doctorDailyEndTime = mDoctorInfoObj[5].toString();
                                        doctorCategoryName =  mDoctorInfoObj[6].toString();
                                        doctorCategoryID = mDoctorInfoObj[7].toString();
                                        doctorWeekOffDay = mDoctorInfoObj[8].toString();
                                        
                                        System.out.println("doctorWeekOffDay:"+doctorWeekOffDay);
                                        /* String str11 = "14:20";
                                         SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm aa");
                                         Date date31 = dateFormat.parse(str11);
                                         String dateString = dateFormat.format(str11).toString();

                                         System.out.println("dateString:"+dateString);*/
                                                
                                        /*
                                        //String dateString3 = "22:30"; 
                                        //old format
                                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

                                        Date date3 = sdf.parse(doctorDailyStartTime);
                                        //new format
                                        SimpleDateFormat sdf2 = new SimpleDateFormat("hh:mm aa");
                                        //formatting the given time to new format with AM/PM
                                        System.out.println("Given time in AM/PM: "+sdf2.format(date3));
                                        
                                        Date date4 = sdf.parse(doctorDailyEndTime);
                                        
                                        System.out.println("Given time in AM/PM: "+sdf2.format(date4));
                                                
                                        //doctorDailyTime = dateFormat.format(displayFormat_H_AM_PM.parse(doctorDailyStartTime).toString());
                                        doctorDailyTime = sdf2.format(date3).toString().replace(":00", "") + " to " + sdf2.format(date4).toString().replace(":00", "");
                                        doctorWeekDay = doctorWeekStartDay + " - " + doctorWeekEndDay;
                                        
                                        System.out.println("doctorDailyStartTime:"+doctorDailyStartTime);
                                        System.out.println("doctorDailyEndTime"+doctorDailyEndTime);
                                        
                                        Date time1 = new SimpleDateFormat("HH:mm:ss").parse(doctorDailyEndTime);
                                        Calendar calendar1 = Calendar.getInstance();
                                        calendar1.setTime(time1);
                                        calendar1.add(Calendar.DATE, 1);


                                        
                                        Date time2 = new SimpleDateFormat("HH:mm:ss").parse(doctorDailyStartTime);
                                        Calendar calendar2 = Calendar.getInstance();
                                        calendar2.setTime(time2);
                                        calendar2.add(Calendar.DATE, 1);

                                        String someRandomTime = new SimpleDateFormat("HH:mm:ss").format(java.util.Calendar.getInstance().getTime());
                                        //String today = dateToday.toString();
                                        System.out.println("today:"+someRandomTime);
                                        
                                        Date d = new SimpleDateFormat("HH:mm:ss").parse(someRandomTime);
                                        Calendar calendar3 = Calendar.getInstance();
                                        calendar3.setTime(d);
                                        calendar3.add(Calendar.DATE, 1);

                                        Calendar calendar = Calendar.getInstance();
                                        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                                        

                                        
                                        Date x = calendar3.getTime();
                                        int startday = 0,endday = 0,offday = 0,upcomingday = 0;
                                        
                                        
                                        
                                        if (doctorWeekOffDay.equals("Sunday"))
                                        {
                                            offday = 1;
                                        }
                                        else if (doctorWeekOffDay.equals("Monday"))
                                        {
                                            offday = 2;
                                        }
                                        else if (doctorWeekOffDay.equals("Tuesday")) 
                                        {
                                            offday = 3;
                                        }
                                        else if (doctorWeekOffDay.equals("Wednesday"))
                                        {
                                            offday = 4;
                                        }
                                        else if (doctorWeekOffDay.equals("Thursday")) 
                                        {
                                            offday = 5;
                                        }
                                        else if (doctorWeekOffDay.equals("Friday")) 
                                        {
                                            offday = 6;
                                        }
                                        else if (doctorWeekOffDay.equals("Saturday")) 
                                        {
                                            offday = 7;
                                        }
                                        
                                        System.out.println("dayOfWeek:"+dayOfWeek);
                                        System.out.println("offday"+offday);
                                        if(dayOfWeek != offday )
                                        {
                                            if (x.before(calendar1.getTime()) && x.after(calendar2.getTime())) {
                                            //checkes whether the current time is between 14:49:00 and 20:11:13.
                                            System.out.println("Available");
                                            doctorAvailableStatus = "Available";
                                            doctorAvailableFlag = "1";
                                            }
                                            else
                                            {
                                                doctorAvailableStatus = "Unavailable";
                                                doctorAvailableFlag = "0";
                                                
                                                if (x.before(calendar1.getTime()))
                                                {
                                                    System.out.println("before");
                                                    upcomingday = dayOfWeek;
                                                    
                                                }
                                                else if(x.after(calendar2.getTime()))
                                                {
                                                    System.out.println("after");
                                                    upcomingday = dayOfWeek + 1;
                                                    if(upcomingday > 7)
                                                    upcomingday = 1;
                                                                                                                                                
                                                }
                                                
                                                switch (upcomingday) {
                                                case 1:
                                                    System.out.print("Sunday");
                                                    doctorUpcomingAvailableDate = "Sunday";
                                                    break;
                                                case 2:
                                                    System.out.print("Monday");
                                                    doctorUpcomingAvailableDate = "Monday";
                                                    break;
                                                case 3:
                                                    System.out.print("Tuesday");
                                                    doctorUpcomingAvailableDate = "Tuesday";
                                                    break;
                                                case 4:
                                                    System.out.print("Wednesday");
                                                    doctorUpcomingAvailableDate = "Wednesday";
                                                    break;
                                                case 5:
                                                    System.out.print("Thursday");
                                                    doctorUpcomingAvailableDate = "Thursday";
                                                    break;
                                                case 6:
                                                    System.out.print("Friday");
                                                    doctorUpcomingAvailableDate = "Friday";
                                                    break;
                                                case 7:
                                                    System.out.print("Saturday");
                                                    doctorUpcomingAvailableDate = "Saturday";
                                                }
                                                calendar.add(Calendar.DATE, 1);
                                                Date modifiedDate = calendar.getTime();
                                                String strDate = formatter.format(modifiedDate);  
                                                doctorUpcomingAvailableTime = doctorDailyTime;
                                                doctorUpcomingAvailableDate += ","+strDate;

                                                    
                                                
                                            }

                                            
                                        }
                                        else
                                        {
                                            doctorAvailableStatus = "Unavailable";
                                            doctorAvailableFlag = "0";
                                            System.out.println("dayOfWeek:"+dayOfWeek);
                                            upcomingday = dayOfWeek + 1;
                                            System.out.println("upcomingday"+upcomingday);
                                            if(upcomingday > 7)
                                                upcomingday = 1;
                                            
                                            switch (upcomingday) {
                                            case 1:
                                                System.out.print("Sunday");
                                                doctorUpcomingAvailableDate = "Sunday";
                                                break;
                                            case 2:
                                                System.out.print("Monday");
                                                doctorUpcomingAvailableDate = "Monday";
                                                break;
                                            case 3:
                                                System.out.print("Tuesday");
                                                doctorUpcomingAvailableDate = "Tuesday";
                                                break;
                                            case 4:
                                                System.out.print("Wednesday");
                                                doctorUpcomingAvailableDate = "Wednesday";
                                                break;
                                            case 5:
                                                System.out.print("Thursday");
                                                doctorUpcomingAvailableDate = "Thursday";
                                                break;
                                            case 6:
                                                System.out.print("Friday");
                                                doctorUpcomingAvailableDate = "Friday";
                                                break;
                                            case 7:
                                                System.out.print("Saturday");
                                                doctorUpcomingAvailableDate = "Saturday";
                                        }
                                            
                                            calendar.add(Calendar.DATE, 1);
                                            Date modifiedDate = calendar.getTime();                                            
                                            String strDate = formatter.format(modifiedDate);  
                                            System.out.println("");
                                            doctorUpcomingAvailableTime = doctorDailyTime;
                                            doctorUpcomingAvailableDate += ","+strDate;
                                            
                                        }*/
                                        
                                        
                                        
                                        
                                        
                                    }
                                }
                                
                                String[] days = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
                                String today = dateFormatYmd.format(dateToday);
                                String[] starttimesplit;
                                String[] endtimesplit;
                                String dayname = "";
                                mDoctorAppointmntInfoSQL = dbsession.createSQLQuery("SELECT id, doctor_id, aponmnt_date, aponmntstart_time, aponmntend_time, short_name "
                                        + "  FROM doctor_aponmnt_info  "                                      
                                        + " WHERE  status = '1' and doctor_id='" + mId + "'"
                                                + " and aponmnt_date >='" + today + "' order by aponmnt_date");
                                if (!mDoctorAppointmntInfoSQL.list().isEmpty()) {
                                    for (Iterator mDoctorAppointmntInfoItr = mDoctorAppointmntInfoSQL.list().iterator(); mDoctorAppointmntInfoItr.hasNext();) {

                                        mDoctorAppointmntInfoObj = (Object[]) mDoctorAppointmntInfoItr.next();
                                        aponmntDate = mDoctorAppointmntInfoObj[2].toString();
                                        aponmntStartTime = mDoctorAppointmntInfoObj[3].toString();
                                        aponmntEndTime = mDoctorAppointmntInfoObj[4].toString();
                                        aponmntTime =  mDoctorAppointmntInfoObj[5].toString();
                                        
                                        System.out.println("aponmntDate:"+aponmntDate);
                                        
                                        starttimesplit = aponmntStartTime.split(" ");
                                        endtimesplit = aponmntEndTime.split(" ");
                                        
                                        Date time1 = new SimpleDateFormat("HH:mm:ss").parse(endtimesplit[1]);
                                        Calendar calendar1 = Calendar.getInstance();
                                        calendar1.setTime(time1);
                                        calendar1.add(Calendar.DATE, 1);


                                        
                                        Date time2 = new SimpleDateFormat("HH:mm:ss").parse(starttimesplit[1]);
                                        Calendar calendar2 = Calendar.getInstance();
                                        calendar2.setTime(time2);
                                        calendar2.add(Calendar.DATE, 1);

                                        
                                        String someRandomTime = new SimpleDateFormat("HH:mm:ss").format(java.util.Calendar.getInstance().getTime());
                                        //String today = dateToday.toString();
                                        System.out.println("today:"+someRandomTime);
                                        
                                        Date d = new SimpleDateFormat("HH:mm:ss").parse(someRandomTime);
                                        Calendar calendar3 = Calendar.getInstance();
                                        calendar3.setTime(d);
                                        calendar3.add(Calendar.DATE, 1);
                                      
                                        Date x = calendar3.getTime();
                                        
                                        Date apndate =new SimpleDateFormat("yyyy-MM-dd").parse(aponmntDate);  
 
                                        doctorAvailableStatus = "Available";
                                        doctorAvailableFlag = "1";
                                                                                
                                        Date tday = new SimpleDateFormat("yyyy-MM-dd").parse(today);  
                                        
                                        
                                        System.out.println("The dateToday 1 is: " + new SimpleDateFormat("yyyy-MM-dd").format(dateToday));
                                        System.out.println("The apndate 2 is: " + new SimpleDateFormat("yyyy-MM-dd").format(apndate));
                                        String dateFormatShow = formatter.format(apndate);
                                        
                                        
                                        Calendar calday = Calendar.getInstance();
                                        calday.setTime(apndate);
                                        System.out.println("today.getDay():"+ calday.get(Calendar.DAY_OF_WEEK));
                                        System.out.println("apndate.getDay():"+apndate.getDay());
                                        
                                        if(tday.compareTo(apndate) == 0) {
                                            
                                           System.out.println("Date equal");
                                           
                                            
                                            if (x.before(calendar1.getTime()) && x.after(calendar2.getTime())) 
                                             {
                                                
                                                System.out.println("MATCH");
                                                doctorUpcomingAvailableDate = aponmntDate;
                                                doctorUpcomingAvailableTime = aponmntTime;
                                                dayname = days[calday.get(Calendar.DAY_OF_WEEK) - 1];
                                                doctorUpcomingAvailableDateFormatShow = dayname + ","+ dateFormatShow;
                                                break;
                                             }
                                            else if(x.compareTo(calendar1.getTime()) < 0)
                                             {
                                                
                                                doctorUpcomingAvailableDate = aponmntDate;
                                                doctorUpcomingAvailableTime = aponmntTime;
                                                dayname = days[calday.get(Calendar.DAY_OF_WEEK) - 1];
                                                doctorUpcomingAvailableDateFormatShow = dayname + ","+ dateFormatShow;
                                                break;
                                             }
                                            else if(x.compareTo(calendar2.getTime()) > 0)
                                             {
                                                
                                             }
                                        }
                                        
                                        else if(tday.before(apndate))
                                        {                                            
                                                doctorUpcomingAvailableDate = aponmntDate;
                                                doctorUpcomingAvailableTime = aponmntTime;
                                                dayname = days[calday.get(Calendar.DAY_OF_WEEK) - 1];
                                                doctorUpcomingAvailableDateFormatShow = dayname + ","+ dateFormatShow;
                                                break;
                                        }
                                        /*else if (dateToday.after(apndate))
                                        {
                                            System.out.println("Today Date After");
                                        }*/
                                        /*if (x.before(calendar1.getTime()) && x.after(calendar2.getTime())) {
                                            System.out.println("MATCH");
                                        }
                                        else
                                        {
                                             System.out.println("NOT MATCH");
                                        }*/
                                        
                                    }
                                    if(doctorUpcomingAvailableDate == "" && doctorUpcomingAvailableTime == "" )
                                    {
                                        doctorAvailableStatus = "Unavailable";
                                        doctorAvailableFlag = "0";
                                        
                                    }
                                }
                                else
                                {
                                    doctorAvailableStatus = "Unavailable";
                                    doctorAvailableFlag = "0";
                                }
                                
                                
                                responseObj.put("doctorAvailableStatus", doctorAvailableStatus);
                                responseObj.put("doctorAvailableFlag", doctorAvailableFlag);
                                responseObj.put("doctorDailyTime", doctorDailyTime);
                                responseObj.put("doctorWeekDay", doctorWeekDay);
                                responseObj.put("doctorCategoryID", doctorCategoryID);
                                responseObj.put("doctorCategoryName", doctorCategoryName);
                                responseObj.put("doctorUpcomingAvailableDate", doctorUpcomingAvailableDate);
                                responseObj.put("doctorUpcomingAvailableTime", doctorUpcomingAvailableTime);
                                responseObj.put("doctorUpcomingAvailableDateFormatShow", doctorUpcomingAvailableDateFormatShow);
                                
                                System.out.println("today:"+today);
                                //doctor fee start
                                mDoctorFeeSQL = dbsession.createSQLQuery("SELECT id_fee,fee_amount,ed,td FROM doctor_fee_info WHERE doctor_id='" + memberXId + "'"
                                        + " and ed <='" 
                                +doctorUpcomingAvailableDate +"' and td >= '" + doctorUpcomingAvailableDate + "'");
                                
                                if (!mDoctorFeeSQL.list().isEmpty()) {
                                    for (Iterator mDoctorFeeItr = mDoctorFeeSQL.list().iterator(); mDoctorFeeItr.hasNext();) {

                                        mDoctorFeeObj = (Object[]) mDoctorFeeItr.next();
                                        doctorFeeId = mDoctorFeeObj[0].toString();
                                        doctorFeeAmount = mDoctorFeeObj[1].toString();
                                        doctorFeeTD = mDoctorFeeObj[2].toString();
                                        doctorFeeED = mDoctorFeeObj[3].toString();

                                    }
                                } else {

                                    doctorFeeAmount = "0.00";
                                    doctorFeeTD = "";
                                    doctorFeeED = "";
                                }
                                // memberContentObj.put("baseFeeId", doctorFeeId);
                                responseObj.put("baseFeeAmount", Math.round(Double.parseDouble(doctorFeeAmount)));
                                //        memberContentObj.put("baseFeeAmount", doctorFeeAmount);

                                responseObj.put("doctorFeeTD", doctorFeeTD);
                                responseObj.put("doctorFeeED", doctorFeeED);

                                //doctor discount
                                mDoctorDiscountSQL = dbsession.createSQLQuery("SELECT ID,DISCOUNT_TYPE,FIXED_AMOUNT,PERCENTAGE_AMOUNT,TD,ED FROM doctor_discount WHERE doctor_id='" + memberXId + "'"
                                + " and ED <='" 
                                +doctorUpcomingAvailableDate +"' and TD >= '" + doctorUpcomingAvailableDate + "'"
                                );
                                if (!mDoctorDiscountSQL.list().isEmpty()) {
                                    for (Iterator mDoctorDiscountItr = mDoctorDiscountSQL.list().iterator(); mDoctorDiscountItr.hasNext();) {

                                        mDoctorDiscountObj = (Object[]) mDoctorDiscountItr.next();
                                        doctorDiscountId = mDoctorDiscountObj[0].toString();
                                        doctorDiscountType = mDoctorDiscountObj[1] == null ? "" : mDoctorDiscountObj[1].toString();

                                        doctorDiscountFixedAmount = mDoctorDiscountObj[2] == null ? "" : mDoctorDiscountObj[2].toString();
                                        doctorDiscountPercentageAmount = mDoctorDiscountObj[3] == null ? "" : mDoctorDiscountObj[3].toString();

                                        if (doctorDiscountType.equals(1)) {
                                            doctorDiscountTypeText = "PercentageAmount";

                                            doctorActualFee = Double.parseDouble(doctorFeeAmount) - Double.parseDouble(doctorDiscountPercentageAmount);
                                            
                                            doctorDiscontPercentageFee = ((Double.parseDouble(doctorDiscountPercentageAmount)/ Double.parseDouble(doctorFeeAmount)) * 100);
                                            
 
                                        } else {
                                            doctorDiscountTypeText = "FixedAmount";
                                            doctorActualFee = Double.parseDouble(doctorFeeAmount) - Double.parseDouble(doctorDiscountFixedAmount);
                                        }

                                        doctorDiscountTD = mDoctorDiscountObj[4] == null ? "" : mDoctorDiscountObj[4].toString();
                                        doctorDiscountED = mDoctorDiscountObj[5] == null ? "" : mDoctorDiscountObj[5].toString();

                                    }
                                } else {

                                    doctorDiscountType = "2";
                                    doctorDiscountTypeText = "NoDiscount";
                                    doctorDiscountFixedAmount = "0.00";
                                    doctorDiscountPercentageAmount = "0.00";
                                    doctorDiscountTD = "";
                                    doctorDiscountED = "";
                                    doctorActualFee = Double.parseDouble(doctorFeeAmount);

                                }

                                String doctorActualFee1 = Double.toString(doctorActualFee);

                                responseObj.put("discountType", doctorDiscountType);
                                responseObj.put("discountTypeText", doctorDiscountTypeText);
                                responseObj.put("discountFixedAmount", Math.round(Double.parseDouble(doctorDiscountFixedAmount)));
                                responseObj.put("discountPercentageAmount", Math.round(Double.parseDouble(doctorDiscountPercentageAmount)));
                                responseObj.put("discontPercentageFee", Math.round(doctorDiscontPercentageFee));                                
                                responseObj.put("doctorDiscountTD", doctorDiscountTD);
                                responseObj.put("doctorDiscountED", doctorDiscountED);

                                responseObj.put("doctorActualFee", Math.round(doctorActualFee));
                                responseObj.put("doctorActualFee1", doctorActualFee1);
                                //  memberContentObj.put("totalWithDiscount", totalWithDiscount);
                                //   memberContentObj.put("totalWithOutDiscount", totalWithoutDiscount);


                                // doctor fee end
                                
                                
                                
                                
                                responseObj.put("Id", memberXId);

                                responseObj.put("doctorDegree0", doctorDegree0);
                                responseObj.put("doctorDegree1", doctorDegree1);
                                responseObj.put("doctorDegree2", doctorDegree2);
                                responseObj.put("doctorDegree3", doctorDegree3);
                                responseObj.put("doctorDegree4", doctorDegree4);

                                responseObj.put("doctorRegNo", doctorRegNo);
                                responseObj.put("doctorMedicalCollege", doctorMedicalCollege);

                                responseObj.put("organizationName", organizationName);
                                responseObj.put("organizationLogo", organizationLogoLink);
                                responseObj.put("unitName", unitName);
                                responseObj.put("nirbachoniAson", nirbachoniAson);
                                responseObj.put("ratingPoint", ratingPoint);
                                responseObj.put("likeCount", likeCount);
                                responseObj.put("shareCount", shareCount);
                                responseObj.put("uploadCount", uploadCount);
                                responseObj.put("bloodDonationCount", bloodDonationCount);

                                responseObj.put("MemberLifeStatus", memberLifeStatus);
                                responseObj.put("MemberLifeStatusText", memberLifeStatusText);
                                responseObj.put("MemberTypeId", mMemberTypeId);
                                responseObj.put("MemberTypeName", mMemberTypeName);
                                responseObj.put("MemberId", memberXIEBId);
                                responseObj.put("Name", memberXName);

                                responseObj.put("FatherName", fatherName);
                                responseObj.put("MotherName", motherName);
                                responseObj.put("PlaceOfBirth", placeOfBirth);
                                responseObj.put("DateOfBirth", dob);
                                responseObj.put("Gender", gender);
                                responseObj.put("BloodGroup", bloodGroup);

                                responseObj.put("Mobile", mobileNo);
                                responseObj.put("Phone1", phone1);
                                responseObj.put("Phone2", phone2);
                                responseObj.put("Email", memberEmail);

                                responseObj.put("Phone1View", memberXPhone1View);
                                responseObj.put("Phone2View", memberXPhone2View);
                                responseObj.put("MobileView", memberXMobileView);
                                responseObj.put("EmailView", memberXEmailView);

                                responseObj.put("Picture", memberXPictureLink);

                                responseObj.put("CenterId", memberCenterId);
                                responseObj.put("CenterName", memberXCenter);

                                responseObj.put("DivisionId", memberDivisionId);
                                responseObj.put("DivisionShortName", memberXDivisionShort);
                                responseObj.put("DivisionFullName", memberXDivisionFull);
                                responseObj.put("Address", addressObjArray);
                                responseObj.put("Education", educationObjArray);
                                responseObj.put("Profession", professionObjArray);
                                responseObj.put("Training", trainingObjArray);
                                responseObj.put("Publication", publicationObjArray);

                                memberContentObjArr.add(responseObj);

                            }

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "1");
                            logingObj.put("ResponseText", "Found");
                            logingObj.put("ResponseData", memberContentObjArr);

                        } else {

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "2");
                            logingObj.put("ResponseText", "Member is not doctor");
                            logingObj.put("ResponseData", logingObjArray);
                            logger.info("User ID :" + memberId + " Member is not doctor");

                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        } 
//        finally {
//            dbtrx.commit();
//
//        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
