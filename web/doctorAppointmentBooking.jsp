<%-- 
    Document   : doctorAppointmentBooking
    Created on : OCT 12, 2020, 1:05:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();
    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date dateToday = new Date();

    // Logger logger = Logger.getLogger("patientQueryRequest_jsp.class");
    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    getRegistryID getId = new getRegistryID();
    SendFirebasePushNotification fcmsend = new SendFirebasePushNotification();

    Logger logger = Logger.getLogger("doctorAppointmentBooking_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());
    DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
    Date dateFileToday = new Date();
    String filePrefixToday = formatterFileToday.format(dateFileToday);

    // String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";
    //  String appointmentNo = filePrefixToday + "_" + rand1 + "_" + rand2;
    String appointmentNo = filePrefixToday + "_" + rand1;

    String key = "";
    String memberId = "";
    String givenPassword = "";
    String doctorCategoryId = "";

    String doctorId = "";
    String appointmentDate = "";
    String appointmentSlotId = "";
    String patientId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId")) {
    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

        //  memberId = request.getParameter("memberId").trim();
        //   givenPassword = request.getParameter("password").trim();
        doctorId = request.getParameter("doctorId").trim();
        //  appointmentDate = request.getParameter("appointmentDate").trim();
        appointmentSlotId = request.getParameter("appointmentSlotId").trim();
        patientId = request.getParameter("patientId").trim();

      //  patientId = "1183";

        System.out.println("doctorId :: " + doctorId);
        System.out.println("appointmentDate :: " + appointmentDate);
        System.out.println("appointmentSlotId :: " + appointmentSlotId);
        System.out.println("patientId :: " + patientId);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    Query memberSQL = null;
    Object memberObj[] = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    Query patientAppointmentInfoAddSQL = null;
    Query patientCovidInfoAddSQL = null;

    Query appointmentBlockedCheckSQL = null;

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: doctorAppointmentBooking API rec :: " + rec);

    String memMemberName = "";
    String memberDeviceId = "";

    if (rec == 1) {

        try {

            //check appointment slot free 
            appointmentBlockedCheckSQL = dbsession.createSQLQuery("SELECT * from doctor_aponmnt_slot where blocked_status= '0' AND status= 0 AND id = '" + appointmentSlotId + "'");

            if (!appointmentBlockedCheckSQL.list().isEmpty()) {

                String idAappS = getId.getID(78); //Doctor_Appointment_Booking_ID
                int pAppointmentId = Integer.parseInt(idAappS);

                InetAddress ip;
                String hostname = "";
                String ipAddress = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostName();
                    ipAddress = ip.getHostAddress();

                } catch (UnknownHostException e) {

                    e.printStackTrace();
                }

                String adduser = patientId;
                String adddate = dateFormatX.format(dateToday);
                String addterm = hostname;
                String published = "1";

                String patientDetailsForNotification = "Test Patent";

                Query memberDeviceIdSQL = dbsession.createSQLQuery("SELECT device_id FROM member_deviceid where member_id='" + patientId + "'");
                memberDeviceId = memberDeviceIdSQL.uniqueResult() == null ? "" : memberDeviceIdSQL.uniqueResult().toString();

                System.out.println("memberDeviceId :: " + memberDeviceId);

                Query doctorDetailsInfoSQL = null;
                Object doctorDetailsInfoObj[] = null;

                String doctorName = "";
                String doctorDeviceId = "";
                String doctorDegree0 = "";
                String doctorDegree1 = "";
                String doctorDegree2 = "";
                String doctorDegree3 = "";
                String doctorDegree4 = "";
                String doctorRegNo = "";
                String doctorMedicalCollege = "";
                String doctorDetailsForNotification = "";

                doctorDetailsInfoSQL = dbsession.createSQLQuery("select m.member_name,md.device_id,"
                        + "ai.addi_info_0 as mDegree0,ai.addi_info_1 as mDegree1,ai.addi_info_2 as mDegree2 ,ai.addi_info_3 as mDegree3, "
                        + "ai.addi_info_4 as mDegree4,ai.addi_info_6 as mRegNo,ai.addi_info_7 as medicalCollege   "
                        + "FROM member m,member_additional_info ai,member_deviceid md  "
                        + "WHERE m.id = ai.member_id AND m.id = md.member_id AND m.id='" + doctorId + "'");

                //  String doctorDeviceId = doctorDeviceIdSQL.uniqueResult() == null ? "": doctorDeviceIdSQL.uniqueResult().toString();
                logger.info("doctorDetailsInfoSQL ::" + doctorDetailsInfoSQL);
                if (!doctorDetailsInfoSQL.list().isEmpty()) {
                    for (Iterator doctorTimeSlotIt = doctorDetailsInfoSQL.list().iterator(); doctorTimeSlotIt.hasNext();) {
                        doctorDetailsInfoObj = (Object[]) doctorTimeSlotIt.next();

                        doctorName = doctorDetailsInfoObj[1] == null ? "" : doctorDetailsInfoObj[0].toString();
                        doctorDeviceId = doctorDetailsInfoObj[1] == null ? "" : doctorDetailsInfoObj[1].toString();
                        doctorDegree0 = doctorDetailsInfoObj[2] == null ? "" : doctorDetailsInfoObj[2].toString();
                        doctorDegree1 = doctorDetailsInfoObj[3] == null ? "" : doctorDetailsInfoObj[3].toString();

                        doctorDegree2 = doctorDetailsInfoObj[4] == null ? "" : doctorDetailsInfoObj[4].toString();
                        doctorDegree3 = doctorDetailsInfoObj[5] == null ? "" : doctorDetailsInfoObj[5].toString();
                        doctorDegree4 = doctorDetailsInfoObj[6] == null ? "" : doctorDetailsInfoObj[6].toString();
                        doctorRegNo = doctorDetailsInfoObj[7] == null ? "" : doctorDetailsInfoObj[7].toString();
                        doctorMedicalCollege = doctorDetailsInfoObj[8] == null ? "" : doctorDetailsInfoObj[8].toString();

                    }
                }

                doctorDetailsForNotification = doctorName + "," + doctorDegree0 + "," + doctorDegree1 + "," + doctorMedicalCollege + ",Reg no-" + doctorRegNo;

                System.out.println("doctorDetailsForNotification :: " + doctorDetailsForNotification);

                System.out.println("doctorDeviceId :: " + doctorDeviceId);

                Query doctorTimeSlotSQL = null;
                Object doctorTimeSlotObj[] = null;

                String scheduleSlotSQL = null;
                Query scheduleSlotSQLQuery = null;
                Object[] scheduleSlotObj = null;
                String scheduleSlotId = "";
                String scheduleSlotStartTime = "";
                String scheduleSlotEndTime = "";
                String scheduleSlotName = "";
                String scheduleSlotName1 = "";
                String scheduleSlotStatus = "";

                scheduleSlotSQL = "SELECT das.id,das.slotstart_time,das.slotend_time, "
                        + "das.slot_name,das.name,das.status  "
                        + "FROM doctor_aponmnt_slot das "
                        + "WHERE das.id = '" + appointmentSlotId + "'";

                System.out.println("scheduleSlotSQL :: " + scheduleSlotSQL);

                String appointmentTime = "";

                scheduleSlotSQLQuery = dbsession.createSQLQuery(scheduleSlotSQL);

                if (!scheduleSlotSQLQuery.list().isEmpty()) {
                    for (Iterator scheduleSlotItr = scheduleSlotSQLQuery.list().iterator(); scheduleSlotItr.hasNext();) {

                        scheduleSlotObj = (Object[]) scheduleSlotItr.next();

                        scheduleSlotId = scheduleSlotObj[0].toString();
                        scheduleSlotStartTime = scheduleSlotObj[1].toString() == null ? "" : scheduleSlotObj[1].toString();
                        scheduleSlotEndTime = scheduleSlotObj[2] == null ? "" : scheduleSlotObj[2].toString();
                        scheduleSlotName = scheduleSlotObj[3] == null ? "" : scheduleSlotObj[3].toString();
                        scheduleSlotName1 = scheduleSlotObj[4] == null ? "" : scheduleSlotObj[4].toString();
                        scheduleSlotStatus = scheduleSlotObj[5] == null ? "" : scheduleSlotObj[5].toString();

                    }

                }

                patientAppointmentInfoAddSQL = dbsession.createSQLQuery("INSERT INTO doctor_patient_aponmnt("
                        + "id,"
                        + "appointment_no,"
                        + "aponmnt_slot_id,"
                        + "patent_id,"
                        + "doctor_id,"
                        + "add_user,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip"
                        + ") VALUES("
                        + "'" + pAppointmentId + "',"
                        + "'" + appointmentNo + "',"
                        + "'" + appointmentSlotId + "',"
                        + "'" + patientId + "',"
                        + "'" + doctorId + "',"
                        + "'" + adduser + "',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "'"
                        + "  ) ");

                logger.info("patientAppointmentInfoAddSQL ::" + patientAppointmentInfoAddSQL);
                patientAppointmentInfoAddSQL.executeUpdate();

                Query updateDoctorTimeSlotStatusSQL = dbsession.createSQLQuery("UPDATE  doctor_aponmnt_slot SET status = '1', mod_date='" + adddate + "'  WHERE id = '" + appointmentSlotId + "'");
                updateDoctorTimeSlotStatusSQL.executeUpdate();

                dbtrx.commit();

                if (dbtrx.wasCommitted()) {
                   
                    String msgPriority = "high"; // normal
                    appointmentTime = scheduleSlotStartTime;
                     /*
                    //Notification for doctor
                    if (!doctorDeviceId.equals("")) {

                        String msgTitle = memMemberName + " appointment details";
                        String msgBody = "Appointment Time :" + appointmentTime + " ," + patientDetailsForNotification;
                        String receipentDeviceId = doctorDeviceId;
                        //     String receipentDeviceId = "cGfWg1IlE1o:APA91bHKfVtMGSlAlLeufYlCuv2ZGlH1RAjY3ssnT8WNVJIJ0Hpvkav1VeHxQ-1WlYcVId09B2ubUDPL7lhzB7GXgU-gwtzZ96MpCwCUuTUHKofNQar32Ypb-mejf4xIPV6p66NsRMAY";
                        System.out.println("receipentDeviceId :: doctorDeviceId ::" + receipentDeviceId);

                        String fcmsendDoctor = fcmsend.sendSingleNotification(msgTitle, msgBody, receipentDeviceId, msgPriority);
                        logger.info("fcmsendDoctor" + fcmsendDoctor);
                        System.out.println("fcmsendDoctor::" + fcmsendDoctor);

                    }
                    */
                    //Notification for patient
                    //    System.out.println("fcmsendpatient::" + fcmsendpatient);
                    if (!memberDeviceId.equals("")) {
                        //  String appointmentTime_YMD_AM_PM = displayFormat_YMD_AM_PM.format(appointmentTime);
                        //   System.out.println("appointmentTime_YMD_AM_PM" + appointmentTime_YMD_AM_PM);
                        String msgTitle = memMemberName + " appointment details";
                        String msgBody = "Appointment Time :" + appointmentTime + " ," + doctorDetailsForNotification;
                        String receipentDeviceId = memberDeviceId;
                        //     String receipentDeviceId = "cGfWg1IlE1o:APA91bHKfVtMGSlAlLeufYlCuv2ZGlH1RAjY3ssnT8WNVJIJ0Hpvkav1VeHxQ-1WlYcVId09B2ubUDPL7lhzB7GXgU-gwtzZ96MpCwCUuTUHKofNQar32Ypb-mejf4xIPV6p66NsRMAY";
                        System.out.println("receipentDeviceId::" + receipentDeviceId);

                        String fcmsendpatient = fcmsend.sendSingleNotification(msgTitle, msgBody, receipentDeviceId, msgPriority);
                        logger.info("fcmsendpatient" + fcmsendpatient);
                        System.out.println("fcmsendpatient::" + fcmsendpatient);
                    }
                     

                    memberContentObj = new JSONObject();

                    memberContentObj.put("appointmentNo", appointmentNo);
                    memberContentObj.put("scheduleSlotId", scheduleSlotId);

                    memberContentObj.put("scheduleSlotStartTime", scheduleSlotStartTime);
                    memberContentObj.put("scheduleSlotName", scheduleSlotName);
                    memberContentObj.put("scheduleSlotName1", scheduleSlotName1);

                    memberContentObj.put("doctorName", doctorName);
                    memberContentObj.put("doctorRegNo", doctorRegNo);
                    memberContentObj.put("doctorMedicalCollege", doctorMedicalCollege);
                    memberContentObj.put("doctorDetails", doctorDetailsForNotification);

                    memberContentObjArr.add(memberContentObj);

                    responseDataObj = new JSONObject();
                    responseDataObj.put("ResponseCode", "1");
                    responseDataObj.put("ResponseText", "Appointment info added successfully");
                    responseDataObj.put("ResponseData", memberContentObjArr);
                } else {
                    responseDataObj = new JSONObject();
                    responseDataObj.put("ResponseCode", "0");
                    responseDataObj.put("ResponseText", "ERROR!!! When Appointment Info Add");
                    responseDataObj.put("ResponseData", memberContentObjArr);
                    logger.info("ERROR!!! When Appointment Info Add");
                }
                //  responseDataObj = new JSONObject();
                //   responseDataObj.put("ResponseCode", "1");
                //   responseDataObj.put("ResponseText", "Found");
                //   responseDataObj.put("ResponseData", memberContentObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "Appointment Already Booked or Block for Book");
                responseDataObj.put("ResponseData", memberContentObjArr);
            }
            /*
            //  String idS = getId.getID(70);
            //  int pQueryId = Integer.parseInt(idS);
            String idAappS = getId.getID(78); //Doctor_Appointment_Booking_ID
            int pAppointmentId = Integer.parseInt(idAappS);

            //  String idPatient = getId.getID(71);
            //  int pQueryPatientId = Integer.parseInt(idPatient);
            //      int pQueryPatientId = 1;
            //   int pQueryPatientId = pQueryId;
            //    doctorTimeslotId
            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }

            String adduser = patientId;
            String adddate = dateFormatX.format(dateToday);
            String addterm = hostname;
            String published = "1";

            patientAppointmentInfoAddSQL = dbsession.createSQLQuery("INSERT INTO doctor_patient_aponmnt("
                    + "id,"
                    + "appointment_no,"
                    + "aponmnt_slot_id,"
                    + "patent_id,"
                    + "doctor_id,"
                    + "add_user,"
                    + "add_date,"
                    + "add_term,"
                    + "add_ip"
                    + ") VALUES("
                    + "'" + pAppointmentId + "',"
                    + "'" + appointmentNo + "',"
                    + "'" + appointmentSlotId + "',"
                    + "'" + patientId + "',"
                    + "'" + doctorId + "',"
                    + "'" + adduser + "',"
                    + "'" + adddate + "',"
                    + "'" + hostname + "',"
                    + "'" + ipAddress + "'"
                    + "  ) ");

            logger.info("patientAppointmentInfoAddSQL ::" + patientAppointmentInfoAddSQL);

            patientAppointmentInfoAddSQL.executeUpdate();

            memberSQL = dbsession.createSQLQuery("SELECT da.id, da.aponmnt_date,da.aponmntstart_time, "
                    + "da.aponmntend_time,da.short_name, da.name,da.time_text,da.time_hour,"
                    + "da.time_minute ,da.time_number ,da.patient_time ,da.max_serial_count "
                    + "FROM  doctor_aponmnt_info da "
                    + "WHERE da.status = 1 "
                    + "AND da.doctor_id = '" + doctorId + "' "
                    + "AND da.aponmnt_date =' " + appointmentDate + "' "
                    + "ORDER BY da.aponmntstart_time ASC");

            //   memberTypeSQL = dbsession.createSQLQuery("SELECT * FROM doctor_category_name ORDER BY name ASC");
            if (!memberSQL.list().isEmpty()) {
                for (Iterator it = memberSQL.list().iterator(); it.hasNext();) {

                    memberObj = (Object[]) it.next();
                    appointmentScheduleId = memberObj[0].toString();

                    appointmentScheduleDate = memberObj[1] == null ? "" : memberObj[1].toString();
                    appointmentScheduleStartTime = memberObj[2] == null ? "" : memberObj[2].toString();
                    appointmentScheduleEndTime = memberObj[3] == null ? "" : memberObj[3].toString();

                    appointmentShortName = memberObj[4] == null ? "" : memberObj[4].toString();
                    appointmentName = memberObj[5] == null ? "" : memberObj[5].toString();
                    appointmentTimeText = memberObj[6] == null ? "" : memberObj[6].toString();
                    appointmentTimeHour = memberObj[7] == null ? "" : memberObj[7].toString();

                    appointmentTimeMinute = memberObj[8] == null ? "" : memberObj[8].toString();
                    appointmentTimeNumber = memberObj[9] == null ? "" : memberObj[9].toString();
                    appointmentPatientTime = memberObj[10] == null ? "" : memberObj[10].toString();
                    appointmentMaxSerialCount = memberObj[11] == null ? "" : memberObj[11].toString();

                    JSONObject scheduleSlotInfoResponseObj = new JSONObject();
                    JSONArray scheduleSlotInfoObjArray = new JSONArray();

                    String scheduleSlotSQL = null;
                    Query scheduleSlotSQLQuery = null;
                    Object[] scheduleSlotObj = null;
                    String scheduleSlotId = "";
                    String scheduleSlotStartTime = "";
                    String scheduleSlotEndTime = "";
                    String scheduleSlotName = "";
                    String scheduleSlotName1 = "";

                    scheduleSlotSQL = "SELECT das.id,das.slotstart_time,das.slotend_time, "
                            + "das.slot_name,das.name "
                            + "FROM doctor_aponmnt_slot das "
                            + "WHERE das.status = 1  AND das.aponmnt_id = '" + appointmentScheduleId + "'  "
                            + "ORDER BY das.id ASC ";

                    System.out.println("scheduleSlotSQL :: " + scheduleSlotSQL);

                    scheduleSlotSQLQuery = dbsession.createSQLQuery(scheduleSlotSQL);
                    if (!scheduleSlotSQLQuery.list().isEmpty()) {
                        for (Iterator scheduleSlotItr = scheduleSlotSQLQuery.list().iterator(); scheduleSlotItr.hasNext();) {

                            scheduleSlotObj = (Object[]) scheduleSlotItr.next();

                            scheduleSlotId = scheduleSlotObj[0].toString();
                            scheduleSlotStartTime = scheduleSlotObj[1].toString() == null ? "" : scheduleSlotObj[1].toString();
                            scheduleSlotEndTime = scheduleSlotObj[2] == null ? "" : scheduleSlotObj[2].toString();
                            scheduleSlotName = scheduleSlotObj[3] == null ? "" : scheduleSlotObj[3].toString();
                            scheduleSlotName1 = scheduleSlotObj[4] == null ? "" : scheduleSlotObj[4].toString();

                            scheduleSlotInfoResponseObj = new JSONObject();

                            scheduleSlotInfoResponseObj.put("scheduleSlotId", scheduleSlotId);
                            scheduleSlotInfoResponseObj.put("scheduleSlotStartTime", scheduleSlotStartTime);
                            scheduleSlotInfoResponseObj.put("scheduleSlotEndTime", scheduleSlotEndTime);
                            scheduleSlotInfoResponseObj.put("scheduleSlotName", scheduleSlotName);
                            scheduleSlotInfoResponseObj.put("scheduleSlotName1", scheduleSlotName1);

                            scheduleSlotInfoObjArray.add(scheduleSlotInfoResponseObj);
                        }

                    } else {

                        scheduleSlotInfoObjArray = new JSONArray();
                    }

                    memberContentObj = new JSONObject();

                    memberContentObj.put("scheduleSlotInfo", scheduleSlotInfoObjArray);

                    memberContentObj.put("appointmentScheduleId", appointmentScheduleId);

                    memberContentObj.put("appointmentScheduleDate", appointmentScheduleDate);
                    memberContentObj.put("appointmentScheduleStartTime", appointmentScheduleStartTime);
                    memberContentObj.put("appointmentScheduleEndTime", appointmentScheduleEndTime);
                    memberContentObj.put("appointmentShortName", appointmentShortName);
                    memberContentObj.put("appointmentName", appointmentName);
                    memberContentObj.put("appointmentTimeText", appointmentTimeText);

                    memberContentObj.put("appointmentTimeHour", appointmentTimeHour);
                    memberContentObj.put("appointmentTimeMinute", appointmentTimeMinute);

                    memberContentObj.put("appointmentTimeNumber", appointmentTimeNumber);
                    memberContentObj.put("appointmentPatientTime", appointmentPatientTime);

                    memberContentObj.put("appointmentMaxSerialCount", appointmentMaxSerialCount);

                    memberContentObjArr.add(memberContentObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", memberContentObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", memberContentObjArr);

            }
            
             */
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>