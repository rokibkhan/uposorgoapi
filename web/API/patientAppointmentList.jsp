<%-- 
    Document   : patientAppointmentList
    Created on : OCT 12, 2020, 1:05:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("patientAppointmentList_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String doctorCategoryId = "";

    String patientId = "";
    String appointmentDate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId")) {
    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

        //  memberId = request.getParameter("memberId").trim();
        //   givenPassword = request.getParameter("password").trim();
        patientId = request.getParameter("patientId").trim();
        //   appointmentDate = request.getParameter("appointmentDate").trim();

        System.out.println("patientId :: " + patientId);
        //  System.out.println("appointmentDate :: " + appointmentDate);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    Query memberSQL = null;
    Object memberObj[] = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    String memberXId = "";
    String appointmentId = "";
    String appointmentNo = "";
    String appointmentPaymentRefNo = "";
    String appointmentPaymentRefDesc = "";
    String appointmentStatus = "";
    String appointmentSlotId = "";
    String appointmentScheduleStartTime = "";
    String appointmentScheduleEndTime = "";
    String appointmentShortName = "";
    String appointmentName = "";
    String appointmentTimeText = "";
    String appointmentTimeHour = "";
    String appointmentTimeMinute = "";
    String appointmentTimeNumber = "";
    String appointmentPatientTime = "";
    String appointmentMaxSerialCount = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String doctorId = "";
    String doctorName = "";
    String doctorDeviceId = "";
    String doctorDegree0 = "";
    String doctorDegree1 = "";
    String doctorDegree2 = "";
    String doctorDegree3 = "";
    String doctorDegree4 = "";
    String doctorRegNo = "";
    String doctorMedicalCollege = "";
    String doctorImageLink = "";
    String doctorImageName = "";
    String doctorDetailsForNotification = "";

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: patientAppointmentList API rec :: " + rec);

    if (rec == 1) {

        try {

            memberSQL = dbsession.createSQLQuery("SELECT pa.id, pa.appointment_no,pa.payment_refno, "
                    + "pa.payment_refdesc,pa.status, "
                    + "pa.aponmnt_slot_id,das.slotstart_time,das.slotend_time,das.slot_name,das.name,"
                    + "pa.doctor_id,doc.member_name,"
                    + "ai.addi_info_0 as mDegree0,ai.addi_info_1 as mDegree1,ai.addi_info_2 as mDegree2 ,ai.addi_info_3 as mDegree3, "
                    + "ai.addi_info_4 as mDegree4,ai.addi_info_6 as mRegNo,ai.addi_info_7 as medicalCollege, doc.picture_name    "
                    + "FROM doctor_patient_aponmnt pa "
                    + "LEFT JOIN doctor_aponmnt_slot das ON das.id = pa.aponmnt_slot_id "
                    + "LEFT JOIN member doc ON doc.id = pa.doctor_id "
                    + "LEFT JOIN member_additional_info ai ON ai.member_id = doc.id "
                    + "WHERE pa.patent_id = '" + patientId + "' "
                    + "ORDER BY das.slotstart_time ASC");

            System.out.println("memberSQL :: " + memberSQL);

            //   memberTypeSQL = dbsession.createSQLQuery("SELECT * FROM doctor_category_name ORDER BY name ASC");
            if (!memberSQL.list().isEmpty()) {
                for (Iterator it = memberSQL.list().iterator(); it.hasNext();) {

                    memberObj = (Object[]) it.next();
                    appointmentId = memberObj[0].toString();

                    appointmentNo = memberObj[1] == null ? "" : memberObj[1].toString();
                    appointmentPaymentRefNo = memberObj[2] == null ? "" : memberObj[2].toString();
                    appointmentPaymentRefDesc = memberObj[3] == null ? "" : memberObj[3].toString();

                    appointmentStatus = memberObj[4] == null ? "" : memberObj[4].toString();

                    appointmentSlotId = memberObj[5] == null ? "" : memberObj[5].toString();
                    appointmentScheduleStartTime = memberObj[6] == null ? "" : memberObj[6].toString();
                    appointmentScheduleEndTime = memberObj[7] == null ? "" : memberObj[7].toString();
                    appointmentShortName = memberObj[8] == null ? "" : memberObj[8].toString();
                    appointmentName = memberObj[9] == null ? "" : memberObj[9].toString();

                    doctorId = memberObj[10] == null ? "" : memberObj[10].toString();
                    doctorName = memberObj[11] == null ? "" : memberObj[11].toString();

                    doctorDegree0 = memberObj[12] == null ? "" : memberObj[12].toString();
                    doctorDegree1 = memberObj[13] == null ? "" : memberObj[13].toString();

                    doctorDegree2 = memberObj[14] == null ? "" : memberObj[14].toString();
                    doctorDegree3 = memberObj[15] == null ? "" : memberObj[15].toString();
                    doctorDegree4 = memberObj[16] == null ? "" : memberObj[16].toString();
                    doctorRegNo = memberObj[17] == null ? "" : memberObj[17].toString();
                    doctorMedicalCollege = memberObj[18] == null ? "" : memberObj[18].toString();
                    doctorImageName = memberObj[19] == null ? "" : memberObj[19].toString();
                    doctorImageLink = GlobalVariable.imageMemberDirLink + doctorImageName;

                    doctorDetailsForNotification = doctorName + "," + doctorDegree0 + "," + doctorDegree1 + "," + doctorMedicalCollege + ",Reg no-" + doctorRegNo;

                    System.out.println("doctorDetailsForNotification :: " + doctorDetailsForNotification);

                    memberContentObj = new JSONObject();

                    //   memberContentObj.put("scheduleSlotInfo", scheduleSlotInfoObjArray);
                    memberContentObj.put("appointmentId", appointmentId);

                    memberContentObj.put("appointmentNo", appointmentNo);
                    memberContentObj.put("appointmentPayRefNo", appointmentPaymentRefNo);
                    memberContentObj.put("appointmentPaymentRefDesc", appointmentPaymentRefDesc);
                    memberContentObj.put("appointmentStatus", appointmentStatus);
                    memberContentObj.put("appointmentSlotId", appointmentSlotId);
                    memberContentObj.put("appointmentScheduleStartTime", appointmentScheduleStartTime);
                    memberContentObj.put("appointmentScheduleEndTime", appointmentScheduleEndTime);
                    memberContentObj.put("appointmentShortName", appointmentShortName);
                    memberContentObj.put("appointmentName", appointmentName);

                    memberContentObj.put("doctorId", doctorId);
                    memberContentObj.put("doctorName", doctorName);
                    memberContentObj.put("doctorDegree0", doctorDegree0);
                    memberContentObj.put("doctorDegree1", doctorDegree1);
                    memberContentObj.put("doctorDegree2", doctorDegree2);
                    memberContentObj.put("doctorDegree3", doctorDegree3);
                    memberContentObj.put("doctorDegree4", doctorDegree4);
                    memberContentObj.put("doctorRegNo", doctorRegNo);
                    memberContentObj.put("doctorMedicalCollege", doctorMedicalCollege);
                    memberContentObj.put("doctorDetails", doctorDetailsForNotification);
                    memberContentObj.put("doctorImageLink", doctorImageLink);
                    memberContentObjArr.add(memberContentObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", memberContentObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", memberContentObjArr);

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>