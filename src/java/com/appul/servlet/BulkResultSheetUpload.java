/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

/**
 *
 * @author Akter
 */
import com.appul.util.GlobalVariable;
import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.google.gson.JsonObject;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.hibernate.Query;
import org.hibernate.Session;

//@WebServlet(name = "BulkResultSheetUpload", urlPatterns = {"/BulkResultSheetUpload"})
//@MultipartConfig
public class BulkResultSheetUpload extends HttpServlet {

    public static Logger logger = Logger.getLogger("BulkResultSheetUpload.class");
    private static final long serialVersionUID = 1L;
    private static final String TMP_DIR_PATH = "/temp";
    private File tmpDir;
    private static final String DESTINATION_DIR_PATH = "/upload";
    private File destinationDir;

    public BulkResultSheetUpload() {
        super();
    }

    public void init(ServletConfig config) throws ServletException {

        super.init(config);
        String realPath = getServletContext().getRealPath(TMP_DIR_PATH);
        tmpDir = new File(realPath);
        if (!tmpDir.isDirectory()) {
            throw new ServletException(TMP_DIR_PATH + " is not a directory");
        }

        realPath = getServletContext().getRealPath(DESTINATION_DIR_PATH);
        destinationDir = new File(realPath);
        if (!destinationDir.isDirectory()) {
            throw new ServletException(DESTINATION_DIR_PATH + " is not a directory");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //PrintWriter to send the JSON response back
        PrintWriter out = response.getWriter();

        //set content type and header attributes
        request.getSession(false);
        response.setContentType("text/html");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
        String sessionid = request.getParameter("sessionid").trim();
        //   String userId = request.getSession(true).getAttribute("username").toString();
        String userId = "ADMIN";
        String entryUserID = "";
        // String entryUserID = "ADMIN";

        //String unitId = request.getSession(true).getAttribute("unitId").toString();  
        //    String attributeId = request.getSession(true).getAttribute("attributeId").toString();
        //    String effectiveDate = request.getSession(true).getAttribute("effectiveDate").toString();
        //    String description = request.getSession(true).getAttribute("remarks").toString();
        //   String universityId = "6"; //IUBAT
        String universityId = ""; //IUBAT

//        String unitId = "18";
//        String year = "2017";
//        String description = "EL";
//        String attributeId = "8";
        DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();

        //Set the size threshold, above which content will be stored on disk.
        fileItemFactory.setSizeThreshold(1 * 1024 * 1024 * 10); //1 MB

        //Set the temporary directory to store the uploaded files of size above threshold.
        fileItemFactory.setRepository(tmpDir);

        ServletFileUpload uploadHandler = new ServletFileUpload(fileItemFactory);
        JsonObject myObj = new JsonObject();

        String fileName = null;
        File file = null;
        String msg = "";

        try {

            //Parse the request
            List items = uploadHandler.parseRequest(request);
            Iterator iterator = items.iterator();
            while (iterator.hasNext()) {
                FileItem item = (FileItem) iterator.next();

                //Handle Form Fields
                if (item.isFormField()) {

                    System.out.println(" item.isFormField() Field Name = " + item.getFieldName() + ", Value = " + item.getString());
                    logger.info("Field Name = " + item.getFieldName() + ", Value = " + item.getString());

                    if (item.getFieldName().trim().equalsIgnoreCase("filename")) {
                        fileName = item.getString().trim();
                    }
                    if (item.getFieldName().trim().equalsIgnoreCase("universityName")) {
                        universityId = item.getString().trim();
                    }
                    if (item.getFieldName().trim().equalsIgnoreCase("entryUserID")) {
                        entryUserID = item.getString().trim();
                    }

                } //Handle Uploaded files.
                else {
                    System.out.println("item.isFormField() Else Field Name = " + item.getFieldName()
                            + ", File Name = " + item.getName()
                            + ", Content type = " + item.getContentType()
                            + ", File Size = " + item.getSize());
                    fileName = item.getName().trim();

                    logger.info("Field Name = " + item.getFieldName()
                            + ", File Name = " + item.getName()
                            + ", Content type = " + item.getContentType()
                            + ", File Size = " + item.getSize());

                    //Write file to the ultimate location.
                    file = new File(destinationDir, item.getName());
                    item.write(file);
                }

            }

            Session dbsession = null;

            dbsession = HibernateUtil.getSessionFactory().openSession();

            try {
                Query q1 = dbsession.createSQLQuery("SELECT * FROM university_result_file WHERE file_name='" + fileName + "'");
                if (q1.list().isEmpty()) {

                    int count = 0;
                    String extension = FilenameUtils.getExtension(fileName);
                    if (extension.trim().equalsIgnoreCase("xlsx")) {
                        System.out.println("xlsx:");
                        count = processExcelFile(file, fileName, universityId, entryUserID);
                        System.out.println("xlsx:" + count);
                    } else if (extension.trim().equalsIgnoreCase("xls")) {
                        //process your binary excel file
                        System.out.println("xls:");
                        count = processXlslFile(file, fileName, universityId, entryUserID);
                        System.out.println("xls:" + count);
                    }
                    if (extension.trim().equalsIgnoreCase("csv")) {
                        //process your CSV file
//                count = processExcelFile(file,fileName);
                    }

//                    myObj.addProperty("success", true);
//                    myObj.addProperty("message", count + " item(s) were processed for file " + fileName);
//                    out.println(myObj.toString());
                    out.println(count + " item(s) were processed for file " + fileName);
                    logger.info("message" + count + " item(s) were processed for file " + fileName);

                    msg = "SUCESS!!! " + count + "  item(s) were processed for file " + fileName;
                    //  response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/bulkResultSheetList.jsp?sessionid=" + sessionid + "&strMsg=" + msg + "&fileName=" + fileName);

                } else {
                    dbsession.close();

//                    myObj.addProperty("error: ", fileName + " already exists! Please try another file.");
//                    out.println(myObj.toString());
                    out.println(fileName + " already exists! Please try another file.");
                    System.out.println(fileName + " already exists! Please try another file.");
                    msg = "ERROR!!! " + fileName + " already exists! Please try another file.";
                    response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/bulkResultSheetAdd.jsp?sessionid=" + sessionid + "&strMsg=" + msg + "&fileName=" + fileName);
                }

            } catch (Exception e) {
                logger.info(e.getMessage());
                msg = "Upload Failed";
                response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/bulkResultSheetAdd.jsp?sessionid=" + sessionid + "&strMsg=" + msg + "&fileName=" + fileName);
            } finally {
                dbsession.close();
                msg = "Upload Successful";
                response.sendRedirect(GlobalVariable.baseUrl + "/systemManagement/bulkResultSheetList.jsp?sessionid=" + sessionid + "&strMsg=" + msg + "&fileName=" + fileName);
            }

        } catch (FileUploadException ex) {
            log("Error encountered while parsing the request", ex);
            myObj.addProperty("success", false);
            out.println(myObj.toString());
            logger.info("Error encountered while parsing the request", ex);
        } catch (Exception ex) {
            log("Error encountered while uploading file", ex);
            myObj.addProperty("success", false);
            out.println(myObj.toString());
            logger.info("Error encountered while uploading file", ex);
        }

        out.close();

    }

    private int processExcelFile(File file, String fileName, String universityId, String userId) {

        int count = 0;

        Session dbsession = null;

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        String studentId = "";
        String studentName = "";
        String studentDOB = "";
        String studentPassingYear = "";
        Double studentPassingYearIn = 0.0;
        int studentPassingYearI = 0;
        Double studentIdn = 0.0;
        Double studentResult = 0.0;
        int studentIdI = 0;

        String studentAdmissionSession = "";
        String studentEngineeringField = "";
        String studentRequiredCreditHrs = "";
        Double studentRequiredCreditHrsIn = 0.0;
        int studentRequiredCreditHrsI = 0;

        getRegistryID getVid = new getRegistryID();
        String resSheetFileId = getVid.getID(58);// UNIVERSITY_RESULT_FILE_ID
        InetAddress ip;
        String hostname = "";
        String ipAddress = "";
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            ipAddress = ip.getHostAddress();

        } catch (UnknownHostException e) {

            e.printStackTrace();
        }

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);
        int errorCode = 0;

        try {
            // Creating Input Stream
            FileInputStream myInput = new FileInputStream(file);

            XSSFWorkbook myWorkBook = new XSSFWorkbook(myInput);

            // Get the first sheet from workbook
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);

            Iterator<Row> rowIter = mySheet.rowIterator();
            Object[] obj = null;

            while (rowIter.hasNext()) {

                XSSFRow myRow = (XSSFRow) rowIter.next();
                Iterator<Cell> cellIter = myRow.cellIterator();
                while (cellIter.hasNext()) {

                    XSSFCell myCell = (XSSFCell) cellIter.next();
                    System.out.println("processExcelFile Cell column index: " + myCell.getColumnIndex());

                    if (myCell.getColumnIndex() == 0) {

                        switch (myCell.getCellType()) {
                            case XSSFCell.CELL_TYPE_NUMERIC:
                                // System.out.println("processExcelFile CELL_TYPE_NUMERIC studentId : " + myCell.getNumericCellValue());
                                studentIdn = myCell.getNumericCellValue();
                                //   System.out.println("processExcelFile CELL_TYPE_NUMERIC studentId new BigDecimal : " + new BigDecimal(studentIdn).toPlainString());                                
                                //  studentIdI = studentIdn.intValue();
                                // System.out.println("processExcelFile CELL_TYPE_NUMERIC studentIdI : " + studentIdI);                                
                                // studentId = String.valueOf(studentIdI);
                                studentId = new BigDecimal(studentIdn).toPlainString();
                                // System.out.println("processExcelFile CELL_TYPE_NUMERIC studentId : " + studentId);
                                break;
                            case XSSFCell.CELL_TYPE_STRING:
                                //  System.out.println("processExcelFile CELL_TYPE_STRING studentId : " + myCell.getRichStringCellValue().getString());
                                studentId = myCell.getRichStringCellValue().getString();
                                break;
                            default:
//                                System.out.println("Cell Value: " + myCell.getRawValue());
                                studentId = myCell.getRawValue();
                                break;
                        }
                    } else if (myCell.getColumnIndex() == 1) {
                        studentName = myCell.getRichStringCellValue().getString();
                    } else if (myCell.getColumnIndex() == 2) {
                        studentDOB = myCell.getRichStringCellValue().getString();
                    } else if (myCell.getColumnIndex() == 3) {

                        switch (myCell.getCellType()) {
                            case XSSFCell.CELL_TYPE_NUMERIC:
                                studentPassingYearIn = myCell.getNumericCellValue();
                                studentPassingYearI = studentPassingYearIn.intValue();

                                studentPassingYear = String.valueOf(studentPassingYearI);
                                break;
                            case XSSFCell.CELL_TYPE_STRING:
                                studentPassingYear = myCell.getRichStringCellValue().getString();
                                break;
                            default:
                                studentPassingYear = myCell.getRawValue();
                                break;
                        }
                    } else if (myCell.getColumnIndex() == 4) {
                        studentResult = myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 5) {
                        studentAdmissionSession = myCell.getRichStringCellValue().getString();
                    } else if (myCell.getColumnIndex() == 6) {
                        studentEngineeringField = myCell.getRichStringCellValue().getString();
                    } else if (myCell.getColumnIndex() == 7) {

                        switch (myCell.getCellType()) {
                            case XSSFCell.CELL_TYPE_NUMERIC:
                                studentRequiredCreditHrsIn = myCell.getNumericCellValue();
                                studentRequiredCreditHrsI = studentRequiredCreditHrsIn.intValue();

                                studentRequiredCreditHrs = String.valueOf(studentRequiredCreditHrsI);
                                break;
                            case XSSFCell.CELL_TYPE_STRING:
                                studentRequiredCreditHrs = myCell.getRichStringCellValue().getString();
                                break;
                            default:
                                studentRequiredCreditHrs = myCell.getRawValue();
                                break;
                        }
                    }

                    if (myCell.getColumnIndex() == 0 && !studentId.equals("")) {

                        count++;
                        System.out.println("processExcelFile count#####" + count);
                        System.out.println("processExcelFile studentId#####" + studentId);

                    }

                }

                if (!studentId.equalsIgnoreCase("")) {
                    String resSheetResultId = getVid.getID(59);//UNIVERSITY_RESULT_BUFFER_ID
                    Query studentResultBufferSQL = dbsession.createSQLQuery("INSERT INTO university_result_buffer("
                            + "result_id,"
                            + "student_id,"
                            + "student_name,"
                            + "student_birth_date,"
                            + "passing_year,"
                            + "student_result,"
                            + "admission_session,"
                            + "engineering_field,"
                            + "required_credit_hrs,"
                            + "university_id,"
                            + "file_id,"
                            + "status,"
                            + "add_user,"
                            + "add_date,"
                            + "add_term,"
                            + "add_ip) VALUES("
                            + "'" + resSheetResultId + "',"
                            + "'" + studentId + "',"
                            + "'" + studentName + "',"
                            + "'" + studentDOB + "',"
                            + "'" + studentPassingYear + "',"
                            + "'" + studentResult + "',"
                            + "'" + studentAdmissionSession + "',"
                            + "'" + studentEngineeringField + "',"
                            + "'" + studentRequiredCreditHrs + "',"
                            + "'" + universityId + "',"
                            + "'" + resSheetFileId + "',"
                            + "'1',"
                            + "'" + userId + "',"
                            + "'" + adddate + "',"
                            + "'" + hostname + "',"
                            + "'" + ipAddress + "')");

                    System.out.println("processExcelFile studentResultBufferSQL#####" + studentResultBufferSQL);

                    studentResultBufferSQL.executeUpdate();
                }
            }//end while

            Query studentResultFileSQL = dbsession.createSQLQuery("INSERT INTO university_result_file("
                    + "ID,"
                    + "file_name,"
                    + "rec_count,"
                    + "upload_time,"
                    + "university_id,"
                    + "status,"
                    + "add_user,"
                    + "add_date,"
                    + "add_term,"
                    + "add_ip) VALUES("
                    + " " + resSheetFileId + ", "
                    + "'" + fileName + "',"
                    + "" + count + ","
                    + "'" + adddate + "',"
                    + "'" + universityId + "',"
                    + "'0',"
                    + "'" + userId + "',"
                    + "'" + adddate + "',"
                    + "'" + hostname + "',"
                    + "'" + ipAddress + "')");

            System.out.println("processExcelFile studentResultFileSQL::" + studentResultFileSQL);

            studentResultFileSQL.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Upload Failed");
            System.out.println("Upload Failed");

        } finally {

            dbsession.flush();
            dbtrx.commit();
            dbsession.close();
            logger.info("Upload Successful");
            System.out.println("Upload Successful");

        }
        return count;

    }

    private int processXlslFile(File file, String fileName, String universityId, String userId) {

        int count = 0;

        Session dbsession = null;

        dbsession = HibernateUtil.getSessionFactory().openSession();
        org.hibernate.Transaction dbtrx = null;
        dbtrx = dbsession.beginTransaction();
        String studentId = "";
        String studentName = "";
        String studentDOB = "";
        String studentPassingYear = "";
        Double studentPassingYearIn = 0.0;
        int studentPassingYearI = 0;
        Double studentIdn = 0.0;
        Double studentResult = 0.0;
        int studentIdI = 0;

        String studentAdmissionSession = "";
        String studentEngineeringField = "";
        String studentRequiredCreditHrs = "";
        Double studentRequiredCreditHrsIn = 0.0;
        int studentRequiredCreditHrsI = 0;

        getRegistryID getVid = new getRegistryID();
        String resSheetFileId = getVid.getID(58);// UNIVERSITY_RESULT_FILE_ID
        InetAddress ip;
        String hostname = "";
        String ipAddress = "";
        try {
            ip = InetAddress.getLocalHost();
            hostname = ip.getHostName();
            ipAddress = ip.getHostAddress();

        } catch (UnknownHostException e) {

            e.printStackTrace();
        }

        DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateX = new Date();
        String adddate = dateFormatX.format(dateX);
        int errorCode = 0;

        try {
            // Creating Input Stream
            FileInputStream myInput = new FileInputStream(file);

            HSSFWorkbook myWorkBook = new HSSFWorkbook(myInput);
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);
            Iterator<Row> rowIter = mySheet.rowIterator();
            Object[] obj = null;
            while (rowIter.hasNext()) {

                HSSFRow myRow = (HSSFRow) rowIter.next();
                Iterator<Cell> cellIter = myRow.cellIterator();
                while (cellIter.hasNext()) {

                    HSSFCell myCell = (HSSFCell) cellIter.next();
//                    System.out.println("Cell column index: " + myCell.getColumnIndex());

                    if (myCell.getColumnIndex() == 0) {

                        switch (myCell.getCellType()) {
                            case HSSFCell.CELL_TYPE_NUMERIC:
                                // System.out.println("processExcelFile CELL_TYPE_NUMERIC studentId : " + myCell.getNumericCellValue());
                                studentIdn = myCell.getNumericCellValue();
                                //   System.out.println("processExcelFile CELL_TYPE_NUMERIC studentId new BigDecimal : " + new BigDecimal(studentIdn).toPlainString());                                
                                //  studentIdI = studentIdn.intValue();
                                // System.out.println("processExcelFile CELL_TYPE_NUMERIC studentIdI : " + studentIdI);                                
                                // studentId = String.valueOf(studentIdI);
                                studentId = new BigDecimal(studentIdn).toPlainString();
                                // System.out.println("processExcelFile CELL_TYPE_NUMERIC studentId : " + studentId);

                                break;
                            case HSSFCell.CELL_TYPE_STRING:
//                                System.out.println("Cell Value: " + myCell.getRichStringCellValue().getString());
                                studentId = myCell.getRichStringCellValue().getString();
                                break;
                            default:
//                                System.out.println("Cell Value: " + myCell.getRawValue());
//                                pfId = myCell.getRawValue();
                                break;
                        }
                    } //                    else if (myCell.getColumnIndex() == 1) {
                    //                        amount = myCell.getNumericCellValue();
                    //                    }
                    else if (myCell.getColumnIndex() == 1) {
                        //  amount = myCell.getNumericCellValue();
                        studentName = myCell.getRichStringCellValue().getString();
                    } else if (myCell.getColumnIndex() == 2) {
                        //  amount = myCell.getNumericCellValue();
                        studentDOB = myCell.getRichStringCellValue().getString();
                    } else if (myCell.getColumnIndex() == 3) {
                        //  amount = myCell.getNumericCellValue();
                        //  studentPassingYear = myCell.getRichStringCellValue().getString();

                        switch (myCell.getCellType()) {
                            case HSSFCell.CELL_TYPE_NUMERIC:
//                                System.out.println("Cell Value: " + myCell.getNumericCellValue());
                                studentPassingYearIn = myCell.getNumericCellValue();
                                studentPassingYearI = studentPassingYearIn.intValue();

                                studentPassingYear = String.valueOf(studentPassingYearI);
                                break;
                            case HSSFCell.CELL_TYPE_STRING:
//                                System.out.println("Cell Value: " + myCell.getRichStringCellValue().getString());
                                studentPassingYear = myCell.getRichStringCellValue().getString();
                                break;
                            default:
//                                System.out.println("Cell Value: " + myCell.getRawValue());
//                                pfId = myCell.getRawValue();
                                break;
                        }
                    } else if (myCell.getColumnIndex() == 4) {
                        studentResult = myCell.getNumericCellValue();
                    } else if (myCell.getColumnIndex() == 5) {
                        studentAdmissionSession = myCell.getRichStringCellValue().getString();
                    } else if (myCell.getColumnIndex() == 6) {
                        studentEngineeringField = myCell.getRichStringCellValue().getString();
                    } else if (myCell.getColumnIndex() == 7) {
                        switch (myCell.getCellType()) {
                            case HSSFCell.CELL_TYPE_NUMERIC:
//                                System.out.println("Cell Value: " + myCell.getNumericCellValue());
                                studentRequiredCreditHrsIn = myCell.getNumericCellValue();
                                studentRequiredCreditHrsI = studentRequiredCreditHrsIn.intValue();

                                studentRequiredCreditHrs = String.valueOf(studentRequiredCreditHrsI);
                                break;
                            case HSSFCell.CELL_TYPE_STRING:
//                                System.out.println("Cell Value: " + myCell.getRichStringCellValue().getString());
                                studentRequiredCreditHrs = myCell.getRichStringCellValue().getString();
                                break;
                            default:
//                                System.out.println("Cell Value: " + myCell.getRawValue());
//                                pfId = myCell.getRawValue();
                                break;
                        }
                    }

//                    if (myCell.getColumnIndex() == 0 && !myCell.getStringCellValue().trim().equals("")) {
                    if (myCell.getColumnIndex() == 0 && !studentId.equals("")) {
                        count++;

                    }

                }
                if (!studentId.equalsIgnoreCase("")) {
                    String resSheetResultId = getVid.getID(59);//UNIVERSITY_RESULT_BUFFER_ID
                    Query studentResultBufferSQL = dbsession.createSQLQuery("INSERT INTO university_result_buffer("
                            + "result_id,"
                            + "student_id,"
                            + "student_name,"
                            + "student_birth_date,"
                            + "passing_year,"
                            + "student_result,"
                            + "admission_session,"
                            + "engineering_field,"
                            + "required_credit_hrs,"
                            + "university_id,"
                            + "file_id,"
                            + "status,"
                            + "add_user,"
                            + "add_date,"
                            + "add_term,"
                            + "add_ip) VALUES("
                            + "'" + resSheetResultId + "',"
                            + "'" + studentId + "',"
                            + "'" + studentName + "',"
                            + "'" + studentDOB + "',"
                            + "'" + studentPassingYear + "',"
                            + "'" + studentResult + "',"
                            + "'" + studentAdmissionSession + "',"
                            + "'" + studentEngineeringField + "',"
                            + "'" + studentRequiredCreditHrs + "',"
                            + "'" + universityId + "',"
                            + "'" + resSheetFileId + "',"
                            + "'1',"
                            + "'" + userId + "',"
                            + "'" + adddate + "',"
                            + "'" + hostname + "',"
                            + "'" + ipAddress + "')");
                    studentResultBufferSQL.executeUpdate();
                }
            }

            Query studentResultFileSQL = dbsession.createSQLQuery("INSERT INTO university_result_file("
                    + "ID,"
                    + "file_name,"
                    + "rec_count,"
                    + "upload_time,"
                    + "university_id,"
                    + "status,"
                    + "add_user,"
                    + "add_date,"
                    + "add_term,"
                    + "add_ip) VALUES("
                    + " " + resSheetFileId + ", "
                    + "'" + fileName + "',"
                    + "" + count + ","
                    + "'" + adddate + "',"
                    + "'" + universityId + "',"
                    + "'0',"
                    + "'" + userId + "',"
                    + "'" + adddate + "',"
                    + "'" + hostname + "',"
                    + "'" + ipAddress + "')");

            System.out.println("studentResultFileSQL::" + studentResultFileSQL);

            studentResultFileSQL.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Upload Failed");
            System.out.println("Upload Failed");

        } finally {

            dbsession.flush();
            dbtrx.commit();
            dbsession.close();
            logger.info("Upload Successful");
            System.out.println("Upload Successful");

        }
        return count;

    }

}
