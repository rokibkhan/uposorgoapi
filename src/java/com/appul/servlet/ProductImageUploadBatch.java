/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.appul.servlet;

import com.appul.util.HibernateUtil;
import com.appul.util.getRegistryID;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.internal.SessionImpl;

@WebServlet(name = "ProductImageUploadBatch", urlPatterns = {"/ProductImageUploadBatch"})
//@MultipartConfig
public class ProductImageUploadBatch extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            String userId = request.getSession(true).getAttribute("username").toString();

            String productCode = "";

            getRegistryID getId = new getRegistryID();
            String idS = "";
            int productId = 0;
            int productImageId = 0;
            InetAddress ip;
            String hostname = "";
            String ipAddress = "";
            try {
                ip = InetAddress.getLocalHost();
                hostname = ip.getHostName();
                ipAddress = ip.getHostAddress();

            } catch (UnknownHostException e) {

                e.printStackTrace();
            }

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            String entryDate = dateFormat.format(date);

            Session dbsession = HibernateUtil.getSessionFactory().openSession();
            SessionImpl sessionImpl = (SessionImpl) dbsession;
            Connection con = sessionImpl.connection();
            PreparedStatement ps;
            PreparedStatement ps1;
            String file1 = "";
            String file2 = "";
            String file3 = "";
            String file4 = "";
            String file5 = "";
//            String filePath = "D:\\RTLBILLING\\DEV_NETBEANS_8.02\\APPUL\\web\\upload\\product_images\\";
            String filePath = "/app/APPUL/webapps/APPUL/upload/product_images/";
            Query q1 = dbsession.createSQLQuery("select product_id,product_code from appuldb.product where product_id not in (SELECT product_id FROM appuldb.product_category_image )order by product_id");

            Object[] object = null;

//            String querySetLimit = "SET GLOBAL max_allowed_packet=512857600;";  // 10 MB
//            Statement stSetLimit = con.createStatement();
//            stSetLimit.execute(querySetLimit);
            for (Iterator itr = q1.list().iterator(); itr.hasNext();) {
                object = (Object[]) itr.next();

                productId = Integer.parseInt(object[0].toString());
                productCode = object[1].toString();
                file1 = filePath + productCode + "_430X556.jpg";
                file2 = filePath + productCode + "_72X93.jpg";
                file3 = filePath + productCode + "_220X285.jpg";
                file4 = filePath + productCode + "_1546X2000.jpg";
                file5 = filePath + productCode + "_48X62.jpg";
                File file11 = new File(file1);
                File file12 = new File(file2);
                File file13 = new File(file3);
                File file14 = new File(file4);
                File file15 = new File(file5);
                InputStream inputStream1 = new FileInputStream(file11);
                InputStream inputStream2 = new FileInputStream(file12);
                InputStream inputStream3 = new FileInputStream(file13);
                InputStream inputStream31 = new FileInputStream(file13);
                InputStream inputStream4 = new FileInputStream(file14);
                InputStream inputStream5 = new FileInputStream(file15);

                idS = getId.getID(24);
                System.out.println("file1" + file1 + "=" + inputStream1);
                System.out.println("file2" + file2 + "=" + inputStream2);
                System.out.println("file3" + file3 + "=" + inputStream3);
                System.out.println("file4" + file4 + "=" + inputStream4);
                System.out.println("file5" + file5 + "=" + inputStream5);

                ps1 = con.prepareStatement("update product set product_image=?,color_image=? where product_id=? ");

                // size must be converted to int otherwise it results in error
                ps1.setBinaryStream(1, inputStream31, (int) file13.length());
                ps1.setBinaryStream(2, inputStream5, (int) file15.length());
                ps1.setInt(3, productId);
                System.out.println(ps1);
                ps1.executeUpdate();
                

                productImageId = Integer.parseInt(idS);
//                System.out.println("productId" + productId);
                ps = con.prepareStatement("insert into product_category_image values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
                ps.setInt(1, productImageId);
                ps.setInt(2, productId);
                ps.setString(3,  productCode + "_430X556.JPEG");
                ps.setString(4,  productCode + "_72X93.JPEG");
                ps.setString(5,  productCode + "_220X285.JPEG");
                ps.setString(6,  productCode + "_1546X2000.JPEG");
                ps.setBinaryStream(7, inputStream1, (int) file11.length());
                ps.setBinaryStream(8, inputStream2, (int) file12.length());
                ps.setBinaryStream(9, inputStream3, (int) file13.length());
                ps.setBinaryStream(10, inputStream4, (int) file14.length());
                ps.setString(11, entryDate);
                ps.setString(12, userId);
                ps.setString(13, hostname);
                ps.setString(14, ipAddress);
                ps.setString(15, "");
                ps.setString(16, "");
                ps.setString(17, "");
                ps.setString(18, null);
//                ps.executeUpdate();
//                ps.close();
                System.out.println(ps);
                int row = ps.executeUpdate();
                if (row > 0) {
                    System.out.println("Successfully Added Product Image.");
                }
            }
            con.commit();

            con.close();
            dbsession.close();
//            out.println("Successfully Added Product Image.");

        } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            out.close();
        }
    }
}
