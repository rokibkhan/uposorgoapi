<%-- 
/* ------------------------------------------------------------------------------
    Project    : Uposorgo API
    Document   : login.jsp
    Description: Login API handles login in system. 
                 * @access public
                 * @param string memberId like mobile number
                 * @param string password
                 * @param string key
                 * @return array with next time system access token
    Version    :1.0.0
    Created on : APR 27, 2020, 12:00:05 PM
    Updated on : OCT 23, 2020, 12:00:05 PM
    Author     : Tahajjat Tuhin
    Email      : tahajjat@bydoengineering.com
    Homepage   : www.bydoengineering.com
 * *------------------------------------------------------------------------------
 * * COPYRIGHT (c) 2020 - 2021 ByDo Engineering Ltd.
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("login_jsp.class");
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String mobileNumber = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        mobileNumber = memberId;

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    String firstLetter = mobileNumber.substring(0, 1);
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";

    String mobileNumber1 = "";

    if (firstLetter.equals("0")) {
        mobileNumber1 = adjustLettter1 + mobileNumber;
    }
    if (firstLetter.equals("8")) {
        mobileNumber1 = adjustLettter2 + mobileNumber;
    }
    if (firstLetter.equals("+")) {
        mobileNumber1 = mobileNumber;
    }

    Member member = null;
    MemberOrganization mOrganization = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String dbPassStatus = "";
    String memberEmail = "";
    String memberType = "";
    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";

    String addressType = "";
    String consumerType = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaId1 = "";
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    String organizationName = "";
    String organizationLogo = "";
    String organizationLogoLink = "";
    String unitName = "";
    String nirbachoniAson = "";
    String ratingPoint = "";
    String likeCount = "";
    String shareCount = "";
    String uploadCount = "";
    String bloodDonationCount = "";

//    'organizationName' => $organizationName,
//                'orgLogo' => $orgLogo,
//                'unitName' => $posting,
//                'nirbachoniAson' => $nirbachoni_ason,
//                'ratingPoint' => $useRating,
//                'shareCount' => $shareContentPoint,
//                'uploadCount' => $uploadContentPoint,
//                'bloodDonationCount' => $bloodDonationCounter,
    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: login rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();
    JSONObject addressObj = new JSONObject();
    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;
    Object[] memberKeyObj = null;

    Query mMemberTypeSQL = null;
    Object[] mMemberTypeObj = null;

    String mMemberTypeId = "";
    String mMemberTypeName = "";

    Query qMemberAddress = null;
    Object[] mAddressObj = null;
    Object[] pAddressObj = null;

    Query memberLifeSQL = null;
    Object[] memberLifeObj = null;
    String memberLifeStatus = "";
    String memberLifeStatusText = "";

    Query mMemberOptionViewSQL = null;
    Object[] mMemberOptionViewObj = null;

    String memberXPhone1View = "";
    String memberXPhone2View = "";
    String memberXMobileView = "";
    String memberXEmailView = "";

    Query mMemberAdminOptionViewSQL = null;
    Object mMemberAdminOptionViewObj[] = null;

    String postDeletePermission = "";
    String noticeDeletePermission = "";
    String bloodDeletePermission = "";
    String doctorQueryDeletePermission = "";

    Query memberTopicsSQL = null;
    Object[] memberTopicsObj = null;

    JSONObject memberTopicsResponseObj = new JSONObject();
    JSONArray memberTopicsResponseObjArray = new JSONArray();

    String memberTopicsId = "";
    String memberTopicsName = "";

    String mAccessToken = "";

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            //  qMember = dbsession.createQuery(" from Member where mobile = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + mobileNumber1 + "'");
            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();

                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key,status FROM member_credential where member_id='" + memId + "'");

                    for (Iterator memberKeyItr = memberKeySQL.list().iterator(); memberKeyItr.hasNext();) {
                        memberKeyObj = (Object[]) memberKeyItr.next();
                        dbPass = memberKeyObj[0].toString();
                        dbPassStatus = memberKeyObj[1] == null ? "" : memberKeyObj[1].toString();
                    }

                    memberName = member.getMemberName();
                    fatherName = member.getFatherName() == null ? "" : member.getFatherName();
                    motherName = member.getMotherName() == null ? "" : member.getMotherName();

                    dob = member.getDob();
                    gender = member.getGender() == null ? "" : member.getGender().trim();

                    mobileNo = member.getMobile() == null ? "" : member.getMobile();
                    phone1 = member.getPhone1() == null ? "" : member.getPhone1();
                    phone2 = member.getPhone2() == null ? "" : member.getPhone2();
                    memberEmail = member.getEmailId() == null ? "" : member.getEmailId();
                    bloodGroup = member.getBloodGroup() == null ? "" : member.getBloodGroup();

                    unitName = member.getUnitName() == null ? "" : member.getUnitName();
                    nirbachoniAson = member.getNirbachoniAson() == null ? "" : member.getNirbachoniAson();

                    Query mOrganizationSQL = dbsession.createQuery(" from MemberOrganization WHERE member_id='" + memId + "'");

                    logger.info("API :: Login API memberOrganizationSQL ::" + mOrganizationSQL);

                    if (!mOrganizationSQL.list().isEmpty()) {
                        for (Iterator mOrganizationItr = mOrganizationSQL.list().iterator(); mOrganizationItr.hasNext();) {
                            //   addressBook = (AddressBook) mAddressItr.next();

                            mOrganization = (MemberOrganization) mOrganizationItr.next();

                            organizationName = mOrganization.getOrganizationInfo().getOrganizationName() == null ? "" : mOrganization.getOrganizationInfo().getOrganizationName();
                            organizationLogo = mOrganization.getOrganizationInfo().getOrgLogo() == null ? "" : mOrganization.getOrganizationInfo().getOrgLogo();
                            organizationLogoLink = GlobalVariable.imageDirLink + "company/" + organizationLogo;
                        }
                    }

                    //  pictureName = member.getPictureName();
                    pictureName = member.getPictureName() == null ? "" : member.getPictureName();

                    pictureLink = GlobalVariable.imageMemberDirLink + pictureName;

                    //     logger.info("API :: Login API givenPassword ::" + encryption.getEncrypt(givenPassword));
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        Query mAccessTokenSQL = dbsession.createSQLQuery("SELECT access_token FROM member_accesstoken where member_id='" + memId + "'");

                        mAccessToken = mAccessTokenSQL.uniqueResult().toString();

                        mMemberTypeSQL = dbsession.createSQLQuery("SELECT mti.member_type_id,mti.member_type_name from member_type mt, member_type_info mti WHERE mt.member_type_id = mti.member_type_id AND mt.member_id='" + memId + "' AND SYSDATE() between mt.ed and mt.td");

                        logger.info("API :: Login API mMemberTypeSQL ::" + mMemberTypeSQL);
                        for (Iterator mMemberTypeItr = mMemberTypeSQL.list().iterator(); mMemberTypeItr.hasNext();) {

                            mMemberTypeObj = (Object[]) mMemberTypeItr.next();
                            mMemberTypeId = mMemberTypeObj[0].toString();
                            mMemberTypeName = mMemberTypeObj[1].toString();

                        }

                        //   Query mAddressSQL = dbsession.createSQLQuery(" from AddressBook WHERE id=(select address_id from member_address where member_id= " + memId + " and address_Type='M' ) ");
                        Query memberAddressSQL = dbsession.createQuery(" from MemberAddress WHERE member_id='" + memId + "'");

                        logger.info("API :: Login API memberAddressSQL ::" + memberAddressSQL);

                        if (!memberAddressSQL.list().isEmpty()) {
                            for (Iterator memberAddressItr = memberAddressSQL.list().iterator(); memberAddressItr.hasNext();) {
                                //   addressBook = (AddressBook) mAddressItr.next();

                                memberAddress = (MemberAddress) memberAddressItr.next();

                                memberAddressId = memberAddress.getId();
                                memberAddressType = memberAddress.getAddressType();
                                logger.info("API :: Login API memberAddressType ::" + memberAddressType);

                                if (memberAddressType.equals("M")) {
                                    mAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                                    mAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                                    mZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                                    //   mThanaId = memberAddress.getAddressBook().getThana().getId();
                                    //   mThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                                    //   mDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                                    //   mDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
//                                    mThanaId1 =   memberAddress.getAddressBook().getThana();
                                    //  mThanaId = memberAddress.getAddressBook().getThana().getId();
                                    //    mThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                                    //   mDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                                    //    mDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                                }

                                if (memberAddressType.equals("P")) {
                                    pAddressLine1 = memberAddress.getAddressBook().getAddress1() == null ? "" : memberAddress.getAddressBook().getAddress1();
                                    pAddressLine2 = memberAddress.getAddressBook().getAddress2() == null ? "" : memberAddress.getAddressBook().getAddress2();

                                    pZipCode = memberAddress.getAddressBook().getZipcode() == null ? "" : memberAddress.getAddressBook().getZipcode();

                                    //   pThanaId = memberAddress.getAddressBook().getThana().getId();
                                    //   pThanaName = memberAddress.getAddressBook().getThana().getThanaName();
                                    //   pDistrictId = memberAddress.getAddressBook().getThana().getDistrict().getId();
                                    //   pDistrictName = memberAddress.getAddressBook().getThana().getDistrict().getDistrictName();
                                }

                            }
                        }

                        mMemberOptionViewSQL = dbsession.createSQLQuery("SELECT phone1_view,phone2_view,mobile_view,email_view FROM member_view_setting WHERE member_id='" + memId + "'");

                        for (Iterator mMemberOptionViewItr = mMemberOptionViewSQL.list().iterator(); mMemberOptionViewItr.hasNext();) {

                            mMemberOptionViewObj = (Object[]) mMemberOptionViewItr.next();
                            memberXPhone1View = mMemberOptionViewObj[0].toString();
                            memberXPhone2View = mMemberOptionViewObj[1].toString();
                            memberXMobileView = mMemberOptionViewObj[2].toString();
                            memberXMobileView = mMemberOptionViewObj[3].toString();

                        }

                        mMemberAdminOptionViewSQL = dbsession.createSQLQuery("SELECT post_del,notice_del,blood_del,doctor_query_del FROM member_admin_setting WHERE member_id='" + memId + "'");

                        for (Iterator mMemberAdminOptionViewItr = mMemberAdminOptionViewSQL.list().iterator(); mMemberAdminOptionViewItr.hasNext();) {

                            mMemberAdminOptionViewObj = (Object[]) mMemberAdminOptionViewItr.next();
                            postDeletePermission = mMemberAdminOptionViewObj[0].toString();
                            noticeDeletePermission = mMemberAdminOptionViewObj[1].toString();
                            bloodDeletePermission = mMemberAdminOptionViewObj[2].toString();
                            doctorQueryDeletePermission = mMemberAdminOptionViewObj[3].toString();

                        }

//                        Query pAddressSQL = dbsession.createSQLQuery("SELECT * from address_book WHERE id=(select address_id from member_address where member_id= " + memId + " and address_Type='P' ) ");
//                        for (Iterator itr3 = pAddressSQL.list().iterator(); itr3.hasNext();) {
//
//                            pAddressObj = (Object[]) itr3.next();
//                            pAddressLine1 = pAddressObj[1].toString();
//                            pAddressLine2 = pAddressObj[2].toString();
//                            pThanaId = Integer.parseInt(pAddressObj[3].toString());
//
//                        }
                        memberTopicsSQL = dbsession.createSQLQuery("SELECT mt.id,t.topics_name,t.topics_details FROM member_topics mt,member_topics_info t WHERE mt.topics_id = t.id AND mt.member_id ='" + memId + "'");
                        if (!memberTopicsSQL.list().isEmpty()) {
                            for (Iterator memberTopicsItr = memberTopicsSQL.list().iterator(); memberTopicsItr.hasNext();) {

                                memberTopicsObj = (Object[]) memberTopicsItr.next();
                                memberTopicsId = memberTopicsObj[0].toString();
                                memberTopicsName = memberTopicsObj[1].toString();

                                memberTopicsResponseObj = new JSONObject();
                                memberTopicsResponseObj.put("TopicsId", memberTopicsId);
                                memberTopicsResponseObj.put("TopicsName", memberTopicsName);

                                memberTopicsResponseObjArray.add(memberTopicsResponseObj);

                            }
                        }

                        responseObj = new JSONObject();

                        responseObj.put("mAddressLine1", mAddressLine1);
                        responseObj.put("mAddressLine2", mAddressLine2);
                        responseObj.put("mThanaId", mThanaId);
                        responseObj.put("mThanaName", mThanaName);
                        responseObj.put("mDistrictId", mDistrictId);
                        responseObj.put("mDistrictName", mDistrictName);
                        responseObj.put("mCountry", mCountry);
                        responseObj.put("mZipCode", mZipCode);

                        responseObj.put("pAddressLine1", pAddressLine1);
                        responseObj.put("pAddressLine2", pAddressLine2);
                        responseObj.put("pThanaId", pThanaId);
                        responseObj.put("pThanaName", pThanaName);
                        responseObj.put("pDistrictId", pDistrictId);
                        responseObj.put("pDistrictName", pDistrictName);
                        responseObj.put("pCountry", pCountry);
                        responseObj.put("pZipCode", pZipCode);

                        logingObjArray.add(responseObj);

                        addressObj = new JSONObject();
                        addressObj.put("ResponseCode", "1");
                        addressObj.put("ResponseText", "Found");
                        addressObj.put("ResponseData", logingObjArray);
                        logger.info("Consumer Address of :" + memberId + " Found");

                        responseObj = new JSONObject();
                        responseObj.put("memId", memId);
                        responseObj.put("token", mAccessToken);

                        responseObj.put("memberTopics", memberTopicsResponseObjArray);

                        responseObj.put("Phone1View", memberXPhone1View);
                        responseObj.put("Phone2View", memberXPhone2View);
                        responseObj.put("MobileView", memberXMobileView);
                        responseObj.put("EmailView", memberXEmailView);

                        responseObj.put("postDeletePermission", postDeletePermission);
                        responseObj.put("noticeDeletePermission", noticeDeletePermission);
                        responseObj.put("bloodDeletePermission", bloodDeletePermission);
                        responseObj.put("doctorQueryDeletePermission", doctorQueryDeletePermission);

                        responseObj.put("organizationName", organizationName);
                        responseObj.put("organizationLogo", organizationLogoLink);
                        responseObj.put("unitName", unitName);
                        responseObj.put("nirbachoniAson", nirbachoniAson);
                        responseObj.put("ratingPoint", ratingPoint);
                        responseObj.put("likeCount", likeCount);
                        responseObj.put("shareCount", shareCount);
                        responseObj.put("uploadCount", uploadCount);
                        responseObj.put("bloodDonationCount", bloodDonationCount);

                        responseObj.put("forcePassStatus", dbPassStatus);
                        responseObj.put("memberLifeStatus", memberLifeStatus);
                        responseObj.put("memberLifeStatusText", memberLifeStatusText);
                        responseObj.put("memberTypeId", mMemberTypeId);
                        responseObj.put("memberTypeId", mMemberTypeId);
                        responseObj.put("memberTypeName", mMemberTypeName);
                        responseObj.put("memberId", memberId);
                        responseObj.put("memberName", memberName);
                        responseObj.put("memberEmail", memberEmail);
                        responseObj.put("memberMobile", mobileNo);
                        responseObj.put("memberPhone1", phone1);
                        responseObj.put("memberPhone2", phone2);
                        responseObj.put("memberGender", gender);
                        responseObj.put("memberBloodGroup", bloodGroup);
                        responseObj.put("memberPicture", pictureLink);
                        responseObj.put("address", addressObj);

                        responseObj.put("memberCenterId", memberCenterId);
                        responseObj.put("memberCenterName", memberCenterName);

                        responseObj.put("memberDivisionId", memberDivisionId);
                        responseObj.put("memberDivisionShortName", memberDivisionShortName);
                        responseObj.put("memberDivisionFullName", memberDivisionFullName);

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "1");
                        logingObj.put("ResponseText", "Found");
                        logingObj.put("ResponseData", responseObj);

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "Member password wrong");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("User ID :" + memberId + " Member password wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Member not found");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
