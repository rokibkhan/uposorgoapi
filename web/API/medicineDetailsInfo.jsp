<%-- 
    Document   : medicineDetailsInfo
    Created on : OCT 30, 2020, 11:34:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("medicineDetailsInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String mMedicineId = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("medicineId")) {
        key = request.getParameter("key").trim();

        //   memberId = request.getParameter("memberId").trim();
        //    givenPassword = request.getParameter("password").trim();
        mMedicineId = request.getParameter("medicineId").trim();
    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
//    String adjustLettter1 = "+88";
//    String adjustLettter2 = "+";
//    String memberId1 = "";
//    String firstLetter = memberId.substring(0, 1);
//
//    if (firstLetter.equals("0")) {
//        memberId1 = adjustLettter1 + memberId;
//    }
//    if (firstLetter.equals("8")) {
//        memberId1 = adjustLettter2 + memberId;
//    }
//    if (firstLetter.equals("+")) {
//        memberId1 = memberId;
//    }

    Member member = null;
    MemberOrganization mOrganization = null;
    MemberAddress memberAddress = null;
    AddressBook addressBook = null;

    int memId = 0;
    String memberName = "";
    String dbPass = "";
    String memberEmail = "";
    String memberTypeId = "";
    String memberType = "";

    String memberLifeStatus = "";
    String memberLifeStatusText = "";

    String fatherName = "";
    String motherName = "";
    String placeOfBirth = "";
    java.util.Date dob = new Date();
    String gender = "";
    String mobileNo = "";
    String phone1 = "";
    String phone2 = "";
    String bloodGroup = "";
    String pictureName = "";
    String pictureLink = "";

    int memberCenterId = 0;
    String memberCenterName = "";

    int memberDivisionId = 0;
    String memberDivisionShortName = "";
    String memberDivisionFullName = "";

    int memberAddressId = 0;
    String memberAddressType = "";
    String memberAddressLine1 = "";
    String memberAddressLine2 = "";
    int memberThanaId = 0;
    String memberThanaName = "";
    int memberDistrictId = 0;
    String memberDistrictName = "";
    String memberCountry = "Bangladesh";
    String memberZipCode = "";

    String mAddressLine1 = "";
    String mAddressLine2 = "";
    int mThanaId = 0;
    String mThanaName = "";
    int mDistrictId = 0;
    String mDistrictName = "";
    String mCountry = "Bangladesh";
    String mZipCode = "";

    String pAddressLine1 = "";
    String pAddressLine2 = "";
    int pThanaId = 0;
    String pThanaName = "";
    int pDistrictId = 0;
    String pDistrictName = "";
    String pCountry = "Bangladesh";
    String pZipCode = "";

    int memberEducationId = 0;

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: medicineDetailsInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();

    Query mMemberOptionViewSQL = null;
    Object[] mMemberOptionViewObj = null;

    Query qMember = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    //int total_pages = 0;
    String memberSQL = null;
    String memberCountSQL = null;
    Query memberSQLQry = null;
    Object[] memberObj = null;

    String memberXId = "";
    String medicineName = "";
    String medicineGeneticName = "";
    String medicineIndications = "";
    String uses = "";
    String composition = "";
    String dosageChild = "";
    String dosageAdult = "";
    String dosageRenal = "";
    String dosageAdministration = "";
    String contraindication = "";

    String sideEffects = "";

    String precaution = "";
    String uses_in_ptegnancy = "";

    String uses_in_paediatric = "";
    String therapeutic_class = "";
    String mode_of_action = "";
    String interaction = "";

    String storage_condition = "";

    String package_size_price = "";

    String preparation = "";
    String attachment = "";
    String feature_image = "";
    String featureImageLink = "";
    String medicineStructureShortName = "";
    String medicineStructureName = "";
    String medicineStructureMG_ML = "";
    String typeShortName = "";
    String typeName = "";
    String typeDetails = "";

    String companyShortName = "";
    String companyName = "";
    String companyBrandName = "";
    String companyLogo = "";
    String companyLogoLink = "";
    String companyWeb = "";
    String companyAddress = "";

    JSONObject medicineContentObj = new JSONObject();
    JSONArray medicineContentObjArr = new JSONArray();

    JSONObject sideEffectsObj = new JSONObject();
    JSONArray sideEffectsObjArr = new JSONArray();

//    'organizationName' => $organizationName,
//                'orgLogo' => $orgLogo,
//                'unitName' => $posting,
//                'nirbachoniAson' => $nirbachoni_ason,
//                'ratingPoint' => $useRating,
//                'shareCount' => $shareContentPoint,
//                'uploadCount' => $uploadContentPoint,
//                'bloodDonationCount' => $bloodDonationCounter,
    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            //    qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");
            //    if (!qMember.list().isEmpty()) {
            //       Query memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");
            //         dbPass = memberKeySQL.uniqueResult().toString();
            //   if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {
            memberSQL = "SELECT m.id, m.medicine_name,m.medicine_genetic_name, "
                    + "m.description,m.uses,m.composition,m.dosage_adult,   "
                    + " m.dosage_child,m.dosage_renal,m.dosage_administration,m.contraindication,"
                    + "m.side_effects,m.precaution,m.uses_in_ptegnancy,"
                    + "m.uses_in_paediatric,m.therapeutic_class,m.mode_of_action,m.interaction, "
                    + "m.storage_condition,m.package_size_price,m.preparation,m.attachment,"
                    + "m.feature_image,"
                    + "ms.SHORT_NAME,ms.NAME,ms.MG_ML,"
                    + "mt.SHORT_NAME typeShortName,mt.NAME typeName,mt.details  typeDetails,   "
                    + "mc.COMPANY_SHORT_NAME companyShortName,mc.COMPANY_NAME companyName,mc.COMPANY_BRAND_NAME  companyBrandName,mc.LOGO companyLogo,mc.WEB companyWeb, mc.ADDRESS companyAddress  "
                    + "FROM  medicine_info m,medicine_company mc,medicine_structure ms,medicine_type mt "
                    + "WHERE  m.published = 1 AND m.id = '" + mMedicineId + "' "
                    + "AND m.company = mc.ID  "
                    + "AND m.structure = ms.ID "
                    + "AND m.medicine_type = mt.ID ";

            logger.info("API :: medicineDetailsInfo API memberSQL ::" + memberSQL);

            memberSQLQry = dbsession.createSQLQuery(memberSQL);

            for (Iterator memberItr = memberSQLQry.list().iterator(); memberItr.hasNext();) {

                memberObj = (Object[]) memberItr.next();
                memberXId = memberObj[0].toString();
                medicineName = memberObj[1] == null ? "" : memberObj[1].toString();
                medicineGeneticName = memberObj[2] == null ? "" : memberObj[2].toString();

                medicineIndications = memberObj[3] == null ? "" : memberObj[3].toString();
                if (!medicineIndications.equals("")) {
                    memberContentObj = new JSONObject();
                    memberContentObj.put("Title", "Indications");
                    memberContentObj.put("Desc", medicineIndications);
                    memberContentObjArr.add(memberContentObj);
                }

                //   memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;
                //     logger.info("API :: medicineDetailsInfo API pictureLink ::" + memberXPictureLink); 
                //   uses = memberObj[4] == null ? "" : memberObj[4].toString();                
                //    memberContentObj = new JSONObject();
                //    memberContentObj.put("Title", "Uses");
                //     memberContentObj.put("Desc", uses);
                //     memberContentObjArr.add(memberContentObj);
                composition = memberObj[5] == null ? "" : memberObj[5].toString();
                if (!composition.equals("")) {
                    memberContentObj = new JSONObject();
                    memberContentObj.put("Title", "Composition");
                    memberContentObj.put("Desc", composition);
                    memberContentObjArr.add(memberContentObj);

                }

                dosageAdult = memberObj[6] == null ? "" : memberObj[6].toString();
                if (!dosageAdult.equals("")) {
                    memberContentObj = new JSONObject();
                    memberContentObj.put("Title", "Dosage Adult");
                    memberContentObj.put("Desc", dosageChild);
                    memberContentObjArr.add(memberContentObj);
                }

                dosageRenal = memberObj[7] == null ? "" : memberObj[7].toString();
                if (!dosageRenal.equals("")) {
                    memberContentObj = new JSONObject();
                    memberContentObj.put("Title", "Dosage Renal");
                    memberContentObj.put("Desc", dosageRenal);
                    memberContentObjArr.add(memberContentObj);
                }

                dosageChild = memberObj[8] == null ? "" : memberObj[8].toString();
                if (!dosageChild.equals("")) {
                    memberContentObj = new JSONObject();
                    memberContentObj.put("Title", "Dosage Child");
                    memberContentObj.put("Desc", dosageAdult);
                    memberContentObjArr.add(memberContentObj);

                }

                dosageAdministration = memberObj[9] == null ? "" : memberObj[9].toString();
                if (!dosageAdministration.equals("")) {
                    memberContentObj = new JSONObject();
                    memberContentObj.put("Title", "Dosage And Administration");
                    memberContentObj.put("Desc", dosageAdministration);
                    memberContentObjArr.add(memberContentObj);
                }

                contraindication = memberObj[10] == null ? "" : memberObj[10].toString();

                sideEffects = memberObj[11] == null ? "" : memberObj[11].toString();
                precaution = memberObj[12] == null ? "" : memberObj[12].toString();
                uses_in_ptegnancy = memberObj[13] == null ? "" : memberObj[13].toString();

                uses_in_paediatric = memberObj[14] == null ? "" : memberObj[14].toString();
                therapeutic_class = memberObj[15] == null ? "" : memberObj[15].toString();
                mode_of_action = memberObj[16] == null ? "" : memberObj[16].toString();
                interaction = memberObj[17] == null ? "" : memberObj[17].toString();

                storage_condition = memberObj[18] == null ? "" : memberObj[18].toString();
                package_size_price = memberObj[19] == null ? "" : memberObj[19].toString();
                preparation = memberObj[20] == null ? "" : memberObj[20].toString();
                attachment = memberObj[21] == null ? "" : memberObj[21].toString();

                feature_image = memberObj[22] == null ? "" : memberObj[22].toString();

                featureImageLink = "";

                medicineStructureShortName = memberObj[23] == null ? "" : memberObj[23].toString();
                medicineStructureName = memberObj[24] == null ? "" : memberObj[24].toString();
                medicineStructureMG_ML = memberObj[25] == null ? "" : memberObj[25].toString();

                typeShortName = memberObj[26] == null ? "" : memberObj[26].toString();
                typeName = memberObj[27] == null ? "" : memberObj[27].toString();
                typeDetails = memberObj[28] == null ? "" : memberObj[28].toString();

                companyShortName = memberObj[29] == null ? "" : memberObj[29].toString();
                companyName = memberObj[30] == null ? "" : memberObj[30].toString();
                companyBrandName = memberObj[31] == null ? "" : memberObj[31].toString();
                companyLogo = memberObj[32] == null ? "" : memberObj[32].toString();
                companyWeb = memberObj[33] == null ? "" : memberObj[33].toString();
                companyAddress = memberObj[34] == null ? "" : memberObj[34].toString();

                memberContentObj = new JSONObject();
                memberContentObj.put("Title", "Contraindication");
                memberContentObj.put("Desc", contraindication);
                memberContentObjArr.add(memberContentObj);

                memberContentObj = new JSONObject();
                memberContentObj.put("Title", "Side Effects");
                memberContentObj.put("Desc", sideEffects);
                memberContentObjArr.add(memberContentObj);

                memberContentObj = new JSONObject();
                memberContentObj.put("Title", "Precaution");
                memberContentObj.put("Desc", precaution);
                memberContentObjArr.add(memberContentObj);

                memberContentObj = new JSONObject();
                memberContentObj.put("Title", "Uses In Pregnancy");
                memberContentObj.put("Desc", uses_in_ptegnancy);
                memberContentObjArr.add(memberContentObj);

                memberContentObj = new JSONObject();
                memberContentObj.put("Title", "Uses In Paediatric");
                memberContentObj.put("Desc", uses_in_paediatric);
                memberContentObjArr.add(memberContentObj);

                /*
                
                memberContentObj = new JSONObject();
                memberContentObj.put("Id", memberXId);

                memberContentObj.put("MedicineName", medicineName);
                memberContentObj.put("MedicineGeneticName", medicineGeneticName);
                memberContentObj.put("Description", description);
                memberContentObj.put("Uses", uses);
                memberContentObj.put("Composition", composition);
                memberContentObj.put("DosageAdult", dosageAdult);
                memberContentObj.put("DosageChild", dosageChild);
                memberContentObj.put("DosageRenal", dosageRenal);
                memberContentObj.put("DosageAdministration", dosageAdministration);
                memberContentObj.put("Contraindication", contraindication);

                memberContentObj.put("SideEffects", sideEffects);
                memberContentObj.put("Precaution", precaution);
                memberContentObj.put("UsesInPregnancy", uses_in_ptegnancy);
                memberContentObj.put("UsesInPaediatric", uses_in_paediatric);
                
                
                
                
                memberContentObj.put("TherapeuticClass", therapeutic_class);

                memberContentObj.put("ModeOfAction", mode_of_action);
                memberContentObj.put("Interaction", interaction);
                memberContentObj.put("StorageCondition", storage_condition);
                memberContentObj.put("PackageSizePrice", package_size_price);
                memberContentObj.put("Preparation", preparation);
                memberContentObj.put("Attachment", attachment);

                memberContentObj.put("FeatureImage", feature_image);
                memberContentObj.put("FeatureImageLink", featureImageLink);

                memberContentObj.put("StructureShortName", medicineStructureShortName);
                memberContentObj.put("StructureName", medicineStructureName);
                memberContentObj.put("StructureMG_ML", medicineStructureMG_ML);

                memberContentObj.put("TypeShortName", typeShortName);
                memberContentObj.put("TypeName", typeName);
                memberContentObj.put("TypeDetails", typeDetails);

                memberContentObj.put("CompanyShortName", companyShortName);
                memberContentObj.put("CompanyName", companyName);
                memberContentObj.put("CompanyBrandName", companyBrandName);
                memberContentObj.put("CompanyLogo", companyLogo);
                memberContentObj.put("CompanyLogoLink", companyLogoLink);
                memberContentObj.put("CompanyWeb", companyWeb);
                memberContentObj.put("CompanyAddress", companyAddress);

             //   memberContentObj = new JSONObject();
            //    memberContentObj.put("Title", memberXId);

                memberContentObjArr.add(memberContentObj);

                 */
            }

            logingObj = new JSONObject();
            logingObj.put("ResponseCode", "1");
            logingObj.put("ResponseText", "Found");
            logingObj.put("ResponseData", memberContentObjArr);

            logingObj.put("Id", memberXId);

            logingObj.put("MedicineName", medicineName);
            logingObj.put("MedicineGeneticName", medicineGeneticName);

            logingObj.put("TherapeuticClass", therapeutic_class);

            logingObj.put("ModeOfAction", mode_of_action);
            logingObj.put("Interaction", interaction);
            logingObj.put("StorageCondition", storage_condition);
            logingObj.put("PackageSizePrice", package_size_price);
            logingObj.put("Preparation", preparation);
            logingObj.put("Attachment", attachment);

            logingObj.put("FeatureImage", feature_image);
            logingObj.put("FeatureImageLink", featureImageLink);

            logingObj.put("StructureShortName", medicineStructureShortName);
            logingObj.put("StructureName", medicineStructureName);
            logingObj.put("StructureMG_ML", medicineStructureMG_ML);

            logingObj.put("TypeShortName", typeShortName);
            logingObj.put("TypeName", typeName);
            logingObj.put("TypeDetails", typeDetails);

            logingObj.put("CompanyShortName", companyShortName);
            logingObj.put("CompanyName", companyName);
            logingObj.put("CompanyBrandName", companyBrandName);
            logingObj.put("CompanyLogo", companyLogo);
            logingObj.put("CompanyLogoLink", companyLogoLink);
            logingObj.put("CompanyWeb", companyWeb);
            logingObj.put("CompanyAddress", "companyAddress");

//                    } else {
//
//                        logingObj = new JSONObject();
//                        logingObj.put("ResponseCode", "2");
//                        logingObj.put("ResponseText", "Member password wrong");
//                        logingObj.put("ResponseData", logingObjArray);
//                        logger.info("User ID :" + memberId + " Member password wrong");
//
//                    }
//            } else {
//                logingObj = new JSONObject();
//                logingObj.put("ResponseCode", "0");
//                logingObj.put("ResponseText", "NotFound");
//                logingObj.put("ResponseData", logingObjArray);
//                logger.info("User ID :" + memberId + " Not Found");
//
//            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
