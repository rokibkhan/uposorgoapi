<%-- 
    Document   : memberUpdatePublicationInfo
    Created on : June 19, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    //  ALTER TABLE `joybandb`.`member_address` 
//CHANGE COLUMN `address_type` `address_type` VARCHAR(1) NOT NULL DEFAULT 'P' COMMENT 'P->Permanent Address:M->Mailing Address' ;
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    Logger logger = Logger.getLogger("memberUpdatePublicationInfo_jsp.class");

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String memberPublicationId = "";
    String title = "";
    String author = "";
    String journalConference = "";
    String journalConferenceName = "";
    String volume = "";
    String pageNumber = "";
    String year = "";
    String month = "";
    String publisher = "";
    String url = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("memberPublicationId")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        memberPublicationId = request.getParameter("memberPublicationId").trim();

        title = request.getParameter("title") == null ? "" : request.getParameter("title").trim();
        author = request.getParameter("author") == null ? "" : request.getParameter("author").trim();
        journalConference = request.getParameter("journalConference") == null ? "" : request.getParameter("journalConference").trim();
        journalConferenceName = request.getParameter("journalConferenceName") == null ? "" : request.getParameter("journalConferenceName").trim();
        volume = request.getParameter("volume") == null ? "" : request.getParameter("volume").trim();
        pageNumber = request.getParameter("pageNumber") == null ? "" : request.getParameter("pageNumber").trim();
        year = request.getParameter("year") == null ? "" : request.getParameter("year").trim();
        month = request.getParameter("month") == null ? "" : request.getParameter("month").trim();
        publisher = request.getParameter("publisher") == null ? "" : request.getParameter("publisher").trim();
        url = request.getParameter("url") == null ? "" : request.getParameter("url").trim();

        //   memberName = request.getParameter("name").trim();
    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);
    
    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberUpdatePublicationInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    adddate = dateFormatX.format(dateToday);

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        if (!memberPublicationId.equals("0")) {
                            //Update member Address

                            //update member Rating for comment_content_point filed
                            Query updateMemberPublicationInfoSQL = dbsession.createSQLQuery("UPDATE  member_publication_info SET "
                                    + "publication_title = '" + title + "',"
                                    + "publication_author = '" + author + "',"
                                    + "publication_journal_conference = '" + journalConference + "',"
                                    + "publication_journal_conference_name='" + journalConferenceName + "', "
                                    + "publication_volume='" + volume + "',"
                                    + "publication_page='" + pageNumber + "',"
                                    + "publication_year='" + year + "',"
                                    + "publication_month='" + month + "',"
                                    + "publication_publisher='" + publisher + "',"
                                    + "publication_url='" + url + "'"
                                    + "WHERE id = '" + memberPublicationId + "'");

                            updateMemberPublicationInfoSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                postContentObj = new JSONObject();
                                //  postContentObj.put("Id", memId);

                                //     postContentObj.put("PictureName", memberPictureName);
                                //    postContentObj.put("PictureLink", memberPictureNameLink);
                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member Publication info updated successfully");
                                logingObj.put("ResponseData", postContentObj);

                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member Publication updated ");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member Publication updated");
                            }

                        } else {
                            //Insert member Publication

                            String idMA = getId.getID(41);
                            int regMemberPublicationId = Integer.parseInt(idMA);

                            Query mPublicationAddSQL = dbsession.createSQLQuery("INSERT INTO member_publication_info("
                                    + "id,"
                                    + "member_id,"
                                    + "publication_title,"
                                    + "publication_author,"
                                    + "publication_journal_conference,"
                                    + "publication_journal_conference_name,"
                                    + "publication_volume,"
                                    + "publication_page,"
                                    + "publication_year,"
                                    + "publication_month,"
                                    + "publication_publisher,"
                                    + "publication_url"
                                    + ") VALUES("
                                    + "" + regMemberPublicationId + ","
                                    + "'" + memId + "',"
                                    + "'" + title + "',"
                                    + "'" + author + "',"
                                    + "'" + journalConference + "',"
                                    + "'" + journalConferenceName + "',"
                                    + "'" + volume + "',"
                                    + "'" + pageNumber + "',"
                                    + "'" + year + "',"
                                    + "'" + month + "',"
                                    + "'" + publisher + "',"
                                    + "'" + url + "'"
                                    + "  ) ");

                            logger.info("mPublicationAddSQL ::" + mPublicationAddSQL);
                            System.out.println("mPublicationAddSQL ::" + mPublicationAddSQL);

                            mPublicationAddSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member Publication info Added Successfully");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("User ID :" + memberId + " Member Publication info Added Successfully");
                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member Publication info Add");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member Publication info Add");
                            }
                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "ERROR!!! Member Password info wrong ");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("ERROR!!! Member Password info wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
