<%-- 
    Document   : doctorAppointmentScheduleCreation
    Created on : OCT 12, 2020, 1:05:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    DateFormat parseFormat_YMDHMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat parseFormat_YMDAMPM = new SimpleDateFormat("yyyy-MM-dd hh:mma");
    DateFormat displayFormat_YMD_AM_PM = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
    DateFormat displayFormat_H_M_AM_PM = new SimpleDateFormat("hh:mma");
    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    // Logger logger = Logger.getLogger("patientQueryRequest_jsp.class");
    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    getRegistryID getId = new getRegistryID();
    SendFirebasePushNotification fcmsend = new SendFirebasePushNotification();

    Logger logger = Logger.getLogger("doctorAppointmentScheduleCreation_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());
    DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
    Date dateFileToday = new Date();
    String filePrefixToday = formatterFileToday.format(dateFileToday);

    // String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";
    //  String appointmentNo = filePrefixToday + "_" + rand1 + "_" + rand2;
    String appointmentNo = filePrefixToday + "_" + rand1;

    String key = "";
    String memberId = "";
    String givenPassword = "";
    String doctorCategoryId = "";

    String doctorId = "";
    String appointmentDate = "";
    String appointmentSlotId = "";
    String patientId = "";
    String appointmentStartingDateTime = "";
    String timeText = "";
    String timeHour = "";
    String timeMinute = "";
    String perPratientTime = "";
    String startTime = "";
    String endTime = "";

    String statDateTime = "";
    Date startingDateTimeM = null;
    String endDateTime = "";
    Date endingDateTimeM = null;

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId")) {
    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

        //  memberId = request.getParameter("memberId").trim();
        //   givenPassword = request.getParameter("password").trim();
        doctorId = request.getParameter("doctorId").trim();
        appointmentDate = request.getParameter("appointmentDate").trim();

        //    timeText = request.getParameter("timeText").trim();
        //    timeHour = request.getParameter("timeHour").trim();
        //    timeMinute = request.getParameter("timeMinute").trim();
        perPratientTime = request.getParameter("perPratientTime").trim();
        startTime = request.getParameter("startTime").trim();
        endTime = request.getParameter("endTime").trim();

        timeText = startTime + "-" + endTime;

        //  patientId = "1183";
        System.out.println("doctorId :: " + doctorId);
        System.out.println("appointmentDate :: " + appointmentDate);
        System.out.println("timeText :: " + timeText);
        System.out.println("timeMinute :: " + timeMinute);
        System.out.println("perPratientTime :: " + perPratientTime);
        System.out.println("startTime :: " + startTime);
        System.out.println("endTime :: " + endTime);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    Query memberSQL = null;
    Object memberObj[] = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    Query patientAppointmentInfoAddSQL = null;
    Query patientCovidInfoAddSQL = null;

    Query appointmentInfoCheckSQL = null;

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: doctorAppointmentScheduleCreation API rec :: " + rec);

    String memMemberName = "";
    String memberDeviceId = "";

    statDateTime = appointmentDate + " " + startTime;
    endDateTime = appointmentDate + " " + endTime;
    System.out.println("statDateTime :: " + statDateTime);
    System.out.println("endDateTime :: " + endDateTime);

    startingDateTimeM = parseFormat_YMDAMPM.parse(statDateTime);
    String newStartingDateTimeM = dateFormatX.format(startingDateTimeM);
    System.out.println("newStartingDateTimeM :: " + newStartingDateTimeM);

    endingDateTimeM = parseFormat_YMDAMPM.parse(endDateTime);
    String newEndingDateTimeM = dateFormatX.format(endingDateTimeM);
    System.out.println("newEndingDateTimeM :: " + newEndingDateTimeM);

    // Calucalte time difference 
    // in milliseconds 
    //   long difference_In_Time = startingDateTimeM.getTime() - endingDateTimeM.getTime();
    //   long difference_In_Minutes = (difference_In_Time / (1000 * 60)) % 60;
    //    System.out.println("difference_In_Minutes :: " + difference_In_Minutes);
    long result_In_Minutes = Math.round((endingDateTimeM.getTime() / 60000) - (startingDateTimeM.getTime() / 60000));
    System.out.println("result_In_Minutes :: " + result_In_Minutes);

    long result_In_Hour = ((endingDateTimeM.getTime() / 3600000) - (startingDateTimeM.getTime() / 3600000));
    System.out.println("result_In_Hour :: " + result_In_Hour);

    long result_In_Hour1 = (result_In_Minutes / 60);
    System.out.println("result_In_Hour1 :: " + result_In_Hour1);
    //1000 * 60 * 60
    //  startingDateTimeM
    //   parseFormat_YMDAMPM
    if (rec == 1) {

        try {

            //check appointment slot free 
            appointmentInfoCheckSQL = dbsession.createSQLQuery("SELECT * from doctor_aponmnt_info where doctor_id ='" + doctorId + "' AND aponmnt_date = '" + appointmentDate + "' AND aponmntstart_time='" + newStartingDateTimeM + "'");

            if (appointmentInfoCheckSQL.list().isEmpty()) {

                System.out.println("appointmentInfoCreate for :: " + doctorId);

                String idAappS = getId.getID(79); //Doctor_Appointment_Info_ID
                int pAppointmentId = Integer.parseInt(idAappS);

                InetAddress ip;
                String hostname = "";
                String ipAddress = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostName();
                    ipAddress = ip.getHostAddress();

                } catch (UnknownHostException e) {

                    e.printStackTrace();
                }

                String adduser = doctorId;
                String adddate = dateFormatX.format(dateToday);
                String addterm = hostname;
                String published = "1";

                String patientDetailsForNotification = "Test Patent";

                Query memberDeviceIdSQL = dbsession.createSQLQuery("SELECT device_id FROM member_deviceid where member_id='" + doctorId + "'");
                memberDeviceId = memberDeviceIdSQL.uniqueResult() == null ? "" : memberDeviceIdSQL.uniqueResult().toString();

                System.out.println("memberDeviceId :: " + memberDeviceId);

                //    String timeHour1 = timeHour + " Hour";
                //    String timeMinute1 = timeMinute + " Minute";
                String timeHour1 = result_In_Hour + " Hour";
                String timeMinute1 = result_In_Minutes + " Minute";

                System.out.println("timeHour1 :: " + timeHour1);
                System.out.println("timeMinute1 :: " + timeMinute1);

                //   Double totalSerialCount = Double.parseDouble(timeMinute) / Double.parseDouble(perPratientTime);
                Double totalSerialCount = result_In_Minutes / Double.parseDouble(perPratientTime);

                System.out.println("totalSerialCount :: " + totalSerialCount);

                Long totalSerialCount1 = Math.round(totalSerialCount);

                System.out.println("totalSerialCount1 :: " + totalSerialCount1);

                patientAppointmentInfoAddSQL = dbsession.createSQLQuery("INSERT INTO doctor_aponmnt_info("
                        + "id,"
                        + "doctor_id,"
                        + "aponmnt_date,"
                        + "aponmntstart_time,"
                        + "aponmntend_time,"
                        + "short_name,"
                        + "name,"
                        + "time_text,"
                        + "time_hour,"
                        + "time_minute,"
                        + "time_number,"
                        + "patient_time,"
                        + "max_serial_count,"
                        + "add_user,"
                        + "add_date,"
                        + "add_term,"
                        + "add_ip"
                        + ") VALUES("
                        + "'" + pAppointmentId + "',"
                        + "'" + doctorId + "',"
                        + "'" + appointmentDate + "',"
                        + "'" + newStartingDateTimeM + "',"
                        + "'" + newEndingDateTimeM + "',"
                        + "'" + timeText + "',"
                        + "'" + timeText + "',"
                        + "'" + timeText + "',"
                        + "'" + timeHour1 + "',"
                        + "'" + timeMinute1 + "',"
                        + "'" + result_In_Minutes + "',"
                        + "'" + perPratientTime + "',"
                        + "'" + totalSerialCount1 + "',"
                        + "'" + adduser + "',"
                        + "'" + adddate + "',"
                        + "'" + hostname + "',"
                        + "'" + ipAddress + "'"
                        + "  ) ");

                System.out.println("patientAppointmentInfoAddSQL :: " + patientAppointmentInfoAddSQL);
                logger.info("patientAppointmentInfoAddSQL ::" + patientAppointmentInfoAddSQL);
                patientAppointmentInfoAddSQL.executeUpdate();

                //    Query updateDoctorTimeSlotStatusSQL = dbsession.createSQLQuery("UPDATE  doctor_aponmnt_slot SET status = '1', mod_date='" + adddate + "'  WHERE id = '" + appointmentSlotId + "'");
                //     updateDoctorTimeSlotStatusSQL.executeUpdate();
                dbtrx.commit();

                if (dbtrx.wasCommitted()) {

                    String msgPriority = "high"; // normal
                    //       appointmentTime = scheduleSlotStartTime;

                    //Notification for doctor
//                    if (!doctorDeviceId.equals("")) {
//
//                        String msgTitle = memMemberName + " appointment details";
//                        String msgBody = "Appointment Time :" + appointmentTime + " ," + patientDetailsForNotification;
//                        String receipentDeviceId = doctorDeviceId;
//                        //     String receipentDeviceId = "cGfWg1IlE1o:APA91bHKfVtMGSlAlLeufYlCuv2ZGlH1RAjY3ssnT8WNVJIJ0Hpvkav1VeHxQ-1WlYcVId09B2ubUDPL7lhzB7GXgU-gwtzZ96MpCwCUuTUHKofNQar32Ypb-mejf4xIPV6p66NsRMAY";
//                        System.out.println("receipentDeviceId :: doctorDeviceId ::" + receipentDeviceId);
//
//                        String fcmsendDoctor = fcmsend.sendSingleNotification(msgTitle, msgBody, receipentDeviceId, msgPriority);
//                        logger.info("fcmsendDoctor" + fcmsendDoctor);
//                        System.out.println("fcmsendDoctor::" + fcmsendDoctor);
//
//                    }
                    memberContentObj = new JSONObject();

                    memberContentObj.put("appointmentId", pAppointmentId);
                    memberContentObj.put("appointmentDate", appointmentDate);

                    memberContentObj.put("startingDateTime", newStartingDateTimeM);
                    memberContentObj.put("endingDateTime", newEndingDateTimeM);
                    memberContentObj.put("timeText", timeText);
                    memberContentObj.put("timeHour", timeHour1);
                    memberContentObj.put("timeMinute", timeMinute1);
                    memberContentObj.put("perPratientTime", perPratientTime);
                    memberContentObj.put("totalSerialCount", totalSerialCount1);

                    memberContentObjArr.add(memberContentObj);

                    responseDataObj = new JSONObject();
                    responseDataObj.put("ResponseCode", "1");
                    responseDataObj.put("ResponseText", "Appointment info added successfully");
                    responseDataObj.put("ResponseData", memberContentObjArr);
                } else {
                    responseDataObj = new JSONObject();
                    responseDataObj.put("ResponseCode", "0");
                    responseDataObj.put("ResponseText", "ERROR!!! When Appointment Info Add");
                    responseDataObj.put("ResponseData", memberContentObjArr);
                    logger.info("ERROR!!! When Appointment Info Add");
                }

               // responseDataObj = new JSONObject();
              //  responseDataObj.put("ResponseCode", "1");
              //  responseDataObj.put("ResponseText", "Found");
              //  responseDataObj.put("ResponseData", memberContentObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "Appointment Already Created");
                responseDataObj.put("ResponseData", memberContentObjArr);
            }

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>