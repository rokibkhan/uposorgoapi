<%-- 
    Document   : getBloodGroupInfo
    Created on : OCT 28, 2019, 1:32:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("getBloodGroupInfo_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }

    Query districtSQL = null;
    Object districtObj[] = null;
    String districtId = "";
    String districtName = "";

    JSONArray bloodGroupObjArr = new JSONArray();
    JSONObject bloodGroupObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;
    logger.info("JoyBangla :: API :: getBloodGroupInfo rec :: " + rec);
    if (rec == 1) {

        try {

            JSONObject objAp = new JSONObject();
            objAp.put("bloodGroupId", "A+");
            objAp.put("bloodGroupName", "A+");
            bloodGroupObjArr.add(objAp);

            JSONObject objAn = new JSONObject();
            objAn.put("bloodGroupId", "A-");
            objAn.put("bloodGroupName", "A-");
            bloodGroupObjArr.add(objAn);

            JSONObject objBp = new JSONObject();
            objBp.put("bloodGroupId", "B+");
            objBp.put("bloodGroupName", "B+");
            bloodGroupObjArr.add(objBp);

            JSONObject objBn = new JSONObject();
            objBn.put("bloodGroupId", "B-");
            objBn.put("bloodGroupName", "B-");
            bloodGroupObjArr.add(objBn);

            JSONObject objABp = new JSONObject();
            objABp.put("bloodGroupId", "AB+");
            objABp.put("bloodGroupName", "AB+");
            bloodGroupObjArr.add(objABp);

            JSONObject objABn = new JSONObject();
            objABn.put("bloodGroupId", "AB-");
            objABn.put("bloodGroupName", "AB-");
            bloodGroupObjArr.add(objABn);

            JSONObject objOp = new JSONObject();
            objOp.put("bloodGroupId", "0+");
            objOp.put("bloodGroupName", "0+");
            bloodGroupObjArr.add(objOp);

            JSONObject objOn = new JSONObject();
            objOn.put("bloodGroupId", "0-");
            objOn.put("bloodGroupName", "0-");
            bloodGroupObjArr.add(objOn);

            responseDataObj = new JSONObject();
            responseDataObj.put("ResponseCode", "1");
            responseDataObj.put("ResponseText", "Found");
            responseDataObj.put("ResponseData", bloodGroupObjArr);

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", bloodGroupObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>