<%-- 
    Document   : doctorAppointmentScheduleSlotCreation
    Created on : OCT 12, 2020, 1:05:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    DateFormat parseFormat_YMDHMS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat displayFormat_YMD_AM_PM = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
    DateFormat displayFormat_H_M_AM_PM = new SimpleDateFormat("hh:mma");
    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    // Logger logger = Logger.getLogger("patientQueryRequest_jsp.class");
    SessionImpl sessionImpl = (SessionImpl) dbsession;
    Connection con = sessionImpl.connection();

    getRegistryID getId = new getRegistryID();
    SendFirebasePushNotification fcmsend = new SendFirebasePushNotification();

    Logger logger = Logger.getLogger("doctorAppointmentScheduleSlotCreation_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;

    Random random = new Random();
    int rand1 = Math.abs(random.nextInt());
    int rand2 = Math.abs(random.nextInt());
    DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
    Date dateFileToday = new Date();
    String filePrefixToday = formatterFileToday.format(dateFileToday);

    // String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";
    //  String appointmentNo = filePrefixToday + "_" + rand1 + "_" + rand2;
    String appointmentNo = filePrefixToday + "_" + rand1;

    String key = "";
    String memberId = "";
    String givenPassword = "";
    String doctorCategoryId = "";

    String doctorId = "";
    String appointmentId = "";
    String appointmentDate = "";
    String appointmentSlotId = "";
    String patientId = "";
    String appointmentStartingDateTime = "";
    String timeText = "";
    String timeHour = "";
    String timeMinute = "";
    String perPratientTime = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId")) {
    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

        //  memberId = request.getParameter("memberId").trim();
        //   givenPassword = request.getParameter("password").trim();
        appointmentId = request.getParameter("appointmentId").trim();

        //appointmentDate = request.getParameter("appointmentDate").trim();
        //   appointmentStartingDateTime = request.getParameter("appointmentStartingDateTime").trim();
        //   timeText = request.getParameter("timeText").trim();
        //   timeHour = request.getParameter("timeHour").trim();
        //    timeMinute = request.getParameter("timeMinute").trim();
        //    perPratientTime = request.getParameter("perPratientTime").trim();
        //  patientId = "1183";
        System.out.println("appointmentId :: " + appointmentId);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    Query memberSQL = null;
    Object memberObj[] = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    Query patientAppointmentInfoAddSQL = null;
    Query patientCovidInfoAddSQL = null;

    Query appointmentInfoCheckSQL = null;

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: doctorAppointmentScheduleSlotCreation API rec :: " + rec);

    String memMemberName = "";
    String appointmentIdXn = "";

    if (rec == 1) {

        try {

            //check appointment slot free 
            appointmentInfoCheckSQL = dbsession.createSQLQuery("SELECT id,aponmnt_date,aponmntstart_time,time_number,patient_time from doctor_aponmnt_info where slot_status = '0' AND id ='" + appointmentId + "'");

            if (!appointmentInfoCheckSQL.list().isEmpty()) {
                System.out.println("appointmentSlotInfoCreate for :: " + doctorId);
                for (Iterator it = appointmentInfoCheckSQL.list().iterator(); it.hasNext();) {

                    memberObj = (Object[]) it.next();
                    appointmentIdXn = memberObj[0].toString();

                    appointmentDate = memberObj[1] == null ? "" : memberObj[1].toString();
                    appointmentStartingDateTime = memberObj[2] == null ? "" : memberObj[2].toString();
                    timeMinute = memberObj[3] == null ? "" : memberObj[3].toString();
                    perPratientTime = memberObj[4] == null ? "" : memberObj[4].toString();
                }

                System.out.println("appointmentDate :: " + appointmentDate);
                System.out.println("appointmentStartingDateTime :: " + appointmentStartingDateTime);
                System.out.println("timeMinute :: " + timeMinute);
                System.out.println("perPratientTime :: " + perPratientTime);

                InetAddress ip;
                String hostname = "";
                String ipAddress = "";
                try {
                    ip = InetAddress.getLocalHost();
                    hostname = ip.getHostName();
                    ipAddress = ip.getHostAddress();

                } catch (UnknownHostException e) {

                    e.printStackTrace();
                }

                String adduser = patientId;
                String adddate = dateFormatX.format(dateToday);
                String addterm = hostname;
                String published = "1";
                String slotStartTime = "";
                String slotEndTime = "";
                String slotStartTimePM = "";
                String slotStartTimePM1 = "";
                String slotName = "";
                String slotName1 = "";

                Date datePM = null;

                Double totalSerialCount = Double.parseDouble(timeMinute) / Double.parseDouble(perPratientTime);

                System.out.println("totalSerialCount :: " + totalSerialCount);

                Long totalSerialCount1 = Math.round(totalSerialCount);

                System.out.println("totalSerialCount1 :: " + totalSerialCount1);

                for (int k = 1; k <= totalSerialCount1; k++) {

                    int s = k - 1;

                    System.out.println("k :: " + k);

                    // if(k==0){
                    //  slotStartTime = appointmentStartingDateTime;
                    // }else{
                    //  }
                    slotName = "Slot-" + k;
                    System.out.println("slotName :: " + slotName);

                    //   int baseTimeFact = Integer.parseInt(doctorTimeSlotTimeNumber) / Integer.parseInt(doctorTimeSlotMaxSerialCount);                            //baseTimeFact 
                    //    int minuteAddForNextSerial = baseTimeFact * Integer.parseInt(doctorTimeSlotMinSerialCount);
                    int minuteAddForStart = s * Integer.parseInt(perPratientTime);
                    int minuteAddForEnd = k * Integer.parseInt(perPratientTime);

                    System.out.println("minuteAddForStart :: " + minuteAddForStart);
                    System.out.println("minuteAddForEnd :: " + minuteAddForEnd);

                    //  String myTime1 = "2020-06-29 17:31:02";
                    //  SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                    Date myTime1X = parseFormat_YMDHMS.parse(appointmentStartingDateTime);
                    Calendar calXn = Calendar.getInstance();
                    calXn.setTime(myTime1X);
                    calXn.add(Calendar.MINUTE, minuteAddForStart);
                    // String newTime1Xn = displayFormat_YMD_AM_PM.format(calXn.getTime());
                    slotStartTime = parseFormat_YMDHMS.format(calXn.getTime());

                    datePM = parseFormat_YMDHMS.parse(slotStartTime);
                    slotStartTimePM = displayFormat_H_M_AM_PM.format(datePM);

                    Calendar calXendN = Calendar.getInstance();
                    calXendN.setTime(myTime1X);

                    calXendN.add(Calendar.MINUTE, minuteAddForEnd);
                    // String newTime1Xn = displayFormat_YMD_AM_PM.format(calXn.getTime());
                    slotEndTime = parseFormat_YMDHMS.format(calXendN.getTime());

                    //     int nextSerial = Integer.parseInt(doctorTimeSlotMinSerialCount) + 1;
                    //    doctorTimeSlotMinSerialCountNext = Integer.toString(nextSerial);
                    System.out.println("timeCheck API appointmentStartingDateTime :" + appointmentStartingDateTime);
                    System.out.println("timeCheck API slotStartTime :" + slotStartTime);
                    System.out.println("timeCheck API slotEndTime :" + slotEndTime);

                    System.out.println("timeCheck API slotStartTimePM :" + slotStartTimePM);

                    String idAappS = getId.getID(80); //Doctor_Appointment_Slot_Info_ID
                    int pAppointmentId = Integer.parseInt(idAappS);

                    patientAppointmentInfoAddSQL = dbsession.createSQLQuery("INSERT INTO doctor_aponmnt_slot("
                            + "id,"
                            + "aponmnt_id,"
                            + "slot_date,"
                            + "slotstart_time,"
                            + "slotend_time,"
                            + "slot_name,"
                            + "name,"
                            + "add_user,"
                            + "add_date,"
                            + "add_term,"
                            + "add_ip"
                            + ") VALUES("
                            + "'" + pAppointmentId + "',"
                            + "'" + appointmentIdXn + "',"
                            + "'" + appointmentDate + "',"
                            + "'" + slotStartTime + "',"
                            + "'" + slotEndTime + "',"
                            + "'" + slotName + "',"
                            + "'" + slotStartTimePM + "',"
                            + "'" + adduser + "',"
                            + "'" + adddate + "',"
                            + "'" + hostname + "',"
                            + "'" + ipAddress + "'"
                            + "  ) ");

                    System.out.println("patientAppointmentInfoAddSQL :: " + patientAppointmentInfoAddSQL);
                      logger.info("patientAppointmentInfoAddSQL ::" + patientAppointmentInfoAddSQL);
                     patientAppointmentInfoAddSQL.executeUpdate();
                }

                Query updateDoctorTimeSlotStatusSQL = dbsession.createSQLQuery("UPDATE  doctor_aponmnt_info SET slot_status = '1', mod_date='" + adddate + "'  WHERE id = '" + appointmentId + "'");
                     updateDoctorTimeSlotStatusSQL.executeUpdate();
                dbtrx.commit();

                if (dbtrx.wasCommitted()) {

                    String msgPriority = "high"; // normal
                    //       appointmentTime = scheduleSlotStartTime;

                    //Notification for doctor
//                    if (!doctorDeviceId.equals("")) {
//
//                        String msgTitle = memMemberName + " appointment details";
//                        String msgBody = "Appointment Time :" + appointmentTime + " ," + patientDetailsForNotification;
//                        String receipentDeviceId = doctorDeviceId;
//                        //     String receipentDeviceId = "cGfWg1IlE1o:APA91bHKfVtMGSlAlLeufYlCuv2ZGlH1RAjY3ssnT8WNVJIJ0Hpvkav1VeHxQ-1WlYcVId09B2ubUDPL7lhzB7GXgU-gwtzZ96MpCwCUuTUHKofNQar32Ypb-mejf4xIPV6p66NsRMAY";
//                        System.out.println("receipentDeviceId :: doctorDeviceId ::" + receipentDeviceId);
//
//                        String fcmsendDoctor = fcmsend.sendSingleNotification(msgTitle, msgBody, receipentDeviceId, msgPriority);
//                        logger.info("fcmsendDoctor" + fcmsendDoctor);
//                        System.out.println("fcmsendDoctor::" + fcmsendDoctor);
//
//                    }
                    memberContentObj = new JSONObject();

                    //   memberContentObj.put("appointmentId", pAppointmentId);
                    memberContentObj.put("appointmentDate", appointmentDate);

                    memberContentObj.put("appointmentStartingDateTime", appointmentStartingDateTime);
                    memberContentObj.put("timeText", timeText);
                    // memberContentObj.put("timeHour", timeHour1);
                    //  memberContentObj.put("timeMinute", timeMinute1);
                    memberContentObj.put("perPratientTime", perPratientTime);
                    memberContentObj.put("totalSerialCount", totalSerialCount1);

                    memberContentObjArr.add(memberContentObj);

                    responseDataObj = new JSONObject();
                    responseDataObj.put("ResponseCode", "1");
                    responseDataObj.put("ResponseText", "Appointment Slot Info added successfully");
                    responseDataObj.put("ResponseData", memberContentObjArr);
                } else {
                    responseDataObj = new JSONObject();
                    responseDataObj.put("ResponseCode", "0");
                    responseDataObj.put("ResponseText", "ERROR!!! When Appointment Slot Info Add");
                    responseDataObj.put("ResponseData", memberContentObjArr);
                    logger.info("ERROR!!! When Appointment Info Add");
                }

//                responseDataObj = new JSONObject();
//                responseDataObj.put("ResponseCode", "1");
//                responseDataObj.put("ResponseText", "Found");
//                responseDataObj.put("ResponseData", memberContentObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "0");
                responseDataObj.put("ResponseText", "Appointment Slot Already Created");
                responseDataObj.put("ResponseData", memberContentObjArr);
            }

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>