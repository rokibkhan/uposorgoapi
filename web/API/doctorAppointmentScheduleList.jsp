<%-- 
    Document   : doctorAppointmentScheduleList
    Created on : OCT 12, 2020, 1:05:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorAppointmentScheduleList_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String doctorCategoryId = "";

    String doctorId = "";
    String appointmentDate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId")) {
    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

        //  memberId = request.getParameter("memberId").trim();
        //   givenPassword = request.getParameter("password").trim();
        doctorId = request.getParameter("doctorId").trim();
        //   appointmentDate = request.getParameter("appointmentDate").trim();

        System.out.println("doctorId :: " + doctorId);
        //  System.out.println("appointmentDate :: " + appointmentDate);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    Query memberSQL = null;
    Object memberObj[] = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();

    String memberXId = "";
    String appointmentScheduleId = "";
    String appointmentScheduleDate = "";
    String appointmentScheduleStartTime = "";
    String appointmentScheduleEndTime = "";
    String appointmentShortName = "";
    String appointmentName = "";
    String appointmentTimeText = "";
    String appointmentTimeHour = "";
    String appointmentTimeMinute = "";
    String appointmentTimeNumber = "";
    String appointmentPatientTime = "";
    String appointmentMaxSerialCount = "";
    String appointmentStatus = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String memberXIEBIdFirstChar = "";
    String memberXPictureName = "";
    String memberXPictureLink = "";
    String memberCustomOrder = "";
    String doctorRegNo = "";

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: doctorAppointmentScheduleList API rec :: " + rec);

    if (rec == 1) {

        try {

            memberSQL = dbsession.createSQLQuery("SELECT da.id, da.aponmnt_date,da.aponmntstart_time, "
                    + "da.aponmntend_time,da.short_name, da.name,da.time_text,da.time_hour,"
                    + "da.time_minute ,da.time_number ,da.patient_time ,da.max_serial_count,da.status "
                    + "FROM  doctor_aponmnt_info da "
                    + "WHERE da.status = 1 "
                    + "AND da.doctor_id = '" + doctorId + "' "
                    + "ORDER BY da.aponmntstart_time ASC");

            //   memberTypeSQL = dbsession.createSQLQuery("SELECT * FROM doctor_category_name ORDER BY name ASC");
            if (!memberSQL.list().isEmpty()) {
                for (Iterator it = memberSQL.list().iterator(); it.hasNext();) {

                    memberObj = (Object[]) it.next();
                    appointmentScheduleId = memberObj[0].toString();

                    appointmentScheduleDate = memberObj[1] == null ? "" : memberObj[1].toString();
                    appointmentScheduleStartTime = memberObj[2] == null ? "" : memberObj[2].toString();
                    appointmentScheduleEndTime = memberObj[3] == null ? "" : memberObj[3].toString();

                    appointmentShortName = memberObj[4] == null ? "" : memberObj[4].toString();
                    appointmentName = memberObj[5] == null ? "" : memberObj[5].toString();
                    appointmentTimeText = memberObj[6] == null ? "" : memberObj[6].toString();
                    appointmentTimeHour = memberObj[7] == null ? "" : memberObj[7].toString();

                    appointmentTimeMinute = memberObj[8] == null ? "" : memberObj[8].toString();
                    appointmentTimeNumber = memberObj[9] == null ? "" : memberObj[9].toString();
                    appointmentPatientTime = memberObj[10] == null ? "" : memberObj[10].toString();
                    appointmentMaxSerialCount = memberObj[11] == null ? "" : memberObj[11].toString();
                    appointmentStatus = memberObj[12] == null ? "" : memberObj[12].toString();

                    JSONObject scheduleSlotInfoResponseObj = new JSONObject();
                    JSONArray scheduleSlotInfoObjArray = new JSONArray();

                    String scheduleSlotSQL = null;
                    Query scheduleSlotSQLQuery = null;
                    Object[] scheduleSlotObj = null;
                    String scheduleSlotId = "";
                    String scheduleSlotStartTime = "";
                    String scheduleSlotEndTime = "";
                    String scheduleSlotName = "";
                    String scheduleSlotName1 = "";
                    String scheduleSlotStatus = "";
                    String scheduleSlotStatusText = "";
                    String scheduleSlotBlockedStatus = "";
                    String scheduleSlotBlockedStatusText = "";

                    scheduleSlotSQL = "SELECT das.id,das.slotstart_time,das.slotend_time, "
                            + "das.slot_name,das.name,das.status,das.blocked_status  "
                            + "FROM doctor_aponmnt_slot das "
                            + "WHERE das.aponmnt_id = '" + appointmentScheduleId + "'  "
                            + "ORDER BY das.id ASC ";

                    // + "WHERE das.status = '0'  AND das.aponmnt_id = '" + appointmentScheduleId + "'  "
                    System.out.println("scheduleSlotSQL :: " + scheduleSlotSQL);

                    scheduleSlotSQLQuery = dbsession.createSQLQuery(scheduleSlotSQL);
                    if (!scheduleSlotSQLQuery.list().isEmpty()) {
                        for (Iterator scheduleSlotItr = scheduleSlotSQLQuery.list().iterator(); scheduleSlotItr.hasNext();) {

                            scheduleSlotObj = (Object[]) scheduleSlotItr.next();

                            scheduleSlotId = scheduleSlotObj[0].toString();
                            scheduleSlotStartTime = scheduleSlotObj[1].toString() == null ? "" : scheduleSlotObj[1].toString();
                            scheduleSlotEndTime = scheduleSlotObj[2] == null ? "" : scheduleSlotObj[2].toString();
                            scheduleSlotName = scheduleSlotObj[3] == null ? "" : scheduleSlotObj[3].toString();
                            scheduleSlotName1 = scheduleSlotObj[4] == null ? "" : scheduleSlotObj[4].toString();
                            scheduleSlotStatus = scheduleSlotObj[5] == null ? "" : scheduleSlotObj[5].toString();

                            if (scheduleSlotStatus.equals("1")) {
                                scheduleSlotStatusText = "Booked";
                            } else {
                                scheduleSlotStatusText = "Free";
                            }

                            scheduleSlotBlockedStatus = scheduleSlotObj[6] == null ? "" : scheduleSlotObj[6].toString();

                            if (scheduleSlotBlockedStatus.equals("1")) {
                                scheduleSlotBlockedStatusText = "Blocked";
                            } else {
                                scheduleSlotBlockedStatusText = "UnBlock";
                            }

                            scheduleSlotInfoResponseObj = new JSONObject();

                            scheduleSlotInfoResponseObj.put("scheduleSlotId", scheduleSlotId);
                            scheduleSlotInfoResponseObj.put("scheduleSlotStartTime", scheduleSlotStartTime);
                            scheduleSlotInfoResponseObj.put("scheduleSlotEndTime", scheduleSlotEndTime);
                            scheduleSlotInfoResponseObj.put("scheduleSlotName", scheduleSlotName);
                            scheduleSlotInfoResponseObj.put("scheduleSlotName1", scheduleSlotName1);
                            scheduleSlotInfoResponseObj.put("scheduleSlotStatus", scheduleSlotStatus);
                            scheduleSlotInfoResponseObj.put("scheduleSlotStatusText", scheduleSlotStatusText);
                            scheduleSlotInfoResponseObj.put("scheduleSlotBlockedStatus", scheduleSlotBlockedStatus);
                            scheduleSlotInfoResponseObj.put("scheduleSlotBlockedStatusText", scheduleSlotBlockedStatusText);

                            scheduleSlotInfoObjArray.add(scheduleSlotInfoResponseObj);
                        }

                    } else {

                        scheduleSlotInfoObjArray = new JSONArray();
                    }

                    memberContentObj = new JSONObject();

                    memberContentObj.put("scheduleSlotInfo", scheduleSlotInfoObjArray);

                    memberContentObj.put("appointmentScheduleId", appointmentScheduleId);

                    memberContentObj.put("appointmentScheduleDate", appointmentScheduleDate);
                    memberContentObj.put("appointmentScheduleStartTime", appointmentScheduleStartTime);
                    memberContentObj.put("appointmentScheduleEndTime", appointmentScheduleEndTime);
                    memberContentObj.put("appointmentShortName", appointmentShortName);
                    memberContentObj.put("appointmentName", appointmentName);
                    memberContentObj.put("appointmentTimeText", appointmentTimeText);

                    memberContentObj.put("appointmentTimeHour", appointmentTimeHour);
                    memberContentObj.put("appointmentTimeMinute", appointmentTimeMinute);

                    memberContentObj.put("appointmentTimeNumber", appointmentTimeNumber);
                    memberContentObj.put("appointmentPatientTime", appointmentPatientTime);

                    memberContentObj.put("appointmentMaxSerialCount", appointmentMaxSerialCount);

                    memberContentObjArr.add(memberContentObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", memberContentObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", memberContentObjArr);

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>