<%-- 
    Document   : memberUpdateProfilePictureInfo
    Created on : MAY 23, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("memberUpdateProfilePictureInfo_jsp.class");

    getRegistryID getId = new getRegistryID();

    // memberXPictureLink = GlobalVariable.imageMemberDirLink + memberXPictureName;
    String imageBase64String = "";
    // String filePath = GlobalVariable.imageUploadPath;
    //  public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";     //WINDOWS
    //  public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";   //SERVER
    //  String filePath = "D:/NetbeansDevelopment/FFMS/web/upload/";     //WINDOWS
    //  String filePath = "/app/FFMS/webapps/FFMS/upload/";     //SERVER
    //   String filePath = "/app/FFMS/webapps/FFMS/web/upload/";     //SERVER

    // public static final String baseUrlImg = "http://localhost:8084/IEBWEB";
    // public static final String imagePath = "/home/joybangl/app/IEBWEB/webapps/IEBWEB/upload/";  //joybangla
    // public static final String imagePath = "D:/NetbeansDevelopment/IEBWEB/web/upload/";         //windows
    String filePath = GlobalVariable.imageMemberUploadPath;

    logger.info("memberUpdateProfilePictureInfo API filePath :" + filePath);

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String haveAttachment = ""; //1== yes 0==No attachment    
    String memberPictureName = "";
    String memberPictureNameLink = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("imageString")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        imageBase64String = request.getParameter("imageString") == null ? "" : request.getParameter("imageString");
        //imageBase64String = request.getParameter("imageString");
        logger.info("memberUpdateProfilePictureInfo API  imageBase64String :" + imageBase64String);

        System.out.println("memberUpdateProfilePictureInfo API imageBase64String :" + imageBase64String);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);
    
    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberUpdateProfilePictureInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    if (rec == 1) {

        try {

            if (imageBase64String != null) {

                Random random = new Random();
                int rand1 = Math.abs(random.nextInt());
                int rand2 = Math.abs(random.nextInt());
                DateFormat formatterFileToday = new SimpleDateFormat("yyyyMMdd");
                Date dateFileToday = new Date();
                String filePrefixToday = formatterFileToday.format(dateFileToday);

                String fileName1 = filePrefixToday + "_" + rand1 + "_" + rand2 + ".JPEG";

                logger.info("memberUpdateProfilePictureInfo API filePath :" + filePath);
                logger.info("memberUpdateProfilePictureInfo API fileName1 :" + fileName1);

                System.out.println("memberUpdateProfilePictureInfo API filePath :" + filePath);
                System.out.println("memberUpdateProfilePictureInfo API fileName1 :" + fileName1);

                // contentPostPictureLink = GlobalVariable.baseUrl + "/upload/" + fileName1;
                memberPictureNameLink = GlobalVariable.baseUrlImg + "/upload/member/" + fileName1;

                /*
                * Converting a Base64 String into Image byte array 
                 */
                // byte[] imageByteArray = Base64.decodeBase64(imageBase64String);
                byte[] imageByteArray = Base64.getDecoder().decode(imageBase64String);

                /*
                  * Write a image byte array into file system  
                 */
                //  FileOutputStream imageOutFile = new FileOutputStream("/Users/jeeva/Pictures/wallpapers/water-drop-after-convert.jpg");
                FileOutputStream imageOutFile = new FileOutputStream(filePath + fileName1);
                imageOutFile.write(imageByteArray);
                imageOutFile.close();

                memberPictureName = fileName1;

                //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
                qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

                if (!qMember.list().isEmpty()) {
                    for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                        member = (Member) itr0.next();
                        memId = member.getId();
                        memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                        dbPass = memberKeySQL.uniqueResult().toString();
                        if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                            InetAddress ip;
                            String hostname = "";
                            String ipAddress = "";
                            try {
                                ip = InetAddress.getLocalHost();
                                hostname = ip.getHostName();
                                ipAddress = ip.getHostAddress();

                            } catch (UnknownHostException e) {

                                e.printStackTrace();
                            }

                            adddate = dateFormatX.format(dateToday);

                            //update post_content_rating_point Rating for comment_content_point filed
                            Query updateMemberProfilePictureSQL = dbsession.createSQLQuery("UPDATE  member SET picture_name = '" + memberPictureName + "',mod_user='" + memId + "', mod_date='" + adddate + "',mod_ip='" + ipAddress + "'  WHERE id = '" + memId + "'");
                            updateMemberProfilePictureSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                postContentObj = new JSONObject();
                                //  postContentObj.put("Id", memId);

                                //     postContentObj.put("PictureName", memberPictureName);
                                //    postContentObj.put("PictureLink", memberPictureNameLink);
                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member picture added successfully");
                                logingObj.put("ResponseData", postContentObj);

                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member picture updated ");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member picture updated");
                            }

                        } else {

                            logingObj = new JSONObject();
                            logingObj.put("ResponseCode", "2");
                            logingObj.put("ResponseText", "Member password wrong");
                            logingObj.put("ResponseData", logingObjArray);
                            logger.info("User ID :" + memberId + " Member password wrong");

                        }

                    }

                } else {
                    logingObj = new JSONObject();
                    logingObj.put("ResponseCode", "0");
                    logingObj.put("ResponseText", "NotFound");
                    logingObj.put("ResponseData", logingObjArray);
                    logger.info("User ID :" + memberId + " Not Found");

                }
            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "Image is empty");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("Image is empty");

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
