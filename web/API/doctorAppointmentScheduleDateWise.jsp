<%-- 
    Document   : doctorAppointmentScheduleDateWise
    Created on : OCT 12, 2020, 1:05:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="org.apache.log4j.Logger"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>





<%

    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    Logger logger = Logger.getLogger("doctorAppointmentScheduleDateWise_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";
    String doctorCategoryId = "";

    String doctorId = "";
    String appointmentDate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    JSONArray prodCatImageObjArray = new JSONArray();
    JSONObject prodCatImageObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    //  if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("doctorId")) {
    if (request.getParameterMap().containsKey("key")) {
        key = request.getParameter("key").trim();

        //  memberId = request.getParameter("memberId").trim();
        //   givenPassword = request.getParameter("password").trim();
        doctorId = request.getParameter("doctorId").trim();
        appointmentDate = request.getParameter("appointmentDate").trim();

        System.out.println("doctorId :: " + doctorId);
        System.out.println("appointmentDate :: " + appointmentDate);

    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    /*
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
     */
    Query memberSQL = null;
    Object memberObj[] = null;

    JSONObject memberContentObj = new JSONObject();
    JSONArray memberContentObjArr = new JSONArray();
    
    
    Query mDoctorFeeSQL = null;
    Object[] mDoctorFeeObj = null;

    Query mDoctorDiscountSQL = null;
    Object[] mDoctorDiscountObj = null;
    
     String doctorDiscountId = "";
    String doctorDiscountType = "";
    String doctorDiscountTypeText = "";
    String doctorDiscountFixedAmount = "";
    String doctorDiscountPercentageAmount = "";

    String doctorDiscountTD = "";
    String doctorDiscountED = "";

    String baseFee = "";
    String discountType = ""; //fixed //percentage
    String discountAmount = "";
    String totalFee = "";
    String totalWithDiscount = "";
    String totalWithoutDiscount = "";
    String doctorBaseFee = "";

    String doctorFeeId = "";
    String doctorFeeAmount = "";
    String doctorFeeTD = "";
    String doctorFeeED = "";
    String doctorDiscountFeePercentage = "";
    Double doctorActualFee = 0.0d;
    Double doctorDiscontPercentageFee = 0.0d;

    String memberXId = "";
    String appointmentScheduleId = "";
    String appointmentScheduleDate = "";
    String appointmentScheduleStartTime = "";
    String appointmentScheduleEndTime = "";
    String appointmentShortName = "";
    String appointmentName = "";
    String appointmentTimeText = "";
    String appointmentTimeHour = "";
    String appointmentTimeMinute = "";
    String appointmentTimeNumber = "";
    String appointmentPatientTime = "";
    String appointmentMaxSerialCount = "";
    String memberXCenter = "";
    String memberXDivisionShort = "";
    String memberXDivisionFull = "";

    String memberXIEBIdFirstChar = "";
    String memberXPictureName = "";
    String memberXPictureLink = "";
    String memberCustomOrder = "";
    String doctorRegNo = "";

    JSONArray memberTypeResponseObjArr = new JSONArray();
    JSONObject memberTypeResponseObj = new JSONObject();

    JSONObject responseDataObj = new JSONObject();

    String keyStr = "";
    byte[] cipherText = Base64.getEncoder().encode(keyStr.getBytes("UTF8"));
    String encriptedString = new String((cipherText), "UTF-8");
    logger.info("EncriptedString  " + encriptedString);

    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("Uposorgo :: API :: doctorAppointmentScheduleDateWise API rec :: " + rec);

    if (rec == 1) {

        try {

            memberSQL = dbsession.createSQLQuery("SELECT da.id, da.aponmnt_date,da.aponmntstart_time, "
                    + "da.aponmntend_time,da.short_name, da.name,da.time_text,da.time_hour,"
                    + "da.time_minute ,da.time_number ,da.patient_time ,da.max_serial_count "
                    + "FROM  doctor_aponmnt_info da "
                    + "WHERE da.status = 1 "
                    + "AND da.doctor_id = '" + doctorId + "' "
                    + "AND da.aponmnt_date =' " + appointmentDate + "' "
                    + "ORDER BY da.aponmntstart_time ASC");

            //   memberTypeSQL = dbsession.createSQLQuery("SELECT * FROM doctor_category_name ORDER BY name ASC");
            if (!memberSQL.list().isEmpty()) {
                for (Iterator it = memberSQL.list().iterator(); it.hasNext();) {

                    memberObj = (Object[]) it.next();
                    appointmentScheduleId = memberObj[0].toString();

                    appointmentScheduleDate = memberObj[1] == null ? "" : memberObj[1].toString();
                    appointmentScheduleStartTime = memberObj[2] == null ? "" : memberObj[2].toString();
                    appointmentScheduleEndTime = memberObj[3] == null ? "" : memberObj[3].toString();

                    appointmentShortName = memberObj[4] == null ? "" : memberObj[4].toString();
                    appointmentName = memberObj[5] == null ? "" : memberObj[5].toString();
                    appointmentTimeText = memberObj[6] == null ? "" : memberObj[6].toString();
                    appointmentTimeHour = memberObj[7] == null ? "" : memberObj[7].toString();

                    appointmentTimeMinute = memberObj[8] == null ? "" : memberObj[8].toString();
                    appointmentTimeNumber = memberObj[9] == null ? "" : memberObj[9].toString();
                    appointmentPatientTime = memberObj[10] == null ? "" : memberObj[10].toString();
                    appointmentMaxSerialCount = memberObj[11] == null ? "" : memberObj[11].toString();

                    JSONObject scheduleSlotInfoResponseObj = new JSONObject();
                    JSONArray scheduleSlotInfoObjArray = new JSONArray();

                    String scheduleSlotSQL = null;
                    Query scheduleSlotSQLQuery = null;
                    Object[] scheduleSlotObj = null;
                    //String scheduleSlotPatientSQL = null;
                    //Query scheduleSlotPatientSQLQuery = null;
                    //Object[] scheduleSlotPatientObj = null;
                    String scheduleSlotId = "";
                    String scheduleSlotPatientId = "";
                    String scheduleSlotStartTime = "";
                    String scheduleSlotEndTime = "";
                    String scheduleSlotName = "";
                    String scheduleSlotName1 = "";
                    String scheduleSlotStatus = "";
                    String scheduleSlotStatusText = "";
                    String scheduleSlotBlockedStatus = "";
                    String scheduleSlotBlockedStatusText = "";
                    
                    
                    mDoctorFeeSQL = dbsession.createSQLQuery("SELECT id_fee,fee_amount,ed,td FROM doctor_fee_info WHERE doctor_id='" + doctorId + "'"
                                        + " and ed <='" 
                                +appointmentDate +"' and td >= '" + appointmentDate + "'");
                                
                                if (!mDoctorFeeSQL.list().isEmpty()) {
                                    for (Iterator mDoctorFeeItr = mDoctorFeeSQL.list().iterator(); mDoctorFeeItr.hasNext();) {

                                        mDoctorFeeObj = (Object[]) mDoctorFeeItr.next();
                                        doctorFeeId = mDoctorFeeObj[0].toString();
                                        doctorFeeAmount = mDoctorFeeObj[1].toString();
                                        doctorFeeTD = mDoctorFeeObj[2].toString();
                                        doctorFeeED = mDoctorFeeObj[3].toString();

                                    }
                                } else {

                                    doctorFeeAmount = "0.00";
                                    doctorFeeTD = "";
                                    doctorFeeED = "";
                                }
                                // memberContentObj.put("baseFeeId", doctorFeeId);
                                //memberContentObj.put("baseFeeAmount", Math.round(Double.parseDouble(doctorFeeAmount)));
                                //        memberContentObj.put("baseFeeAmount", doctorFeeAmount);

                                //memberContentObj.put("doctorFeeTD", doctorFeeTD);
                                //memberContentObj.put("doctorFeeED", doctorFeeED);

                                //doctor discount
                                mDoctorDiscountSQL = dbsession.createSQLQuery("SELECT ID,DISCOUNT_TYPE,FIXED_AMOUNT,PERCENTAGE_AMOUNT,TD,ED FROM doctor_discount WHERE doctor_id='" + doctorId + "'"
                                + " and ED <='" 
                                +appointmentDate +"' and TD >= '" + appointmentDate + "'"
                                );
                                if (!mDoctorDiscountSQL.list().isEmpty()) {
                                    for (Iterator mDoctorDiscountItr = mDoctorDiscountSQL.list().iterator(); mDoctorDiscountItr.hasNext();) {

                                        mDoctorDiscountObj = (Object[]) mDoctorDiscountItr.next();
                                        doctorDiscountId = mDoctorDiscountObj[0].toString();
                                        doctorDiscountType = mDoctorDiscountObj[1] == null ? "" : mDoctorDiscountObj[1].toString();

                                        doctorDiscountFixedAmount = mDoctorDiscountObj[2] == null ? "" : mDoctorDiscountObj[2].toString();
                                        doctorDiscountPercentageAmount = mDoctorDiscountObj[3] == null ? "" : mDoctorDiscountObj[3].toString();

                                        if (doctorDiscountType.equals(1)) {
                                            doctorDiscountTypeText = "PercentageAmount";

                                            doctorActualFee = Double.parseDouble(doctorFeeAmount) - Double.parseDouble(doctorDiscountPercentageAmount);
                                            
                                            doctorDiscontPercentageFee = ((Double.parseDouble(doctorDiscountPercentageAmount)/ Double.parseDouble(doctorFeeAmount)) * 100);
                                            
 
                                        } else {
                                            doctorDiscountTypeText = "FixedAmount";
                                            doctorActualFee = Double.parseDouble(doctorFeeAmount) - Double.parseDouble(doctorDiscountFixedAmount);
                                        }

                                        doctorDiscountTD = mDoctorDiscountObj[4] == null ? "" : mDoctorDiscountObj[4].toString();
                                        doctorDiscountED = mDoctorDiscountObj[5] == null ? "" : mDoctorDiscountObj[5].toString();

                                    }
                                } else {

                                    doctorDiscountType = "2";
                                    doctorDiscountTypeText = "NoDiscount";
                                    doctorDiscountFixedAmount = "0.00";
                                    doctorDiscountPercentageAmount = "0.00";
                                    doctorDiscountTD = "";
                                    doctorDiscountED = "";
                                    doctorActualFee = Double.parseDouble(doctorFeeAmount);

                                }

                                String doctorActualFee1 = Double.toString(doctorActualFee);

                                //memberContentObj.put("discountType", doctorDiscountType);
                                //memberContentObj.put("discountTypeText", doctorDiscountTypeText);
                                //memberContentObj.put("discountFixedAmount", Math.round(Double.parseDouble(doctorDiscountFixedAmount)));
                                //memberContentObj.put("discountPercentageAmount", Math.round(Double.parseDouble(doctorDiscountPercentageAmount)));
                                //memberContentObj.put("discontPercentageFee", Math.round(doctorDiscontPercentageFee));                                
                                //memberContentObj.put("doctorDiscountTD", doctorDiscountTD);
                                //memberContentObj.put("doctorDiscountED", doctorDiscountED);

                                //  memberContentObj.put("totalWithDiscount", totalWithDiscount);
                                //   memberContentObj.put("totalWithOutDiscount", totalWithoutDiscount);


                                // doctor fee end
                                
                    

                    scheduleSlotSQL = "SELECT das.id,das.slotstart_time,das.slotend_time, "
                            + "das.slot_name,das.name,das.status,das.blocked_status "
                            + "FROM doctor_aponmnt_slot das "
                            + "WHERE das.aponmnt_id = '" + appointmentScheduleId + "'  and das.status != '4' "
                            + "ORDER BY das.id ASC ";

                    // + "WHERE das.status = '0'  AND das.aponmnt_id = '" + appointmentScheduleId + "'  "
                    System.out.println("scheduleSlotSQL :: " + scheduleSlotSQL);

                    scheduleSlotSQLQuery = dbsession.createSQLQuery(scheduleSlotSQL);
                    if (!scheduleSlotSQLQuery.list().isEmpty()) {
                        for (Iterator scheduleSlotItr = scheduleSlotSQLQuery.list().iterator(); scheduleSlotItr.hasNext();) {

                            scheduleSlotObj = (Object[]) scheduleSlotItr.next();

                            scheduleSlotId = scheduleSlotObj[0].toString();
                            scheduleSlotStartTime = scheduleSlotObj[1].toString() == null ? "" : scheduleSlotObj[1].toString();
                            scheduleSlotEndTime = scheduleSlotObj[2] == null ? "" : scheduleSlotObj[2].toString();
                            scheduleSlotName = scheduleSlotObj[3] == null ? "" : scheduleSlotObj[3].toString();
                            scheduleSlotName1 = scheduleSlotObj[4] == null ? "" : scheduleSlotObj[4].toString();
                            scheduleSlotStatus = scheduleSlotObj[5] == null ? "" : scheduleSlotObj[5].toString();

                            if (scheduleSlotStatus.equals("1")) {
                                scheduleSlotStatusText = "Booked"; 
                                
                                /*scheduleSlotPatientSQL = "SELECT dpa.id,dpa.patent_id  "
                                + " dpa.status,dpa.blocked_status "
                                + "FROM doctor_patient_aponmnt dpa "
                                + "WHERE dpa.aponmnt_slot_id = '" + appointmentScheduleId + "'  and dpa.status = '0' ";
                                */
                                Query scheduleSlotPatientSQL = dbsession.createSQLQuery("SELECT dpa.patent_id FROM doctor_patient_aponmnt dpa WHERE dpa.aponmnt_slot_id='" + scheduleSlotId + "' and dpa.status = '0'");
                                scheduleSlotPatientId = scheduleSlotPatientSQL.uniqueResult() == null ? "" : scheduleSlotPatientSQL.uniqueResult().toString();
                            
                            } 
                            else if(scheduleSlotStatus.equals("2"))
                            {
                                scheduleSlotStatusText = "Paid";
                            }
                            else {
                                scheduleSlotStatusText = "Free";
                            }

                            scheduleSlotBlockedStatus = scheduleSlotObj[6] == null ? "" : scheduleSlotObj[6].toString();

                            if (scheduleSlotBlockedStatus.equals("1")) {
                                scheduleSlotBlockedStatusText = "Blocked";
                            } else {
                                scheduleSlotBlockedStatusText = "UnBlock";
                            }
                            
                            scheduleSlotInfoResponseObj = new JSONObject();

                            scheduleSlotInfoResponseObj.put("scheduleSlotId", scheduleSlotId);
                            scheduleSlotInfoResponseObj.put("scheduleSlotStartTime", scheduleSlotStartTime);
                            scheduleSlotInfoResponseObj.put("scheduleSlotEndTime", scheduleSlotEndTime);
                            scheduleSlotInfoResponseObj.put("scheduleSlotName", scheduleSlotName);
                            scheduleSlotInfoResponseObj.put("scheduleSlotName1", scheduleSlotName1);
                            scheduleSlotInfoResponseObj.put("scheduleSlotStatus", scheduleSlotStatus);
                            scheduleSlotInfoResponseObj.put("scheduleSlotStatusText", scheduleSlotStatusText);
                            scheduleSlotInfoResponseObj.put("scheduleSlotBlockedStatus", scheduleSlotBlockedStatus);
                            scheduleSlotInfoResponseObj.put("scheduleSlotBlockedStatusText", scheduleSlotBlockedStatusText);
                            scheduleSlotInfoResponseObj.put("scheduleSlotPatientId",scheduleSlotPatientId);
                            
                            scheduleSlotInfoResponseObj.put("baseFeeAmount", Math.round(Double.parseDouble(doctorFeeAmount)));
                            scheduleSlotInfoResponseObj.put("doctorActualFee", Math.round(doctorActualFee));
                            //scheduleSlotInfoResponseObj.put("doctorActualFee1", doctorActualFee1);

                            scheduleSlotInfoObjArray.add(scheduleSlotInfoResponseObj);
                            scheduleSlotPatientId = "";
                        }

                    } else {

                        scheduleSlotInfoObjArray = new JSONArray();
                    }

                    memberContentObj = new JSONObject();

                    memberContentObj.put("scheduleSlotInfo", scheduleSlotInfoObjArray);

                    memberContentObj.put("appointmentScheduleId", appointmentScheduleId);

                    memberContentObj.put("appointmentScheduleDate", appointmentScheduleDate);
                    memberContentObj.put("appointmentScheduleStartTime", appointmentScheduleStartTime);
                    memberContentObj.put("appointmentScheduleEndTime", appointmentScheduleEndTime);
                    memberContentObj.put("appointmentShortName", appointmentShortName);
                    memberContentObj.put("appointmentName", appointmentName);
                    memberContentObj.put("appointmentTimeText", appointmentTimeText);

                    memberContentObj.put("appointmentTimeHour", appointmentTimeHour);
                    memberContentObj.put("appointmentTimeMinute", appointmentTimeMinute);

                    memberContentObj.put("appointmentTimeNumber", appointmentTimeNumber);
                    memberContentObj.put("appointmentPatientTime", appointmentPatientTime);

                    memberContentObj.put("appointmentMaxSerialCount", appointmentMaxSerialCount);
                    
                    
                    
                    

                    memberContentObjArr.add(memberContentObj);

                }

                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "Found");
                responseDataObj.put("ResponseData", memberContentObjArr);
            } else {
                responseDataObj = new JSONObject();
                responseDataObj.put("ResponseCode", "1");
                responseDataObj.put("ResponseText", "NotFound");
                responseDataObj.put("ResponseData", memberContentObjArr);

            }
        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);
        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

    } else {
        responseDataObj = new JSONObject();
        responseDataObj.put("ResponseCode", "999");
        responseDataObj.put("ResponseText", "ValidationFailed");
        responseDataObj.put("ResponseData", memberTypeResponseObjArr);
//                        responseDataObjArray.add(responseDataObj);
        PrintWriter writer = response.getWriter();
        writer.write(responseDataObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>