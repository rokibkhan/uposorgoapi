<%-- 
    Document   : memberUpdateAddressInfo
    Created on : June 19, 2020, 11:18:05 PM
    Author     : TAHAJJAT
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.hibernate.internal.SessionImpl"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page language="java" contentType="application/json; charset=UTF-8" pageEncoding="UTF-8"%>


<%@page import="java.util.*" %>
<%@page import="java.io.*" %>
<%@page import="java.io.File" %>
<%@page import="java.io.FileOutputStream" %>
<%@page import="java.io.IOException" %>
<%@page import="java.net.*" %>
<%@page import="javax.servlet.http.HttpSession" %>
<%@page session="true" %>
<%@page import="com.appul.util.*" %>
<%@page import="com.appul.entity.*" %>
<%@page import="org.hibernate.*" %>
<%@page import="com.appul.servlet.*" %>
<%@page import=" org.json.simple.*" %>


<%

    //  ALTER TABLE `joybandb`.`member_address` 
//CHANGE COLUMN `address_type` `address_type` VARCHAR(1) NOT NULL DEFAULT 'P' COMMENT 'P->Permanent Address:M->Mailing Address' ;
    Session dbsession = HibernateUtil.getSessionFactory().openSession();
    org.hibernate.Transaction dbtrx = null;
    dbtrx = dbsession.beginTransaction();

    getRegistryID getId = new getRegistryID();

    Logger logger = Logger.getLogger("memberUpdateAddressInfo_jsp.class");

    DateFormat dateFormatX = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    DateFormat dateFormatYmd = new SimpleDateFormat("yyyy-MM-dd");
    Date dateToday = new Date();

    //  Logger logger = Logger.getLogger("postContentRequest_jsp.class");
    //   String imageUrl = new GlobalVariable().imageUrl;
    //   String imagePath = new GlobalVariable().imagePath;
    String key = "";
    String memberId = "";
    String givenPassword = "";

    String addressType = "";

    String addressLine1 = "";
    String addressLine2 = "";
    String district = "";
    String thana = "";

    String memberName = "";
    String gender = "";
    String bloodGroup = "";
    String phone1 = "";
    String phone2 = "";
    String email = "";

    String adddate = "";

    JSONArray bannerObjArray = new JSONArray();
    JSONObject bannerObj = new JSONObject();

    if (request.getMethod().equals("GET")) {

        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "GET Method is not allowed here!.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
        return;
    }

    if (request.getParameterMap().containsKey("key") && request.getParameterMap().containsKey("memberId") && request.getParameterMap().containsKey("password") && request.getParameterMap().containsKey("addressType") && request.getParameterMap().containsKey("district") && request.getParameterMap().containsKey("thana")) {
        key = request.getParameter("key").trim();

        memberId = request.getParameter("memberId").trim();
        givenPassword = request.getParameter("password").trim();

        addressType = request.getParameter("addressType").trim();
        addressType = addressType.toUpperCase();

        addressLine1 = request.getParameter("addressLine1") == null ? "" : request.getParameter("addressLine1").trim();
        addressLine2 = request.getParameter("addressLine2") == null ? "" : request.getParameter("addressLine2").trim();
        district = request.getParameter("district") == null ? "" : request.getParameter("district").trim();
        thana = request.getParameter("thana") == null ? "" : request.getParameter("thana").trim();

        //   memberName = request.getParameter("name").trim();
    } else {
        bannerObj = new JSONObject();
        bannerObj.put("ResponseCode", "0");
        bannerObj.put("ResponseText", "Wrong parameter.");
        bannerObj.put("ResponseData", bannerObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(bannerObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();

        return;
    }
    String adjustLettter1 = "+88";
    String adjustLettter2 = "+";
    String memberId1 = "";
    String firstLetter = memberId.substring(0, 1);

    if (firstLetter.equals("0")) {
        memberId1 = adjustLettter1 + memberId;
    }
    if (firstLetter.equals("8")) {
        memberId1 = adjustLettter2 + memberId;
    }
    if (firstLetter.equals("+")) {
        memberId1 = memberId;
    }
    Member member = null;
    int memId = 0;
    String dbPass = "";

    logger.info("User ID :" + memberId);
    //   int rec = SystemToken.KeyValidation(key);
    int rec = 1;

    logger.info("JoyBangla :: API :: memberUpdateAddressInfo API rec :: " + rec);

    Encryption encryption = new Encryption();

    JSONObject logingObj = new JSONObject();

    JSONObject postContentObj = new JSONObject();
    JSONArray postContentObjArr = new JSONArray();

    JSONObject responseObj = new JSONObject();
    JSONArray logingObjArray = new JSONArray();
    Query qMember = null;
    Query memberKeySQL = null;

    String filterSQLStr = "";
    Object searchObj[] = null;

    InetAddress ip;
    String hostname = "";
    String ipAddress = "";
    try {
        ip = InetAddress.getLocalHost();
        hostname = ip.getHostName();
        ipAddress = ip.getHostAddress();

    } catch (UnknownHostException e) {

        e.printStackTrace();
    }

    adddate = dateFormatX.format(dateToday);

    if (rec == 1) {

        try {

            //  qMember = dbsession.createQuery(" from Member where memberId = '" + memberId + "'");
            qMember = dbsession.createQuery(" from Member where mobile = '" + memberId1 + "'");

            if (!qMember.list().isEmpty()) {
                for (Iterator itr0 = qMember.list().iterator(); itr0.hasNext();) {
                    member = (Member) itr0.next();
                    memId = member.getId();
                    memberKeySQL = dbsession.createSQLQuery("SELECT member_key FROM member_credential where member_id='" + memId + "'");

                    dbPass = memberKeySQL.uniqueResult().toString();
                    if (dbPass.equalsIgnoreCase(encryption.getEncrypt(givenPassword))) {

                        int memberAddressId = 0;
                        Query memAdrsSQL = dbsession.createSQLQuery("select address_id from member_address WHERE member_id ='" + memId + "' AND address_type ='" + addressType + "'");
                        if (!memAdrsSQL.list().isEmpty()) {
                            //Update member Address

                            memberAddressId = Integer.parseInt(memAdrsSQL.uniqueResult().toString());

                            //update member Rating for comment_content_point filed
                            Query updateMemberAddressInfoSQL = dbsession.createSQLQuery("UPDATE  address_book SET "
                                    + "ADDRESS_1 = '" + addressLine1 + "',"
                                    + "ADDRESS_2 = '" + addressLine1 + "',"
                                    + "DISTRICT_ID = '" + district + "',"
                                    + "THANA_ID='" + thana + "', "
                                    + "COUNTRY_CODE='BD', "
                                    + "mod_user='" + memId + "', "
                                    + "MOD_DATE='" + adddate + "'"
                                    + "WHERE ID = '" + memberAddressId + "'");

                            updateMemberAddressInfoSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                postContentObj = new JSONObject();
                                //  postContentObj.put("Id", memId);

                                //     postContentObj.put("PictureName", memberPictureName);
                                //    postContentObj.put("PictureLink", memberPictureNameLink);
                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member address info updated successfully");
                                logingObj.put("ResponseData", postContentObj);

                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member address updated ");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member address updated");
                            }

                            /*
                            InetAddress ip;
                            String hostname = "";
                            String ipAddress = "";
                            try {
                                ip = InetAddress.getLocalHost();
                                hostname = ip.getHostName();
                                ipAddress = ip.getHostAddress();

                            } catch (UnknownHostException e) {

                                e.printStackTrace();
                            }

                            adddate = dateFormatX.format(dateToday);

                            //update member Rating for comment_content_point filed
                            Query updateMemberPersonalInfoSQL = dbsession.createSQLQuery("UPDATE  member SET "
                                    + "member_name = '" + memberName + "',"
                                    + "gender = '" + gender + "',"
                                    + "blood_group = '" + bloodGroup + "',"
                                    + "phone1='" + phone1 + "', "
                                    + "phone2='" + phone2 + "', "
                                    + "email_id='" + email + "', "
                                    + "mod_user='" + memId + "', "
                                    + "mod_date='" + adddate + "', "
                                    + "mod_ip='" + ipAddress + "'  "
                                    + "WHERE id = '" + memId + "'");

                            updateMemberPersonalInfoSQL.executeUpdate();

                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                postContentObj = new JSONObject();
                                //  postContentObj.put("Id", memId);

                                //     postContentObj.put("PictureName", memberPictureName);
                                //    postContentObj.put("PictureLink", memberPictureNameLink);
                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member personal profile updated successfully");
                                logingObj.put("ResponseData", postContentObj);

                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "ERROR!!! When member profile updated ");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("ERROR!!! When member profile updated");
                            }
                            
                             */
                        } else {
                            //Insert member Address

                            String idS = getId.getID(27);
                            int regAddressId = Integer.parseInt(idS);

                            String idMA = getId.getID(28);
                            int regMemberAddressId = Integer.parseInt(idMA);

                            Query mAddressAddSQL = dbsession.createSQLQuery("INSERT INTO address_book("
                                    + "ID,"
                                    + "ADDRESS_1,"
                                    + "ADDRESS_2,"
                                    + "DISTRICT_ID,"
                                    + "THANA_ID,"
                                    + "COUNTRY_CODE,"
                                    + "ADD_USER,"
                                    + "ADD_DATE,"
                                    + "MOD_DATE"
                                    + ") VALUES("
                                    + "" + regAddressId + ","
                                    + "'" + addressLine1 + "',"
                                    + "'" + addressLine2 + "',"
                                    + "'" + district + "',"
                                    + "'" + thana + "',"
                                    + "'BD',"
                                    + "'" + memId + "',"
                                    + "'" + adddate + "',"
                                    + "'" + adddate + "'"
                                    + "  ) ");

                            logger.info("mAddressAddSQL ::" + mAddressAddSQL);
                            System.out.println("mAddressAddSQL ::" + mAddressAddSQL);

                            mAddressAddSQL.executeUpdate();

                            Query mMemberAddressAddSQL = dbsession.createSQLQuery("INSERT INTO member_address("
                                    + "id,"
                                    + "member_id,"
                                    + "address_id,"
                                    + "address_type,"
                                    + "ed,"
                                    + "td"
                                    + ") VALUES("
                                    + "" + regMemberAddressId + ","
                                    + "'" + memId + "',"
                                    + "'" + regAddressId + "',"
                                    + "'" + addressType + "',"
                                    + "'" + adddate + "',"
                                    + "'" + adddate + "'"
                                    + "  ) ");

                            logger.info("mMemberAddressAddSQL ::" + mMemberAddressAddSQL);
                            System.out.println("mMemberAddressAddSQL ::" + mMemberAddressAddSQL);

                            mMemberAddressAddSQL.executeUpdate();

                            //insert address_book
                            //insert member_address
                            dbtrx.commit();

                            if (dbtrx.wasCommitted()) {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "1");
                                logingObj.put("ResponseText", "Member Address Added Successfully");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("User ID :" + memberId + " Member Address Added Successfully");
                            } else {

                                logingObj = new JSONObject();
                                logingObj.put("ResponseCode", "0");
                                logingObj.put("ResponseText", "Member Address Added Successfully");
                                logingObj.put("ResponseData", logingObjArray);
                                logger.info("Member Address Added Successfully");
                            }
                        }

                    } else {

                        logingObj = new JSONObject();
                        logingObj.put("ResponseCode", "2");
                        logingObj.put("ResponseText", "ERROR!!! Member Password info wrong ");
                        logingObj.put("ResponseData", logingObjArray);
                        logger.info("ERROR!!! Member Password info wrong");

                    }

                }

            } else {
                logingObj = new JSONObject();
                logingObj.put("ResponseCode", "0");
                logingObj.put("ResponseText", "NotFound");
                logingObj.put("ResponseData", logingObjArray);
                logger.info("User ID :" + memberId + " Not Found");

            }

        } catch (Exception e) {
            bannerObj = new JSONObject();
            bannerObj.put("ResponseCode", "0");
            bannerObj.put("ResponseText", "Something went wrong!");
            bannerObj.put("ResponseData", bannerObjArray);

        }

//        finally {
//            dbtrx.commit();
//
//        }
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    } else {
        logingObj = new JSONObject();
        logingObj.put("ResponseCode", "999");
        logingObj.put("ResponseText", "Key Validation Failed");
        logingObj.put("ResponseData", logingObjArray);
        PrintWriter writer = response.getWriter();
        writer.write(logingObj.toJSONString());
        response.getWriter().flush();
        response.getWriter().close();
    }
    dbsession.flush();
    dbsession.close();

%>
